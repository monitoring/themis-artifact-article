---
title: DIY Experiment (Tutorial)
---

This is a directory reserved for a small experiment that we can build to test THEMIS.

> Make sure you are running in the docker or that THEMIS is installed

## Setup


### Properties
Be sure to be operating in the folder designated for the experiment, that is `experiments/diy`.
Generate the general experiment skeleton using:

```
themis_expskeleton
```

Then edit `setup.properties`, change the entries provided below

```
traces.components=3
traces.observations=2
traces.maximum=5
traces.length=10
run.length=14
run.components=3
```

This instructs the experiment tool the following:

* Traces contain 3 components
* Traces contain 2 observations per component
* We will be reading a total of 5 traces (with IDs, 0-4)
* Traces have length 10 (i.e., they contain 10 events)
* The length of the run is 14 (that is, we wait an additional 4 rounds before timeout)
* The algorithm will assume there is 3 components (traces can include more components than those we need for monitoring)

### Traces

We next proceed to generating the traces.

```
themis_gentrace 5 10 3 2  $THEMIS_HOME/resources/profiles/normal.profile traces 1
```

* We generate **5** traces, of size **10** each, for **3** components with **2** observations each.
* We generate the traces using the normal probability distribution.
* We store the trace in folder **traces**.
* The **1** indicates to keep the folder, as normally the folder is tar gzipped and then deleted.


### Specifications

We next generate **2** specifications for **3** components and **2** observations per component using the ltl template.
We store them in a directory called **spec**

```
themis_genspec 2 3 2  $THEMIS_HOME/resources/templates/ltl.xml spec 1
```

This generates the `spec` directory along with the `specs.txt` file.

## Execution

To execute the experiment we will need two separate terminals.

> In the docker simply use `screen` or `tmux`

To spawn a screen use: 

```
screen
```

Then you can use the following to create and navigate (for tmux, use Ctrl+b instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal

To run the experiment, then proceed as follows:

* In terminal 1 execute `make node`, wait for the `Node Reloaded` message.
* Then, in terminal 2, execute `make run`

Once the run is complete, you can terminate the node using `Ctrl+C`.

The execution will generate a database based on the address of the node: `localhost-8056.db` 

## Exploring the Results

Since all results are stored in the database, we can now query the results to explore them.


### Verification

You can first check that algorithms did not produce inconsistent verdicts (i.e., for the same traces and specs, two algorithms generated the same verdicts) using the provided query `process/inconsistent.sql`.

```
sqlite3 localhost-8056.db < process/inconsistent.sql
```

If the query returns no rows, then all results are consistent, as it only selects the inconsistent ones.


### Example

Create a file `bench.sql` as follows:

```sql
SELECT alg, comps, 
	avg((1.0*sum_delay)/resolutions) as delay, 
	avg((1.0*msg_num)/run_len) as num, 
	avg((1.0*msg_data)/run_len) as data
FROM bench
	GROUP BY alg, comps
```

Then simply execute it:

```
sqlite3 -header localhost-8056.db < bench.sql
```

### Dump to CSV

Typically you might be interested in reading the data in another software (like R) and process it there, this can be acheived easily by using

```
sqlite3 -header -csv localhost-8056.db "SELECT * FROM bench" > data.csv
```

> You can also normalize your data directly in the database using queries such as `process/normalize.sql` although we recommend normalizing when processing the data. Do not execute the normalization query if you intend to proceed with the next section

### Plot

You can use the chiron plotting file to directly plot the results of your small experiment.
To do so execute the following:

```
cd ../../plot/diy
make
```

If you are using a custom database name you can specify it to the plot

```
make DB=../../experiments/diy/dbname.db
```

