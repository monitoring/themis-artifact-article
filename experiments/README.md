---
title: Experiments
---

We include two experiments and a tutorial

* [Synthetic Benchmark](syn/) (~ 48-72 hours)
* [Chiron](chiron/) (~ 2-4 hours)
* [DIY Experiment](diy) (Tutorial ~ 10 mins)

The synthetic benchmarks and chiron traces are compressed, as the traces and results are particularly large. 
