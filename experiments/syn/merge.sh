#!/bin/bash
i=0
SQL="merge.sql"
DB="merge.db"


rm -f $SQL
echo 'BEGIN;' > $SQL
while read line
do
  if [ $i -eq 0 ]; then
    echo "Copying $line"
    cp $line merge.db;
    Q=`sqlite3 merge.db 'PRAGMA table_info(bench);' | cut -d '|' -f 2 | tail -n +2 | paste -d, -s` 
  else
    echo "Adding... $line"
	  echo "attach database '$line' as e$i;" >> $SQL
	  echo "insert into 'bench' select NULL, $Q from e$i.bench;" >> $SQL
	fi
	i=$(( i + 1 ))
done
echo 'COMMIT;' >> $SQL

sqlite3 $DB < $SQL
rm -f $SQL
