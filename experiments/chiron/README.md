---
title: Chiron Example
---

An experiment that runs based on traces from the [Chiron example](http://laser.cs.umass.edu/verification-examples/chiron/).

## Running the experiment

To run the experiment you need to first extract the traces.

```
cd chiron-source
tar xvzf traces
cd ..
```
Before starting delete or rename `localhost-8056.db`

To run the experiment you will need at least a terminal per node and one for the execution.
To do so you can use `screen` as follows, to spawn a screen use: 

```
screen
```

Then you can use the following to create and navigate (for tmux, use Ctrl+b instead):

* `Ctrl+a c` to create a new terminal in the same screen
* `Ctrl+a n` to navigate to the next terminal
* `Ctrl+a p` to navigate to the previous terminal

The experiment uses one node, you need to start a node using

```
make node
```

Then execute the experiment while the node is running using

```
make run
```

## Generating Traces

To generate traces and run chiron read the documentation in [chiron-source](chiron-source/)
