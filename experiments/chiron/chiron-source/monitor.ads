with Ada.Containers.Hashed_Maps;    use Ada.Containers;
package monitor is

  type AP is (
      ap_a0, ap_a1, ap_a2, ap_a3, ap_a4, ap_a5, ap_a6, ap_a7, ap_a8, ap_a9, ap_a10, ap_a11, 
      ap_b0, ap_b1, ap_b2, ap_b3, ap_b4, ap_b5,
      ap_c0, ap_c1, ap_c2, ap_c3, ap_c4, ap_c5,
      ap_d0
  );
  
  type AP_AR is array(Positive range <>) of AP;
  
  function ID_Hashed (id: AP) return Hash_Type;
  
  package AtomMap is new Ada.Containers.Hashed_Maps(AP, Boolean, ID_Hashed, "=");
  
  observations : AtomMap.Map;
  
  
  task mon is
    entry observe(obs: AP; v : Boolean);
    entry observe(obs: AP_AR; v : Boolean);
    entry trace;
    entry start;
    entry stop;
  end mon;
  
  task moninit;

end monitor;

