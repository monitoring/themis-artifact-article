#!/bin/bash
rm -rf traces
mkdir traces
for i in $(seq 0 $1)
do
	echo -n "$i [Execute] "
	./chiron $2 > t1.trace
	echo -n " [Trace]"
	make -s convert FILE=t1.trace PREFIX="traces/$i-"
	echo ""
done
rm t1.trace
