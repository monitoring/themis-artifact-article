with disp;
with monitor;                       use monitor;
with Ada.Text_IO;                   use Ada.Text_IO;
with Ada.Integer_Text_IO;           use Ada.Integer_Text_IO;
with Ada.Command_Line;              use Ada.Command_Line;
with Ada.Numerics.discrete_Random;

package body disp is

   subtype Rand_Range is positive;
   package Rand_Int is new Ada.Numerics.Discrete_Random(Rand_Range);
   gen : Rand_Int.Generator;
   

  function choose return boolean is
  begin
    return true;
  end choose;
  
  type Sub_List is array (1..number_of_artists) of a_id_type;
  type State_List is array(0 .. 3) of Boolean;
   
  function choose return e_kind is
 
  begin
    if((Rand_Int.Random(gen) mod 2) = 0) then
      return e1;
    else
      return e2;
    end if;
  end choose;

  function ev_eq(lst : Sub_List; sz : Integer) return String is
  begin
    if(lst'length = sz) then
      return "t";
    else
      return "f";
     end if;
  end ev_eq;
  function ev_gt(lst : Sub_List; sz : Integer) return String is
  begin
    if(lst'length > sz) then
      return "t";
    else
      return "f";
     end if;
  end ev_gt;
  
  function btrace(b : Boolean) return String is
  begin
    if b then return "t";
    end if;
    return "f";
  end btrace;
  
  procedure eset(slist : IN OUT State_List; offset : Integer; id : a_id_type; v : Boolean) is
  begin
    if id = art1 
      then slist(offset)   := v;
      else slist(offset+2) := v;
    end if;
  end eset;
   
  task body dispatcher is

    e1_lst: Sub_List;
    e2_lst: Sub_List;
    e1_sz : list_size := 0;
    e2_sz : list_size := 0;
    rg : State_List := (others => False);
    notify : State_List := (others => False);
    i : i_range;
    
    procedure trace is
    begin

      mon.observe(ap_a0,  rg(0));  -- registered_event_a1_e1
      mon.observe(ap_a1,  rg(1));  -- registered_event_a1_e2
      mon.observe(ap_a2,  rg(2));  -- registered_event_a2_e1
      mon.observe(ap_a3,  rg(3));  -- registered_event_a2_e2
      mon.observe(ap_a4,  notify(0)); -- notify_a1_e1
      mon.observe(ap_a5,  notify(1)); -- notify_a1_e2
      mon.observe(ap_a6,  notify(2)); -- notify_a2_e1
      mon.observe(ap_a7,  notify(3)); -- notify_a2_e2
      mon.observe(ap_a8,  e1_sz = 0); -- lst_sz0_e1
      mon.observe(ap_a9,  e2_sz = 0); -- lst_sz0_e2
      mon.observe(ap_a10, e1_sz > 2); -- lst_gt2_e1
      mon.observe(ap_a11, e2_sz > 2); -- lst_gt2_e2
      mon.trace;
      
    end trace;

    
    begin
      loop
        notify := (others => False);
        select
        
          accept register_event (a_id : a_id_type; event_kind : e_kind) do
            case event_kind is

              when e1 =>
                i := 1;
                loop
                  if i > e1_sz then
                    e1_sz := e1_sz + 1;
                    e1_lst(i) := a_id;
                    eset(rg, 0, a_id, true); trace;
                    exit;
                  end if;
                  if e1_lst(i) = a_id then
                    exit;
                  end if;
                  i := i + 1;
                end loop;

              when e2 =>
                i := 1;
                loop
                  if i > e2_sz then
                    e2_sz := e2_sz + 1;
                    e2_lst(i) := a_id;
                    eset(rg, 1, a_id, true); trace;
                    exit;
                  end if;
                  if e2_lst(i) = a_id then
                    exit;
                  end if;
                  i := i + 1;
                end loop;

            end case;
          end register_event;
	    
	  or
	    
          accept unregister_event (a_id : a_id_type; event_kind : e_kind) do
            case event_kind is

              when e1 =>
                if e1_sz = 0 then
                  null;
                else
                  i := 1;
                  while i <= e1_sz loop
                    if e1_lst(i) = a_id then
                      while i < e1_sz loop
                        e1_lst(i) := e1_lst(i+1);
                        i := i + 1;
                      end loop;
                      e1_sz := e1_sz - 1;
                    end if;
                    i := i + 1;
                  end loop;
                  eset(rg, 0, a_id, false); trace;
                end if;

              when e2 =>
                if e2_sz = 0 then
                  null;
                else
                  i := 1;
                  while i <= e2_sz loop
                    if e2_lst(i) = a_id then
                      while i < e2_sz loop
                        e2_lst(i) := e2_lst(i+1);
                        i := i + 1;
                      end loop;
                      e2_sz := e2_sz - 1;
                    end if;
                    i := i + 1;
                  end loop;
                  eset(rg, 1, a_id, false); trace;
                end if;

            end case;
          end unregister_event;
	    
          or

          accept notify_artists (event : e_kind) do
            notify := (others => False);
            case event is

              when e1 =>
                for i in 1 .. e1_sz loop
                  eset(notify, 0, e1_lst(i), true); trace;
                  case e1_lst(i) is
                    when art1 =>
                      a1.notify_client_event(e1);
                    when art2 =>
                      a2.notify_client_event(e1);
                  end case;
                end loop;

              when e2 =>
                for i in 1 .. e2_sz loop
                  eset(notify, 1, e1_lst(i), true); trace;
                  case e2_lst(i) is
                    when art1 =>
                      a1.notify_client_event(e2);
                      
                    when art2 =>
                      a2.notify_client_event(e2);
                  end case;
                end loop;
		     
            end case;
          end notify_artists;
	    
	  or terminate;
	    
        end select;
      end loop;
    end dispatcher;
  

  
  task body a1 is
    notify_event : e_kind;
    a_aps : AP_AR(1 .. 6) := (ap_b0, ap_b1, ap_b2, ap_b3, ap_b4, ap_b5);
    begin
      accept start_artist;
      mon.observe(a_aps, false); mon.observe(ap_b2, true); mon.trace;
      dispatcher.register_event(art1, e1);
      mon.observe(a_aps, false); mon.observe(ap_b3, true); mon.trace;
      dispatcher.register_event(art1, e2);
      artist_manager.a1_registered;
      loop
        select
  
          accept notify_client_event(event: e_kind) do
            notify_event := event;
            if event = e1 then 
              mon.observe(a_aps, false);
              mon.observe(ap_b0, true);
            else
              mon.observe(a_aps, false);
              mon.observe(ap_b1, true);
            end if;
            mon.trace;
          end notify_client_event;
  
          or
  
          accept terminate_artist do
            mon.observe(a_aps, false); mon.observe(ap_b4, true); mon.trace;
            dispatcher.unregister_event(art1, e1);
            mon.observe(a_aps, false); mon.observe(ap_b5, true); mon.trace;
            dispatcher.unregister_event(art1, e2);
            mon.observe(ap_b5, false); mon.trace;
          end terminate_artist;
          exit;
  
        end select;
      end loop;
    end a1;


  task body a2 is
    notify_event : e_kind;
        a_aps : AP_AR(1 .. 6) := (ap_c0, ap_c1, ap_c2, ap_c3, ap_c4, ap_c5);
    begin
      accept start_artist;
      mon.observe(a_aps, false); mon.observe(ap_c2, true); mon.trace;
      dispatcher.register_event(art2, e1);
      artist_manager.a2_registered;
      loop
        select
  
          accept notify_client_event(event: e_kind) do
            notify_event := event;
            if event = e1 then 
              mon.observe(a_aps, false);
              mon.observe(ap_c0, true);
            else
              mon.observe(a_aps, false);
              mon.observe(ap_c1, true);
            end if;
            mon.trace;
          end notify_client_event;
  
          or
  
          accept terminate_artist do
            mon.observe(a_aps, false); mon.observe(ap_c4, true); mon.trace;
            dispatcher.unregister_event(art2, e1);
            mon.observe(ap_c4, false); mon.trace;
          end terminate_artist;
          exit;
  
        end select;
      end loop;
    end a2;


  task body adt_wrapper is
    i : Integer := 0;
    begin
    
      accept start_wrapper;

      
      loop
        i := i + 1;
        
        dispatcher.notify_artists(choose);
        if i >  Integer'Value (Argument(1)) then artist_manager.stop_manager; exit; end if;
      end loop;
    end adt_wrapper;


  task body artist_manager is
    begin
    
      accept start_manager do
        a1.start_artist;
        a2.start_artist;
        accept a1_registered;
        accept a2_registered;
      end start_manager;
    
      accept stop_manager do 
        a1.terminate_artist;
        a2.terminate_artist;
        mon.observe(ap_d0, true);
        mon.trace;
        mon.stop;
      end stop_manager;
      
    end artist_manager;


  task body client_init is
    begin
      artist_manager.start_manager;
      adt_wrapper.start_wrapper;
    end client_init;
    


end disp;

