with monitor;
with Ada.Text_IO;                   use Ada.Text_IO;
with Ada.Integer_Text_IO;           use Ada.Integer_Text_IO;

  
package body monitor is
  

    function ID_Hashed (id: AP) return Hash_Type is
    begin
      return AP'pos(id);
    end ID_Hashed;
  

 
    
    task body mon is
        stopMonitoring : Boolean := false;  
    begin
      accept start do

        for atom in AP loop
          observations.Insert(atom, false);
        end loop;

        while stopMonitoring = false loop
          select 
            accept observe(obs: AP; v : Boolean) do
              observations.Replace(obs, v);
            end observe;
           or

            accept observe(obs: AP_AR; v :Boolean) do
              for ap in obs'Range loop
                observations.Replace(obs(ap), v);
              end loop;
            end observe;

           or
            accept trace do
              for atom in AP loop
                Put(",");
                Put(AP'Image(atom));
                Put(":");
                if observations.Element(atom) then
                   Put("t");
                else Put("f");
                end if;
                
              end loop;
              New_Line(1);
            end trace;
          or 
            accept stop do
               stopMonitoring := true;
            end stop;
          end select;
        end loop;
      end start;

    end mon;
    
    task body moninit is
    begin
      mon.start;
    end moninit;

  end monitor;
