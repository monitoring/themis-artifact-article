---
title: DIY Plots
---

## Generating Plots

These plots are intended for the tutorial experiment.
Make sure you complete the tutorial before plotting.
To create all relevant plots and LaTeX tables run

```
make
```

To cleanup execute

```
make clean
```
