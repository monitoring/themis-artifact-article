# Common Plotting
source("../common.r")

# Set up graphs
cols<- "OrRd"
leg <- "none"

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("data.dat", header = TRUE)

# Tidy Algorithms
dat$alg <- factor(dat$alg, 
              levels=c("Orchestration", "Migration", "MigrationRR", "Choreography"), 
              labels=c("Orch", "Migr", "Migrr", "Chor"))

# Tidy Traces
dat$tracedir <- factor(dat$tracedir, 
                  levels=c("tr-norm", "tr-bin", "tr-bet1", "tr-bet2"), 
                  labels=c("normal", "binomial", "beta-1", "beta-2"))

#dat$spec <- gsub("specs/chiron", "", dat$spec) 
#dat$spec <- gsub(".xml", "", dat$spec) 

#dat$spec <- factor(dat$spec, 
#                  levels=c("1", "2", "3", "5", "15a", "15b"))


# Basic Corrections for Metrics

# Adjust Min delay to 0 if there was no resolutions (metric will display max trace length)
dat$min_delay = ifelse(dat$resolutions == 0, 0, dat$min_delay)

# Adjust Length of Run, algorithms abort on a round + 1
dat$run_len   = ifelse(is.na(dat$verdict), dat$run_len, dat$run_len - 1)

# Generate Data Frame for metrics graphed

res<- dat %>% 
    group_by(alg, comps) %>%
  mutate(
    delay=zerodiv(sum_delay, resolutions)
  ) %>%
  summarise(
    delay_avg_1=mean(delay), delay_avg_2=sd(delay),
    msg_num_1=mean(zerodiv(msg_num,run_len)),  msg_num_2=sd(zerodiv(msg_num,run_len)),
    msg_data_1=mean(zerodiv(msg_data,run_len)),  msg_data_2=sd(zerodiv(msg_data,run_len)),
    simpm_1=mean(simp_maxmon), simpm_2=sd(simp_maxmon),
    simpc_1=mean(zerodiv(simp_maxmon_crit,run_len)), simpc_2=sd(zerodiv(simp_maxmon_crit,run_len)),
    conve_1=mean(zerodiv(exp_conv_comp,run_len)),  conve_2=sd(zerodiv(exp_conv_comp,run_len)),
    len=mean(run_len),
    verdictf=getverdict(verdict)
  ) %>%
  mutate(
    delay_avg=displaystat(delay_avg_1, delay_avg_2),
    msg_num=displaystat(msg_num_1, msg_num_2),
    msg_data=displaystat(msg_data_1, msg_data_2),
    #      simpt=displaystat(simpt_1, simpt_2),
    simpm=displaystat(simpm_1, simpm_2),
    simpc=displaystat(simpc_1, simpc_2),
    #    expt=displaystat(expt_1, expt_2),
    conve=displaystat(conve_1, conve_2)
  ) %>%
  select(alg, comps, 
         #verdictf, 
         #delay_max, 
         delay_avg, 
         #mdelay, 
         msg_num, msg_data, 
         #simpt, 
         simpm, 
         simpc,
         #expt, 
         #expm, 
         #convs, 
         conve
         #len
  )

message("Writing table to table.tex")
print(xtable(res), file="table.tex", include.rownames=FALSE)

# cor <- dat %>% 
#       mutate(
#         vard=sum_delay,
#         vars=simp_total
#       ) %>%
#       select(alg,vard,vars)

# cor <- dat %>%
# #        filter(alg %in% c("Migr", "Migrr")) %>%
#        filter(resolutions > 0 & run_len > 0) %>%
#        mutate(
#          vard=sum_delay/resolutions,
#          vars=simp_maxmon_crit/run_len
#        ) %>%
#        select(alg,vard,vars)
# 
# gdraw(ggplot(cor, aes(x=vard, y=vars))) +geom_point(shape=1,  color="darkred") + #geom_smooth(method="loess") +
#   labs(x="Average Delay", y="Simplifications (Critical)") + 
#   facet_wrap(~ alg, scales="free", ncol=2) 
# 
# summary(lm(vard ~ vars, data = cor %>% filter(alg=="Migr")))



#tp <- dat %>%
#    filter(alg=="Migr" | alg=="Migrr") %>%
#    group_by(alg, spec) %>%
#    summarize(msg_data = mean(msg_data/run_len))


#gdraw(ggplot(tp, aes(x=spec,  y= msg_data, fill=alg))+ 
#        geom_bar(stat="identity", position="dodge") ) + scale_fill_brewer(palette = "Set1") + 
#  geom_text(aes(label=sprintf("%.0f", msg_data)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
#  labs(x="Specification", y="Data") 

# Generate Metric Plots
themis_plot(dat, "zerodiv(msg_num, run_len)", "#Msgs  (Normalized)",  type=themis_box_nonotch, pal=cols, legend=leg) + coord_cartesian(ylim=c(0,8.5)) + ggsave("msgnum.pdf")
themis_plot(dat, "zerodiv(msg_data, msg_num)", "Data per Message", pal=cols, legend=leg) + coord_cartesian(ylim=c(0,420)) + ggsave("msgdatanorm.pdf")
#themis_plot(dat, "zerodiv(simp_total, run_len)", "Simplifications/Round  (Normalized)", pal=cols, legend=leg)  + coord_cartesian(ylim=c(0,65)) + ggsave("compall.pdf")
themis_plot(dat, "simp_maxmon", "Maximum Simplifications per Monitor (Worst-Case)", pal=cols, legend=leg)  + coord_cartesian(ylim=c(0,50)) + ggsave("compmon.pdf")
#themis_plot(dat, "zerodiv(exp_maxmon, run_len)", "Max Evaluations/Monitor  (Normalized)", pal=cols, legend=leg)  + coord_cartesian(ylim=c(0,70)) + ggsave("compmonexpr.pdf")


themis_plot(dat, "zerodiv(simp_maxmon_crit, run_len)", "Critical Simplifications (Normalized)", pal=cols, legend=leg)  + 
  coord_cartesian(ylim=c(0,30)) + 
  ggsave("compcrit.pdf")

#themis_plot(dat, "zerodiv(exp_maxmon_crit, run_len)", "Evaluations (Critical)", pal=cols, legend=leg) +
#  coord_cartesian(ylim=c(0,250)) +
#  ggsave("evalcrit.pdf");


themis_plot(dat, "zerodiv(sum_delay, resolutions)", "Average Delay",
            pal=cols, type=themis_box_nonotch, legend="top")   + 
  coord_cartesian(ylim=c(0,5)) + 
  theme(legend.text=element_text(size=20)) +
  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal") + 
  ggsave("delay.pdf")

themis_plot(dat, "zerodiv(msg_data, run_len)", "Data  (Normalized)", 
            pal=cols) + 
  coord_cartesian(ylim=c(0,420)) + 
  theme(legend.text=element_text(size=20)) +
  theme(legend.position = c(0.5, 0.95), legend.direction = "horizontal") + 
  ggsave("msgdata.pdf")


#themis_plot(dat, "max_delay", "Max Delay", pal=cols,type=themis_box_nonotch)  + coord_cartesian(ylim=c(0,7.5))
themis_plot(dat, "zerodiv(exp_conv_comp, run_len)", "Convergence", pal=cols, legend=leg, type=themis_box_nonotch)+
  ggsave("convergence.pdf")

# Compare data across traces


# Generate data frame
restrace <- dat %>% 
  filter(comps==6) %>%
  group_by(alg, tracedir) %>%
  mutate(
    delay=zerodiv(sum_delay, resolutions)
  ) %>%
  summarise(
    delay_max=max(delay),
    delay_avg_1=mean(delay), delay_avg_2=sd(delay),
    mdelay_1=mean(max_delay), mdelay_2=sd(max_delay),
          msg_num_1=mean(msg_num/run_len),  msg_num_2=sd(msg_num/run_len),
          msg_data_1=mean(msg_data/run_len),  msg_data_2=sd(msg_data/run_len),
          simpt_1=mean(simp_total/run_len), simpt_2=sd(simp_total/run_len),
          simpm_1=mean(simp_maxmon), simpm_2=sd(simp_maxmon),
          simpc_1=mean(zerodiv(simp_maxmon_crit, run_len)), simpc_2=sd(zerodiv(simp_maxmon_crit, run_len)),
          expt_1=mean(exp_total/run_len), expt_2=sd(exp_total/run_len),
          expm_1=mean(exp_maxmon), expm_2=sd(exp_maxmon),
    convs_1=mean(simp_conv_comp/run_len),  convs_2=sd(simp_conv_comp/run_len),
    conve_1=mean(exp_conv_comp/run_len),  conve_2=sd(exp_conv_comp/run_len),
    len=mean(run_len)
    #     verdictf=getverdict(verdict)
  ) %>%
  mutate(
    delay_avg=displaystat(delay_avg_1, delay_avg_2),
    delay_max=sprintf("%.2f", delay_max),
    mdelay=displaystat(mdelay_1, mdelay_2),
        msg_num=displaystat(msg_num_1, msg_num_2),
        msg_data=displaystat(msg_data_1, msg_data_2),
        simpt=displaystat(simpt_1, simpt_2),
        simpm=displaystat(simpm_1, simpm_2),
        simpc=displaystat(simpc_1, simpc_2),
        expt=displaystat(expt_1, expt_2),
        expm=displaystat(expm_1, expm_2),
    convs=displaystat(convs_1, convs_2),
    conve=displaystat(conve_1, conve_2)
  ) %>%
  select(alg, tracedir, 
         #verdictf, 
         #delay_max, delay_avg, 
         mdelay, 
         msg_num, msg_data, 
         #simpt, 
         #simpm,
         simpc,
         #expt, expm, 
         #convs, 
         #len,
         conve
    )
# Generate table
message("Writing table to tabletrace.tex")
print(xtable(restrace), file="tabletrace.tex", include.rownames=FALSE)
# 
# # Plot data
# themis_plot(dat, "zerodiv(msg_num, run_len)", "#Msgs  (Normalized)", 
#             myf="factor(tracedir)", labf="Trace", myx="factor(alg)", labx="Algorithm", pal=cols, legend="top") + coord_cartesian(ylim=c(0,8.5)) + 
#         ggsave("tr_msgnum.pdf")
# 
# themis_plot(dat, "zerodiv(msg_data, run_len)", "Data  (Normalized)", 
#             myf="factor(tracedir)", labf="Trace", myx="factor(alg)", labx="Algorithm", pal=cols, legend=leg) + coord_cartesian(ylim=c(0,500)) + 
#   ggsave("tr_msgdata.pdf")
# 
# themis_plot(dat, "simp_maxmon", "Max Simplifications/Monitor  (Worst-Case)",
#             myf="factor(tracedir)", labf="Trace", myx="factor(alg)", labx="Algorithm", pal=cols, legend=leg)  + 
#   coord_cartesian(ylim=c(0,45)) + 
#   ggsave("tr_compmon.pdf")
# 

old <-read.csv("old.dat", header = TRUE)
# Tidy Algorithms
old$alg <- factor(old$alg, 
                  levels=c("Orchestration", "Migration", "MigrationRR", "Choreography"), 
                  labels=c("Orch", "Migr", "Migrr", "Chor"))

olds <- old %>% 
        filter(alg %in% c("Migr", "Migrr")) %>%
        group_by(alg, comps) %>%
        summarize(
          y=mean(zerodiv(msg_data, msg_num))
        )  %>%
        mutate(
          v = "ISSTA'17",
          x = sprintf("%s-%d", alg, comps)
        ) %>%
        select(alg, comps, v, x,y)

news <- dat %>%
    filter(alg %in% c("Migr", "Migrr") & comps < 6) %>%
    group_by(alg, comps) %>%
    summarize(
      y=mean(zerodiv(msg_data, msg_num))
    )  %>%
    mutate(
      v = "Current",
      x = sprintf("%s-%d", alg, comps)
    ) %>%
    select(alg, comps, v, x,y)



gdraw(ggplot(rbind(olds, news), aes(x=x,  y=y, fill=v))+ 
        geom_bar(stat="identity", position="dodge") + 
        geom_text(aes(label=sprintf("%.2f", y)), vjust=-1, color="black", position = position_dodge(0.9), size=2.3)+
        guides(fill=guide_legend(title="Version"))
) + 
  theme(strip.text.x = element_blank()) + 
  theme(legend.position="right") + 
  scale_y_log10() +
  scale_fill_brewer(palette = cols) +
  labs(x="Algorithm", y="Size of the EHE (log scale)") +
  ggsave("ehesize.pdf")
