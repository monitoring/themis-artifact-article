# Common Plotting
source("../common.r")

# Set up graphs
cols<- "OrRd"
#cols<- "Dark2"
leg <- "none"

# Read data and process it by doing basic replacements and values ordering
dat<-read.csv("data.dat", header = TRUE)

# Tidy Algorithms
dat$alg <- factor(dat$alg, 
              levels=c("Orchestration", "Migration", "MigrationRR", "Choreography"), 
              labels=c("Orch", "Migr", "Migrr", "Chor"))

# Tidy Traces
dat$tracedir <- factor(dat$tracedir, 
                  levels=c("tr-norm", "tr-bin", "tr-bet1", "tr-bet2"), 
                  labels=c("normal", "binomial", "beta-1", "beta-2"))

#dat$spec <- gsub("specs/chiron", "", dat$spec) 
#dat$spec <- gsub(".xml", "", dat$spec) 

#dat$spec <- factor(dat$spec, 
#                  levels=c("1", "2", "3", "5", "15a", "15b"))


# Basic Corrections for Metrics

# Adjust Min delay to 0 if there was no resolutions (metric will display max trace length)
dat$min_delay = ifelse(dat$resolutions == 0, 0, dat$min_delay)

# Adjust Length of Run, algorithms abort on a round + 1
dat$run_len   = ifelse(is.na(dat$verdict), dat$run_len, dat$run_len - 1)
gdraw(ggplot(data=dat, aes(dat$run_len)) + geom_histogram(aes(fill=dat$verdict), binwidth = 1)+
        scale_fill_brewer(palette = cols, na.value="black", name="Verdict")+
        xlab("Run Length") + ylab("Count")) +  theme(legend.position="bottom") + ggsave("hist.pdf") # theme(legend.position=c(0.1,0.9)) + ggsave("hist.pdf")

gdraw(ggplot(data=dat, aes(dat$run_len)) + geom_histogram(aes(fill=dat$verdict), binwidth = 1)+
        scale_fill_brewer(palette = cols, na.value="black", name="Verdict")+
        xlab("Run Length") + ylab("Count") +
        facet_grid(reformulate("factor(dat$tracedir)")) ) +  theme(legend.position ="bottom") + ggsave("hist-dist.pdf")

