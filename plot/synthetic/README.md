---
title: Synthetic Plots
---

## Dependencies

The synthetic plots require that `merge.db` be created in [experiments/syn](../../experiments/syn).

If you have not already unpacked the data do the following

```
cd ../../experiments/syn
make unpack
make merge
cd ../../plot/synthetic
```

## Generating Plots

To create all relevant plots and LaTeX tables run

```
make
```

To cleanup execute

```
make clean
```
