---
title: Chiron Plots
---

## Generating Plots

To create all relevant plots and LaTeX tables run

```
make
```

To cleanup execute

```
make clean
```
