---
title: THEMIS Trace Generator
---

## Usage

A tool to generate a decentralized trace of events.

Uses [Colt](https://dst.lbl.gov/ACSSoftware/colt/) for randomization engines and probability distributions.


Usage:

```
java -jar /path/to/jar <profile> <length> <n components> <observations per component> <directory> [<prefix>]
```

* **profile**: Profile for the distribution, this configures the probability distribution using Colt objects
* **length**: Length of the trace
* **n components**: The number of components. 1 file is generated per component.
* **observations per component**: Specifies the number of observations per event for each component
* **directory**: Directory used to store the generated files
* **prefix**: An optional prefix for the filename

Components are named starting with `a`.

Observations are prefixed by the component name and followed by an index starting at 0.

The verdict treshhold is set at 0.5, values <= 0.5 are *false* while those greater than are *true*.
The environment variable **VERDICT_INVERT** can be set to true to invert the verdicts for T/F.

## COLT Profile

A COLT profile is an XML document that describes the probability distribution.
It has the typical layout:

```$xslt
<generator>
    <engine seed="%seed" type="%stype">%ecls</engine>
    <distribution cls="%dcls">
        <param type="%ptype">%pvalue</param>
        ...
    </distribution>
</generator>
```

With the following:

* **%seed**: An integer value to use as the seed for the random engine.
* **%stype**: The type of the seed
    * **NONE**: Do not seed
    * **NOW**: Seed with current time (`Date.getTime()`)
    * **INT**: Seed with an integer which will be the value in **%seed**
* **%ecls**: Class name for the random engine, must extend [cern.jet.random.engine.RandomEngine](https://dst.lbl.gov/ACSSoftware/colt/api/cern/jet/random/engine/RandomEngine.html)
* **%dcls**: Class for the probability distribution, must extend [cern.jet.random.AbstractDistribution](https://dst.lbl.gov/ACSSoftware/colt/api/cern/jet/random/AbstractDistribution.html)

The list of `<param>` are used to load the probability distribution, they represent the constructor arguments invoked.
 
* **%ptype**: Type of parameter (INT or DOUBLE)
* **%pvalue**: Value of the parameter

For examples, see [example](example/).

