package uga.corse.themis.tools;



import cern.jet.random.AbstractDistribution;
import cern.jet.random.engine.RandomEngine;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.PrintWriter;
import java.util.*;

public class Generator {
    public interface Parse {
        Object parse(String s);
    }
    public enum TSEED {NONE, NOW, INT}
    public enum TPARAM{
        INT     (int.class,     (x) -> Integer.parseInt(x)),
        DOUBLE  (double.class,  (x) -> Double.parseDouble(x));

        public Class cls;
        public Parse func;

        TPARAM(Class cls, Parse func) {
            this.cls = cls;
            this.func = func;
        }

    }

    public static void main(String[] args) {
        if(args.length < 5) {
            usage(null);
        }


        try {
            Integer length = Integer.parseInt(args[1]);
            Integer ncomps = Integer.parseInt(args[2]);
            Integer nobs = Integer.parseInt(args[3]);

            File path = null;
            try {
                path = new File(args[4]);
            }
            catch(Exception e) {
                usage(e.getLocalizedMessage());
            }
            if(!path.isDirectory())
                usage(path.getAbsolutePath() + " is not a directory");
            if(!path.canWrite())
                usage(path.getAbsolutePath() + " is not writable");


            AbstractDistribution dist = loadProfile(args[0]);

            if(dist == null)
                usage("Failed to load profile: " + args[0]);

            boolean invert = false;
            String toInvert = System.getenv("VERDICT_INVERT");
            if(toInvert != null)
                invert = Boolean.parseBoolean(toInvert);

            generate(length, ncomps, nobs, path, args.length > 5 ? args[5] : "", dist, invert);

        } catch (NumberFormatException e) {
            usage("Wrong Format  " + e.getLocalizedMessage());
        } catch (DocumentException exc) {
            usage("Could not load profile: " + exc.getLocalizedMessage());
        }

    }

    public static AbstractDistribution loadProfile(String path) throws DocumentException {

        SAXReader reader = new SAXReader();
        Document document = reader.read(path);
        Element root = document.getRootElement();


        Element engineElem = root.element("engine");
        Element dist   = root.element("distribution");


        RandomEngine engine = loadEngine(engineElem);
        if(engine == null) return null;

        return loadDist(dist, engine);

    }
    private static AbstractDistribution loadDist(Element dist, RandomEngine engine) {

        AbstractDistribution distInstance = null;
        try {
            String clsName = dist.attributeValue("cls");

            Class<AbstractDistribution> cls = (Class<AbstractDistribution>) Generator.class.getClassLoader().loadClass(clsName);

            List<Class> params = new LinkedList<>();
            List<Object> pvalue = new LinkedList<>();

            Iterator<Element> init = dist.elementIterator("param");
            while (init.hasNext()) {
                Element param = init.next();
                TPARAM ptype = TPARAM.valueOf(param.attributeValue("type"));
                String arg = param.getText();

                params.add(ptype.cls);
                pvalue.add(ptype.func.parse(arg));
            }
            params.add(RandomEngine.class);
            pvalue.add(engine);

            distInstance = cls.getDeclaredConstructor(params.toArray(new Class[params.size()]))
                    .newInstance(pvalue.toArray(new Object[pvalue.size()]));
            return distInstance;

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }

    }
    private static RandomEngine loadEngine(Element engine)   {

        try {
            String enginecls = engine.getText();
            String seed = engine.attributeValue("seed");
            String type = engine.attributeValue("type");

            TSEED stype = TSEED.valueOf(type);

            Class<RandomEngine> cls = (Class<RandomEngine>) Generator.class.getClassLoader().loadClass(enginecls);

            if (cls == null)
                return null;

            RandomEngine engineInstance = null;
            switch (stype) {

                case NOW:
                    if (!seed.isEmpty()) return null;
                    Date d = new Date();
                    engineInstance = cls.getConstructor(int.class).newInstance((int) d.getTime());
                    break;
                case INT:
                    engineInstance = cls.getConstructor(int.class).newInstance(Integer.parseInt(seed));
                    break;
                case NONE:
                    engineInstance = cls.newInstance();
                    break;

            }
            return engineInstance;
        } catch (Exception e) {
            return null;
        }

    }

    public static boolean generate(Integer length, Integer ncomps, Integer nobs, File path, String prefix, AbstractDistribution dist, boolean inverted) {
        PrintWriter out;
        try {
            for (int i = 0; i < ncomps; i++) {
                String comp = comp(i);
                out = new PrintWriter(new File(path, prefix + comp + ".trace"));


                for(int n = 0; n < length; n++) {
                    for(int k = 0; k < nobs; k++) {
                        int b = dist.nextInt() % 2;
                        if( k > 0) out.print(",");
                        if(!inverted)
                            out.print(comp + k + ":" + (b == 1 ? "t" : "f"));
                        else
                            out.print(comp + k + ":" + (b == 1 ? "f" : "t"));
                    }
                    out.println();
                }

                out.close();
            }

        } catch (Exception e) {
            e.printStackTrace(System.err);
            return false;
        }

        return true;
    }
    public static String comp(int n) {
        return ((char) ('a' + n)) + "";
    }

    private static void usage(String err) {
        if(err != null)
            System.err.println(err);
        System.err.println("Usage: <profile> <length> <ncomponents> <observations per component> <directory>");
        System.exit(1);
    }
}
