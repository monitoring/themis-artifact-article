select  b1.tid, b1.spec, b2.alg, b1.verdict as "ref_verdict", b2.verdict as "alg_verdict", b1.run_len as "ref_run", b2.run_len as "alg_run"
  From bench as b1
  INNER join bench as b2 on b2.spec == b1.spec AND b1.tid == b2.tid
where b1.alg == "Orchestration" AND b2.alg like "Migration"
  AND b1.verdict <> b2.verdict

 union

select  b1.tid, b1.spec, b2.alg, b1.verdict as "ref_verdict", b2.verdict as "alg_verdict", b1.run_len as "ref_run", b2.run_len as "alg_run"
  From bench as b1
  INNER join bench as b2 on b2.spec == b1.spec AND b1.tid == b2.tid
where b1.alg == "Orchestration" AND b2.alg like "MigrationRR"
  AND b1.verdict <> b2.verdict

  union
  
select  b1.tid, b1.spec,  b2.alg, b1.verdict as "ref_verdict", b2.verdict as "alg_verdict", b1.run_len  as "ref_run", b2.run_len as "AlgRun"
  From bench as b1
  INNER join bench as b2 on b2.spec == b1.spec AND b1.tid == b2.tid
where b1.alg == "Orchestration" AND b2.alg=="Choreography"
  AND b1.verdict <> b2.verdict
  
  ORDER by b1.spec ASC;
