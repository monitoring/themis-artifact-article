# Gernerate Random Traces

This script generate random traces for usage with THEMIS using the [themis-generator](../../../generator) tool.

## Usage

```
./themis_gentrace ntraces trace_length ncomps nobspercomp profile out [keep]
```

* ntraces : number of traces to generate
* trace_length : length of the trace (in events)
* ncomps : number of components in the trace (this creates one file per component, named from a-z)
* nobspercomp : number of observations per component (this is the number of atomic propositions per event)
* profile : profile for the generator, this determines the probability distribution and random engine for the trace (see [themis-generator](../../../generator)  for more information). Example profiles are provided in [profiles](profiles/).
* out : output director to store traces (also name of the tar file)
* keep : if set to any value the director with generated traces is kept and not tar-ed

The generation of traces calls the THEMIS generator tool **ntraces** amount of times for each trace.
Each execution generates a file per component, in the file each event is laid out on a line.

## Example

```
./themis_gentrace 3 5 3 2 profiles/binomial.profile traces
```

This creates 3 traces containing 5 events each.
The traces will provide events for 3 components, each event containing 2 observations (2 Atomic propositions).
For randomness, the trace will use a [binomial distribution](profiles/binomial.profile).
Traces are saved in a folder called **traces**, in the tar **traces.tgz**.
