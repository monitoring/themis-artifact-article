# Generate Random LTL Formulae

These scripts generate random LTL formulae using [randltl](https://spot.lrde.epita.fr/randltl.html) from [Spot](https://spot.lrde.epita.fr/index.html).

## Usage

To generate a set of formulae compatible with Themis simply run the **create** script

```
./themis_genspec nf nc no template out [keep]
```

* nf : number of formulae to generate
* nc : number of components (components are labeled a-z) [so maximum is 19]
* no : number of observations per component (this will be used to generate the literals, a number that suffixes the component letter)
* template : the template file to use for generating the specification, which contain the string `%form%` which will be replaced by the formula (see [templates](templates/))
* out : the output folder name to store the specification files
* keep : if this is set to anything the directory structure will be kept, otherwise it will be tar-ed and deleted

The above script uses various smaller scripts:

* [themis_randltl](themis_randltl) : creates the formulas by repeatedly calling randltl (to generate unique formulas)
* [themis_ltlfilter](themis_ltlfilter) : removes formulae that reference less components than those provided ( < nc )
* [themis_mkspec](themis_mkspec) : converts the lines of formulas generated to THEMIS distributed specifications and necessary structures for the experiment tool.


## Example

```
./themis_genspec 100 3 2 templates/ltl.xml spec
```

Generate 100 formulae that reference 3 components, with 2 possible observations per component.
Atomic propositions will include values such as a0, a1, b0, b1, c0,and c1.
The generated formulae will then be converted to THEMIS specifications using the [LTL template](templates/ltl.xml), and will be stored in the folder **spec** in the tar **spec.tgz** and deleted.

