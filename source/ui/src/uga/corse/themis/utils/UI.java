package uga.corse.themis.utils;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import org.graphstream.ui.view.ViewerPipe;
import uga.corse.themis.algorithms.choreography.MonitorNetwork;
import uga.corse.themis.algorithms.choreography.MonitorNetwork.NetNode;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.State;
import uga.corse.themis.automata.Transition;
import uga.corse.themis.automata.Verdict;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Map.Entry;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * Utility functions for drawing things with graphstream
 * @author Antoine El-Hokayem <antoine.el-hokayem@imag.fr>
 */
public class UI {
  
	private static Graph getGraph(String title) {
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		StringBuilder content = new StringBuilder();

		content.append(new Scanner(UI.class.getResourceAsStream("graph.css"))
					.useDelimiter("\\Z").next());

		try {
			content.append(new Scanner(new File("graph.css")).useDelimiter("\\Z").next());
		} catch (FileNotFoundException e) {
			
		}
		Graph graph = new MultiGraph(title);
		graph.addAttribute("ui.stylesheet", content.toString());
		graph.addAttribute("ui.antialias");
		return graph;
	}
	public static Viewer draw(MonitorNetwork net) 
	{
		Graph graph = getGraph(net.root.spec.toString());
		
		for(NetNode node : net.nodes.values()) {
			String text = "<" + node.id + ", " + node.comp + ">";
			Node n = graph.addNode("mon" + node.id);
			n.addAttribute("ui.label", text);
			n.addAttribute("ui.class", node == net.root ? "init" : "");
		}
		for(Entry<Integer, java.util.Set<Integer>> entry : net.edges.entrySet()){
			Integer from = entry.getKey();
			for(Integer to : entry.getValue()) {
				graph.addEdge(from + "-" + to, "mon" + from, "mon" + to, true);
			}
		}


        return graph.display();
	}
	
	public static Viewer draw(Automata aut, String title)
	{
		Graph graph = getGraph("Automata");
		for(Entry<State, Verdict> entry : aut.getStates().entrySet())
		{
			String s = entry.getKey().toString();
			Node n = graph.addNode(s);
			n.addAttribute("ui.label", s);
			n.addAttribute("ui.class", ((aut.getInitialState().toString().equals(s)) ? "init, " : "") + 
					entry.getValue().toString());
		}
		for(Entry<State, List<Transition>> entry : aut.getDelta().entrySet())
		{
			for(Transition tau : entry.getValue())
			{
				String from  = tau.getFrom().toString();
				String to	 = tau.getTo().toString();
				String label = tau.getLabel().toString();
				Edge e = graph.addEdge(from + "-" + to + ": " + label, from, to, true);
				e.addAttribute("ui.label", label);
			}
		}


		Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
		View view = viewer.addDefaultView(false);


		JFrame frame = new JFrame(title);
		frame.add((javax.swing.JComponent) view);


		//ViewerPipe fromViewer = viewer.newViewerPipe();
		//fromViewer.addViewerListener(this);
		//fromViewer.addSink(graph);
        frame.setPreferredSize(new Dimension(500,500));
        viewer.enableAutoLayout();

		frame.pack();
		frame.setVisible(true);

        //viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.EXIT);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		return viewer;
	}
}
