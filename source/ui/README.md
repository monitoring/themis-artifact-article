---
title: THEMIS UI Tools
---

## Drawing

Use the **Draw** tool to draw monitors, this requires **ltl2mon** to be installed.

```
java -cp /path/to/jar uga.corse.themis.tools.Draw [OPTIONS]

```

> When THEMIS is installed, the script `themis-draw` is a shorthand.
>
> Drawing may not work inside the docker

You must provide one of the following options based on your input:

* **-ltl** : String LTL Formula 
* **-ltlf** : Path to File with formula on the first line
* **-spec** : Path to THEMIS Specification File (XML) 

Once a specification is provided, you can provide the additional options:

* **-net** : Generate the decentralized specification (by splitting the LTL formula) using the Choreography heuristic
* **-expr** : Generate simplified delta expressions (the automaton's transition will be minimal expressions)
* **-only** : Draw only specs (in case of **-spec**) with provided keys (comma separated) 
* **-simp** : Apply LTL simplification to input formula (for **-ltl** and **-ltlf**)

