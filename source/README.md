---
title: THEMIS Tool
---

## Installing

If you wish to simply install tool go to [dist](dist/).
The distribution folder will bundle everything in a folder that can be referenced by an environment variable, add the `bin` path of the resulting folder to your $PATH to access all tools.

> THEMIS is already installed in the docker

## Exploring Modules

Each folder contains a module for THEMIS:

* [core](core/) contains the core package of functionality for the THEMIS framework, includes metrics, algorithms and basic tools for executing monitoring
* [experiment](experiment/) contains the module to carry out experiments
* [generator](generator/) contains the module responsible for generating traces
* [ui](ui/) contains the module responsible for drawing specifications
* [scripts](scripts/) contains utility scripts to simplify various tasks
