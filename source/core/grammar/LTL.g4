grammar LTL;

@header{ package uga.corse.themis.parser.backend; }

ltlexpr
	: '(' ltlexpr ')'		 #LTL_PAREN
	| OPNEG ltlexpr			 #LTL_NEG
	| OPG ltlexpr		 	 #LTL_GLOBAL
	| OPF ltlexpr			 #LTL_FINALLY
	| OPX ltlexpr			 #LTL_NEXT
	| ltlexpr OPAND ltlexpr  #LTL_AND
	| ltlexpr OPOR ltlexpr	 #LTL_OR
	| ltlexpr '<->' ltlexpr  #LTL_EQUIV
	| ltlexpr '->' ltlexpr	 #LTL_IMPLIES
	| ltlexpr 'U' ltlexpr	 #LTL_UNTIL
	| ltlexpr OPR ltlexpr	 #LTL_RELEASE
	| STRING		 	     #LTL_SYM
	;



atom 
  : STRING
  ;


 
OPNEG
  : '!'
  | '-'
  | '~'
  ;

OPG
  : '[]'
  | 'G'
  ;
  
OPF
  : '<>'
  | 'F'
  ;
 
OPX
  : 'X'
  | '()'
  ;
OPAND
  : '&'
  | '&&'
  ;
 
 OPOR
  : '|'
  | '||'
  ;
 
 OPR
  : 'R'
  | 'V'
  ;
 
  
STRING 
	: LETTER (LETTER | DIGIT)*
	;
INT
	: DIGIT+
	;


fragment LETTER
   : [a-z]
   ;

fragment DIGIT
   : [0-9]
   ;


WS
   : [ \t\n\r]+ -> skip
   ;
