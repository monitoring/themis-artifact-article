---
title: THEMIS Core
---

### Overview

The purpose of the THEMIS framework is to minimize the overhead of designing and assessing decentralized monitoring algorithms.
THEMIS provides an API for monitoring and necessary datastructures to load, encode, store, exchange, and process observations, as well as manipulate specifications and traces.
These basic building blocks can be reused or extended to modify existing algorithms or design new more intricate algorithms.
To assess the behavior of an algorithm, the THEMIS framework provides a base set of metrics (such as messages exchanged and their size, along with computations performed), but also allows for the definition of new metrics by using the API or by writing custom AspectJ instrumentation.
These metrics can be used to assess existing algorithms as well as newly developed ones.
Once algorithms and metrics are developed, it is possible to use existing tools to perform monitoring runs with the algorithm and metrics, or full experiments.
Experiments are used to define sets of parameters, traces and specifications.
Experiments can be shared and re-run separately, including or excluding new measures and algorithms.
After running a single run or an experiment, the metrics are stored in a database for postmortem analysis.
These can be queried, merged or plotted easily using third-party tools.
After completing the analysis, algorithms and metrics can be tuned so as to refine the design as necessary.

![Overview Image](overview.png)


### Building ###
This project is build with [Maven](https://maven.apache.org/), to build the project run: (~ 2-15 minutes)
```bash
make install
```
This will generate a `JAR` file in `target/` called `themis-core-0.0.2.jar`.
It will also install the jar into your local maven repository.

### Notes on Packaging ###

* Themis is fully packaged in one JAR for convenience
* The JAR includes all (java) dependencies and the `aspectweaver.jar`, it therefore acts as a java agent by using the [AspectJ agent for LTW](http://www.eclipse.org/aspectj/doc/released/devguide/ltw.html)

### Dependencies ###
#### Java Libraries ####

* [Antlr4](http://www.antlr.org/) for the parsers.
* [SQLite](https://www.sqlite.org/) for databases.
* [AspectJ](http://www.eclipse.org/aspectj/) for our instrumentation (require both Runtime and Weaver Agent).
* [dom4j](https://dom4j.github.io/) for XML parsing.

#### External Tools ####

* [LTL3Tools](http://ltl3tools.sourceforge.net/) for converting LTL formulae into monitor automata.
* [Spot](https://spot.lrde.epita.fr/)  (Particularly: [ltlfilt](https://spot.lrde.epita.fr/ltlfilt.html)) for minimizing LTL and boolean formulae.

* Make sure that Spot is installed.
* `ltl2mon` requires that the path to its script be known, the script will attempt to auto-discover the path using `readlink` and `dirname` commands, but if those are missing please set the environment variable `LTL2MON_HOME` to `ltl3tools-0.0.7` folder.

### Running the Tools ###
To run a given tool simply load the resulting jar on the classpath and specify it as a java agent (for LTW):
```bash
java -cp target/themis-core-0.0.2.jar -javaagent:target/themis-core-0.0.2.jar [tool]
```
Where [tool] is one of the following (see [tools](source/src/uga/corse/themis/tools)):

* `uga.corse.themis.runtime.Runtime` : sets up a THEMIS node given a host name and a port.

* `uga.corse.themis.tools.Run` : executes a monitoring algorithm on a trace using one specification on a given node.

> If you have THEMIS installed, the scripts: `themis-node` and `themis-run` are shorthand scripts to execute the node, and the run tool.


#### The RUN Tool ####
The `Run` tool requires a node to be active, to start a node simply run:

```bash
themis-node <HOST> <PORT>
```

Make sure that the node is run in the same directory as the `Run` tool, if you are using relative paths in the parameters.

The common tool to test THEMIS is the `Run` tool, we describe its basic parameters.

```bash
themis-run -nc <INT> -nr <INT> -in <PATH> -spec <PATH> -alg <CLASS> -tid <INT> -tmax <INT> -node <NODE> -listen <NODE>
```

* -nr : Length of the Run
* -nc : Number of components
* -in : Directory to read traces (on the node)
* -spec : Spec file
* -alg : Full java classname of the algorithm to use (example: uga.corse.themis.algorithms.orchestration.Orchestration). The class must extend [StandardMonitoring](src/uga/corse/themis/bootstrap/StandardMonitoring.java) and exist on the *classpath* of the program
* -tid : Trace ID to read
* -tmax : [Optional] Trace ID to read last, if specified, the traces from tid to tmax will be ran
* -node : [Optional] Specifies the node address to run the algorithm on, defaults to *localhost:8056*.
* -listen: [Optional] Specifies the node address to notify when the execution is complete, defaults to *localhost:8090*.


### Specifications ###
A top level specification is by default a decentralized specification, that is: it is multiple specifications.
Specifications are passed to a monitoring algorithm as a `Map<String, Specification>`, where each sub-specification is identified by a key.
Each sub-specification must provide the following attributes:

* id : a string name for the specification, this is used by the algorithms when reading the spec, the provided algorithms use the id **root** to denote the main specification.
* class : a string representing a full java class name of the specification class. The provided class must implement [Specification](src/uga/corse/themis/monitoring/specifications/Specification.java) and exist on the **classpath** of the program.

A specification can provide additional operations tags, these tags are used to setup the specification object.
Each tag name represents a method invocation **class**, the method must have the following signature: `void method(String)` the values in the tag are passed to the method.

A general specification has the following structure:
```xml
<specifications>
  <specification id="ID" class="CLASS">
    ... OPS
  </specification>
  ...
</specifications>
```

An example of an LTL specification:
```xml
<specifications>
  <specification id="root" class="uga.corse.themis.monitoring.specifications.SpecLTL">
       <setLTL><![CDATA[XXXX(!a0 | (b1 U G(a0 & b0 & c0)))]]></setLTL>
  </specification>
</specifications>
```
This would cause the following to happen:

* A specification object of type [SpecLTL](src/uga/corse/themis/monitoring/specifications/SpecLTL.java) is initialized.
* The method `setLTL` is invoked with the formula passed as string.
* The resulting object is added to the map with the key `root`


### Traces ###
The provided tools and algorithms use a simple format for the components and trace.
This behavior can be extended or modified as necessary.
The components are named alphabetically starting with `a` (for example: a, b, c).
The observations bound to the components are prefixed by the component and followed with a number starting from `0` (for example: a0, a1, a2, b0, b1).
The trace format consists of multiple files, prefixed by the trace ID and suffixed by the component name.
For example, a decentralized trace for two components `a` and `b`, consists of two files: `1-a.trace` and `1-b.trace`.
Each line in the file consists of an event, a comma separated sequence of atomic proposition followed by a colon followed by `t` or `f` indicating whether or not it was observed.

An example trace file `1-a.trace` for a trace of length 3  would have the following contents:
```
a0:t,a1:f
a0:f,a1:f
a0:t,a1:t
```



### Environment Variables ###

#### Core ####

* `THEMIS_SPOT_LTLFILT` contains the path or name of `ltlfilt` used to minimize LTL and boolean expressions. Defaults to `ltlfilt`.
* `THEMIS_SUBPROCESS_TIMEOUT` contains the time in seconds before timing out on running any subprocess (ltl2mon or ltlfilt)

#### Utility ####

* `THEMIS_LTL2MON_BIN` contains the path or name of `ltl2mon` used to convert LTL to automata. Defaults to `ltl2mon`.
* `THEMIS_CACHE_LTL` contains the directory used to cache Automata created from LTL (so as to not call LTL2MON again.

#### Measures ####

* `THEMIS_BENCH_VERBOSE` is used to control the printing level of benchmarks. Currently a higher than 0 verbosity causes the benchmark measures to be printed, higher than 10 causes experiments to print the output of each run.
* `THEMIS_BENCH_TAG` can be used to tag a run with any text, defaults to empty.
* `THEMIS_BENCH_DB` is used to indicate the SQLITE database file to store benchmarks in. When empty it does not save anything. Defaults to `bench.db`

### Monitoring with THEMIS ###

We begin by explaining the basic layout of a generalized decentralized monitoring algorithm.
A monitoring algorithm has two phases: `setup` and `monitor`.
In the first phase, the algorithm is expected to create and initialize the monitors, connect them to each other so they can communicate, and attach them to components so they receive the observations that these components generate.
In the second phase, each monitor will receive observations at a timestamp based on the component they are attached to.
The monitor can then perform some computation, communicate with other monitors, abort monitoring or report a verdict.

To accomplish this we use the two interfaces [Boostrap](src/uga/corse/themis/bootstrap/Bootstrap.java) for setup and [Monitor](src/uga/corse/themis/monitoring/Monitor.java) for the monitor.

For simplicity and interaction with the existing tools, the extended interface [StandardMonitoring](src/uga/corse/themis/bootstrap/StandardMonitoring.java) allows for algorithms to be passed a default specification.
Furthermore, a simple general purpose monitor partial implementation is provided in [GeneralMonitor](src/uga/corse/themis/monitoring/GeneralMonitor.java), this sets up the basic operations for communication.

The method `setSpecification` of `StandardMonitoring` is called whenever the existing tools load a specification.
In the basic use case, using the `Bootstrap` interface, a decentralized algorithm is expected to provide the `getMonitors()` method, which does the setup phase of the algorithm, and returns a set of monitors (`<? extends Monitor>`). 
`Monitor` generally implements the `monitor` method to implement the monitoring logic, and the `reset` method to reset its state when executing multiple runs.
The method `monitor` provides the monitor a  read-only memory of observations based on the monitored component the component is attached to.
A monitor is expected to return a [MonitorResult](src/uga/corse/themis/monitoring/MonitorResult.java) which instructs the node what to do.
A `CONTINUE` result indicates that the node should continue monitoring, `VERDICT` notifies the node that the monitor has found a verdict, `ABORT_NODE` abort all monitoring on the node, and `ABORT_RUN` aborts all monitoring on all active nodes.
Developing new decentralized monitoring algorithms consists in implementing these interfaces and extending the provided base classes for more advanced control.
For example the [MonitorRound](src/uga/corse/themis/monitoring/MonitorRound.java) interface allows a node that uses rounds to notify the monitor that implements it of the round number.

The default flow of the base node [NodeRounds](src/uga/corse/themis/runtime/NodeRounds.java) is similar to the [Bulk Synchronous Parallel (BSP)](https://en.wikipedia.org/wiki/Bulk_synchronous_parallel#The_model) flow.
In the BSP model, all processes execute a computation phase, then, they communicate and finally synchronize.
The monitoring begins by setting up the monitor network.
Then, for each timestamp, the observations are gathered from the trace asynchronously, once all [Peripheries](src/uga/corse/themis/periphery/Periphery.java) of a component have received observations, all observations are passed to the monitors attached to the component.
Each monitor will then execute `monitor()` and may send data asynchronously to other monitors during the phase.
Once all monitors have executed their `monitor()` a new monitoring round begins.

The default flow, can be changed to accommodate other scenarios, and the communication protocol can be a different implementation (to simulate different types of communication).

The basic methods to extend for a monitoring algorithm:

```java
Set<? extends Monitor> getMonitors(Topology topology);
void setSpecification(Map<String, Specification> specs);
```

Generally, the basic methods to implement for a monitor, the last 2 are handled by `GeneralMonitor`:

```java
void reset();
void setup();
MonitorResult monitor(MemoryRO<Atom> observations) throws Exception;
Message recv(boolean consume);
public void send(int to, Message msg);
```

### Encoding ###
In addition to providing the basic blocks for the control flow of the algorithm, the THEMIS framework provides the basic concepts and data structures explained in this paper.
THEMIS provides parsers and basic structures for our automata specification.
A [basic parser frontend](src/uga/corse/themis/parser/FrontAutomataParser.java) for automata encoded as `DOT` files is provided.
This is used to parse the output from `ltl2mon`.
Two possible approaches to model automata are provided: [expressions](src/uga/corse/themis/automata/formats/LabelExpression.java) and [events](src/uga/corse/themis/automata/formats/LabelEvent.java).
These define the labels on the automata transitions, whether they consist of an expression or an event (set of observations).
Transition labels can be extended to handle any addition of algorithms that would require a different encoding for the [transition label](src/uga/corse/themis/automata/Label.java) (by extending [TransitionFormatter](src/uga/corse/themis/automata/formats/TransitionFormatter.java)). See [formats](src/uga/corse/themis/automata/formats) for more details.


THEMIS also provides expressions over atoms, the [provided atom types](src/uga/corse/themis/symbols) include [observations](src/uga/corse/themis/symbols/AtomString.java) (atomic propositions)  and [timed observations](src/uga/corse/themis/symbols/AtomObligation.java) (pair of timestamp and atomic proposition).
It is also possible to define custom encodings  by implementing the interface [Atom](src/uga/corse/themis/automata/Atom.java).
To create a new encoding of atomic propositions, it is needed to implement the `observe()` method which returns a string representation of the encoding.
In addition it is possible to return an object using the method `group()` which can be used to group atoms with the same objects.
This can be used to generate events for example, by grouping all timed observations by their timestamp.
Lastly, the method `isEmpty()` is used to denote an empty symbol.

Encoding functions from observations to atoms are provided in the [encoders](src/uga/corse/themis/encoders/) package, it includes the identity and timestamp encoders.

### Datastructures ###
THEMIS provides the [Memory](src/uga/corse/themis/monitoring/memory/Memory.java) interface to access the memory  data-structure as well.
[MemoryAtoms](src/uga/corse/themis/monitoring/memory/MemoryAtoms.java) is used to store atoms and lookup verdicts associated with them as described in the paper.
Another variant [MemoryEvent](src/uga/corse/themis/inference/MemoryEvents.java) can be used to store timed observations, where it automatically groups them into events based on their timestamp.
The memory supports the merge operation and can be sent and received.
THEMIS also provides the EHE datastructure via the class [Representation](src/uga/corse/themis/monitoring/ehe/Representation.java)  .
A `Representation` object is initialized with an automaton and a [simplifier](src/uga/corse/themis/utils/Simplifier.java).

### Instrumenting Execution with Metrics ###
The THEMIS framework uses AspectJ to record metrics for a given algorithm.
Writing a metric for an algorithm consists in using AspectJ's aspects to intercept the points in the execution and gathering information from the context.
To simplify the task, THEMIS provides the base aspect [Instrumentation](src/uga/corse/themis/measurements/Instrumentation.aj)  along with the classes [Measure](src/uga/corse/themis/measurements/Measure.java) and the [MeasureFunction](src/uga/corse/themis/measurements/MeasureFunction.java)}.
The `Instrumentation` aspect already defines basic pointcuts and triggers simple methods upon reaching them:

```java
void setup(Node n, Map<String, Serializable> tags) 
void runBegin() 
void stepBegin(Monitor mon, MemoryRO<Atom> obs)
void stepEnd(Monitor mon, MemoryRO<Atom> obs)
void runEnd() 
```

Furthermore, it is possible to provide your own handlers for events, without having to use the `Instrumentation` class, as such if you do not want to generate measures you can use the [RunHook](src/uga/corse/themis/measurements/RunHook.aj).

```java
void prestart(Node n, Map<String, Serializable> tags)
void start(Node n, Map<String, Serializable> tags)
void end() {}
```

The THEMIS framework comes currently bundled with existing measures found in the [measurements](src/uga/corse/themis/measurements) package.
The default active measures and utility aspects can be found in the [aop.xml](src/META-INF/aop.xml) file.
