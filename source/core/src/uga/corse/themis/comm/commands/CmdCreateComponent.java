package uga.corse.themis.comm.commands;


import uga.corse.themis.monitoring.Component;
import uga.corse.themis.periphery.Periphery;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.comm.messages.MsgCreateComponent;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.file.Paths;

/**
 * Command to add a Component to the Runtime
 */
public class CmdCreateComponent implements CmdRuntime<MsgCreateComponent> {
    private static final String TOKEN = "%pwd%";
    @Override
    public boolean handle(MsgCreateComponent msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {

        // Convert current path to an absolute path
        String path       = Paths.get(".").toAbsolutePath().normalize().toString();

        // Initialize Component
        Component comp =  (Component) msg.getCls().getDeclaredConstructor(String.class).newInstance(msg.getId());

        // Load Peripheries
        for(MsgCreateComponent.InputInfo info :  msg.getInput()) {
            // Replace tokens by their info in the resource string
            String res =  info.getRes().replaceAll(TOKEN, path);
            // Initialize Periphery
            Class<Periphery> pericls = (Class<Periphery>) CmdCreateComponent.class.getClassLoader()
                    .loadClass(info.getCls());
            Periphery peri = pericls.getDeclaredConstructor(String.class).newInstance(res);

            // Connect Periphery to component
            comp.addInput(peri);
        }
        // Add Component to the NodeRounds
        n.addComponent(comp);
        return true;
    }
}
