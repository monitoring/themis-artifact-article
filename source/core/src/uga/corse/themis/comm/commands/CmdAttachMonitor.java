package uga.corse.themis.comm.commands;


import uga.corse.themis.algorithms.orchestration.MonOrchMain;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.comm.messages.MsgAttachMonitor;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;

/**
 * Command to add a Component to the Runtime
 */
public class CmdAttachMonitor implements CmdRuntime<MsgAttachMonitor> {
    @Override
    public boolean handle(MsgAttachMonitor msg, Node n, AsynchronousSocketChannel client,
                          ObjectInputStream in, ObjectOutputStream out)
    throws Error, Exception {
        boolean result =  n.attachMonitor(msg.getComp(), msg.getMon());
        return result;
    }
}
