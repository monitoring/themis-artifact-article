package uga.corse.themis.comm.packets;

import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.monitoring.memory.Memory;

/**
 * A message containing a memory
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MemoryPacket implements Message {
    private static final long serialVersionUID = 1L;
    @SuppressWarnings("rawtypes")
    public Memory mem;


    /**
     * Create an empty memory packet
     */
    public MemoryPacket() {
    }

    /**
     * Creates a new memory packet given a memory
     *
     * @param mem Memory to enclose in packet
     */
    @SuppressWarnings("rawtypes")
    public MemoryPacket(Memory mem) {
        this.mem = mem;
    }

    public String toString() {
        return mem.toString();
    }

}
