package uga.corse.themis.comm.packets;

import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.monitoring.ehe.Representation;

/**
 * Message containing a representation
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class RepresentationPacket implements Message {
    private static final long serialVersionUID = 1L;

    public Representation repr;

    /**
     * Create an empty message
     */
    public RepresentationPacket() {
    }

    /**
     * Create a new representation packet given a representation
     *
     * @param repr Representation to enclose in the packet
     */
    public RepresentationPacket(Representation repr) {
        this.repr = repr;
    }

    @Override
    public String toString() {
        return repr.toString();
    }

}
