package uga.corse.themis.comm.protocol;

import java.io.Serializable;

/**
 * A message that can be communicated over the monitor network
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Message extends Serializable {
}
