
package uga.corse.themis.comm.protocol;

import uga.corse.themis.comm.dispatcher.Dispatcher;

import javax.xml.ws.Dispatch;

/**
 * A communication network
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Network {
    /**
     * Resets the network and makes it ready for monitoring
     */
    void reset() throws Exception;


    /**
     * Send a message to an address
     * @param address Address to receive message
     * @param msg Message to send
     */
    void put(int address, Message msg) throws Exception;

    /**
     * Receive a message from an address
     * @param address Address to receive from
     * @param consume If true, message is removed from the queue, use false to peek at message
     * @return A message if available or Null otherwise
     */
    Message get(int address, boolean consume) throws Exception;
}
