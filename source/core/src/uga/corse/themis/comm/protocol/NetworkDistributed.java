package uga.corse.themis.comm.protocol;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Platform;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.messages.MsgMonitorPayload;
import uga.corse.themis.runtime.Runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;


public class NetworkDistributed implements Network {
    private static class NetQueue extends LinkedBlockingQueue<Message> {
        private static final long serialVersionUID = 1L;
    }


    private static Logger log = LogManager.getLogger(NetworkDistributed.class);
    private String localAddr;
    private Platform platform ;
    private Map<Integer, NetQueue> recv;
    private Dispatcher dispatch;

    public NetworkDistributed(String myAddress, Dispatcher dispatch) {
        this.localAddr = myAddress;
        this.dispatch = dispatch;
    }

    public NetworkDistributed(String myAddress, Dispatcher dispatch, Platform platform) {
        this.platform   = platform;
        this.localAddr  = myAddress;
        this.dispatch   = dispatch;
        setupNetwork(platform);
    }
    public void setupNetwork(Platform platform) {
        recv = new HashMap<>(platform.getMonitors().size());
        for (Map.Entry<Integer, String> mon : platform.getMonitors().entrySet()) {
            if(mon.getValue().equals(localAddr))
                recv.put(mon.getKey(), new NetQueue());
        }
    }

    /**
     * Unreliable Put, will send Async, will not catch errors, only log them
     * Local Monitors will have their messages queued immediately
     *
     * @param id Monitor to send message to
     * @param message Payload to send
     */
    @Override
    public void put(final int id, final Message message)  {

        if(recv.containsKey(id)) {
            try {
                recv.get(id).put(message);
            }
            catch(InterruptedException e) {
                log.warn("Send Interrupted " + message.toString());
            }
            catch(Exception e) {
                log.warn(e.getClass().getSimpleName() + " " + e.getLocalizedMessage(), e);
            }
            return;
        }

        Runtime.getScheduler().submit(() -> {
            try {
                if (!dispatch.cmdSync(platform.getAddressMonitor(id), new MsgMonitorPayload(
                        0, id, message
                ))) log.warn("Rejected: " + id + " <- " + message.getClass().getSimpleName());
            } catch (Exception e) {
                log.warn(e.getClass().getSimpleName() + " " + e.getLocalizedMessage(), e);
            }
        });

    }


    /**
     * Retrieve a message sent to id
     *
     * @param id Monitor ID to receive message from
     * @return Message or null if missing
     */
    public Message get(int id) {
        return get(id, true);
    }

    @Override
    public Message get(int id, boolean consume) {
        if (!recv.containsKey(id)) return null;


        Message toGet = null;
        if (!consume)
            toGet = recv.get(id).peek();
        else
            toGet = recv.get(id).poll();


        return toGet;
    }

    /**
     * Clears the network of all messages
     */
    public void reset() {
        recv.clear();
    }

    /**
     * This implementation sends messages without buffering
     * Sync does nothing.
     */
    public void sync() {

    }
}
