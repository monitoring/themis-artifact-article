package uga.corse.themis.comm.dispatcher;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.comm.messages.MsgResult;
import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.runtime.Runtime;
import uga.corse.themis.runtime.observers.DispatcherEnd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

public class Sockets implements Dispatcher {
    private static Logger log = LogManager.getLogger(DispatcherEnd.class.getName());

    public boolean broadcast(final Collection<String> addr, final String exclude, final MsgRuntime msg) {

        Set<String> unique              = new HashSet<>(addr);
        unique.remove(exclude);
        CompletionService<Boolean> tasks   = new ExecutorCompletionService<>(Runtime.getScheduler());

        for(final String node : unique) {
            tasks.submit(() -> cmdSync(node, msg));
        }

        boolean success = true;
        for(String node : unique) {
            try {
                success = success && tasks.take().get();
            } catch (InterruptedException e) {

            } catch (ExecutionException e) {
                Runtime.report(e);
            }
        }
        return success;
    }
    public MsgResult cmdSyncObj(final String addr, final MsgRuntime msg)
            throws IOException
    {
        Socket sock = connect(addr);
        ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
        out.writeObject(msg);
        out.flush();


        ObjectInputStream in = new ObjectInputStream(sock.getInputStream());
        MsgResult res = null;
        try {
            res = (MsgResult) in.readObject();
        }
        catch(Exception e) {
            throw new IOException(e);
        }
        finally {
            sock.close();
            if(res == null) {
                throw new IOException("Command Failed: " + msg.getClass().getSimpleName());
            }
            return res;
        }
    }

    public boolean cmdSync(final String addr, final MsgRuntime msg)
            throws IOException
    {
        return cmdSyncObj(addr, msg).success();
    }

    public void cmdNoReply(final String addr, final MsgRuntime msg)
    throws IOException
    {
        Socket sock = connect(addr);
        ObjectOutputStream out = new ObjectOutputStream(sock.getOutputStream());
        out.writeObject(msg);
        out.flush();
        sock.close();
    }

    @Override
    public void cmdAsync(String addr, MsgRuntime msg, Callback<MsgResult> callback) {

        Runtime.getScheduler().submit(() -> {
            try {
                MsgResult res = cmdSyncObj(addr, msg);
                callback.onResponse(res);
            }
            catch(Exception e) {
                log.warn(e.getMessage(), e);
            }

        });

    }

    @Override
    public void cmdAsyncNoReply(String addr, MsgRuntime msg)  {
        Runtime.getScheduler().submit(() -> {
            try {
                cmdNoReply(addr, msg);
            }
            catch(Exception e) {
                log.warn(e.getMessage(), e);
            }
        });
    }

    public static Socket connect(String addr)
            throws IOException
    {
        String[] ad = addr.split(":");
        return new Socket(ad[0], Integer.parseInt(ad[1]));

    }
}
