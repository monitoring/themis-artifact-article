package uga.corse.themis.comm.messages;


import uga.corse.themis.monitoring.Monitor;

public class MsgAttachMonitor implements MsgRuntime {


    private static final long serialVersionUID = 6002522395900151190L;
    private Monitor mon;
    private String comp;

    public Monitor getMon() {
        return mon;
    }
    public String getComp() {
        return comp;
    }

    public MsgAttachMonitor(String comp, Monitor mon) {
        this.comp = comp;
        this.mon = mon;
    }

    @Override
    public String toString() {
        return comp + " <- " + mon.getID() + ":" + mon.getClass().getSimpleName() + "@" + hashCode() + ": " + mon.toString() ;
    }
}
