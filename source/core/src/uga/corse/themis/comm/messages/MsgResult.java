package uga.corse.themis.comm.messages;


import java.io.Serializable;

public class MsgResult implements MsgRuntime {


    private static final long serialVersionUID = -5375674219281166169L;

    public void setResult(boolean result) {
        this.result = result;
    }

    boolean result;

    public Serializable getData() {
        return data;
    }

    public void setData(Serializable data) {
        this.data = data;
    }

    Serializable data;
    public MsgResult(boolean value) {
        result = value;
    }

    public boolean success() { return result; }
}
