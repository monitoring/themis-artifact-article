package uga.corse.themis.comm.messages;


import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MsgStart implements MsgRuntime {


    private static final long serialVersionUID = -5996771562105789198L;
    private Date start;
    private Map<String , Serializable> tags = new HashMap<>();

    public String getNotify() {
        return notify;
    }


    public void setNotify(String notify) {
        this.notify = notify;
    }

    private String notify = null;


    public MsgStart(Date start) {
        this.start = start;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }


    @Override
    public String toString() {
        return start.toString() + (tags != null ? " " + tags.toString() : "");
    }

    public Map<String, Serializable> getTags() {
        return tags;
    }

    public void setTags(Map<String, Serializable> tags) {
        this.tags = tags;
    }

    public void tag(String key, Serializable value) {
        this.tags.put(key, value);
    }
    public void removeTag(String key) {
        this.tags.remove(key);
    }
}
