package uga.corse.themis.periphery;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.symbols.AtomString;

/**
 * Reads a trace from a string
 * The timestamps are separated by {@literal >}
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class TraceString implements Periphery {
    String[] trace;
    int t = 0;

    public TraceString(String fullTrace) {
        trace = fullTrace.split(">");
    }

    @Override
    public Memory<Atom> next() {
        Memory<Atom> mem = new MemoryAtoms();
        if (t >= trace.length || trace[t].isEmpty())
            return mem;

        String[] aps = trace[t].split(",");
        for (String ap : aps) {
            String[] obs = ap.split(":");
            mem.add(new AtomString(obs[0]),
                    obs[1].equals("t"));
        }
        t++;
        return mem;
    }

    @Override
    public void stop() {
        trace = new String[0];
    }

    @Override
    public void notify(Control ctrl) {
    }
}
