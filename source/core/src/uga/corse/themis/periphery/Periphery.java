package uga.corse.themis.periphery;


import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;

/**
 * The interface with the system under monitoring
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Periphery {
    /**
     * Get the next observation
     *
     * @return
     */
    Memory<Atom> next() throws Exception;

    /**
     * Notify Periphery of a Ctrl Signal
     *
     * @param ctrl
     */
    void notify(Control ctrl) throws Exception;

    /**
     * Signal no need to read any more
     */
    void stop();
}
