package uga.corse.themis.periphery;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.symbols.AtomString;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TraceNetPlain extends TraceSock{

    public TraceNetPlain(String res) throws IOException {
        super(res);
    }
    public TraceNetPlain(String host, Integer port) throws IOException {
        super(host, port);
    }

    private BufferedReader in;
    private PrintWriter out;

    @Override
    protected void setup(Socket client) throws IOException {
        this.in     = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.out    = new PrintWriter(client.getOutputStream());
    }

    @Override
    public Memory<Atom> next() {
        Memory<Atom> mem = new MemoryAtoms();
        String line = null;

        try {
            line = this.in.readLine();
        } catch (IOException var9) {

        }

        if(line != null && !line.isEmpty()) {
            String[] aps = line.split(",");
            String[] var4 = aps;
            int var5 = aps.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String ap = var4[var6];
                String[] obs = ap.split(":");
                mem.add(new AtomString(obs[0]), obs[1].equals("t"));
            }

            return mem;
        } else {
            return mem;
        }
    }

    @Override
    public void notify(Control ctrl) {
        this.out.println(ctrl.toString());
        this.out.flush();
    }
}
