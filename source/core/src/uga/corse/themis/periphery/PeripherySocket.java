package uga.corse.themis.periphery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.symbols.AtomString;

/**
 * Reads a trace from a socket
 * Each event is on a line
 * <p>
 * Example:
 * a0:t,b0:f,c0:t
 * a0:t,b0:t,c0:f
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class PeripherySocket implements Periphery {

    Socket sock;
    ServerSocket serv;
    BufferedReader in;
    PrintWriter out;

    public PeripherySocket(ServerSocket s, Socket client) throws IOException {
        serv = s;
        sock = client;
        in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        out = new PrintWriter(sock.getOutputStream());
    }

    @Override
    public Memory<Atom> next() throws IOException {
        Memory<Atom> mem = new MemoryAtoms();
        String line;

        line = in.readLine();

        if (line == null || line.isEmpty()) return mem;

        String[] aps = line.split(",");
        for (String ap : aps) {
            String[] obs = ap.split(":");
            mem.add(new AtomString(obs[0]),
                    obs[1].equals("t"));
        }
        return mem;
    }

    @Override
    public void stop() {
        try {
            in.close();
            out.close();
            sock.close();
            serv.close();

        } catch (IOException e) {

        }
    }

    @Override
    protected void finalize() throws Throwable {
        in.close();
        out.close();
        sock.close();
        serv.close();
    }


    @Override
    public void notify(Control ctrl) {
        out.println(ctrl.toString());
        out.flush();
    }
}
