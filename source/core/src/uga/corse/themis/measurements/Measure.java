package uga.corse.themis.measurements;

/**
 * A Benchmark Measure
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Measure {
    /**
     * Measure key used to update or save measure
     */
    protected String key;
    protected String description;
    /**
     * Value of the measure
     */
    protected Object value;
    /**
     * Default update measure for the function
     */
    protected MeasureFunction func;

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    public Measure(String key, String description, Object value) {
        this(key, description, value, MeasureFunction.Default);
    }

    public Measure(String key, String description, Object value, MeasureFunction updateFunc) {
        this.key = key;
        this.description = description;
        this.value = value;
        this.func = updateFunc;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDescription() {
        return description;
    }

    /**
     * Sets a label description for the measure
     * (For printing mostly)
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object update(Object... args) {
        value = this.func.update(value, args);
        return value;
    }


    public void setFunc(MeasureFunction func) {
        this.func = func;
    }

    public MeasureFunction getFunc() {
        return func;
    }


}
