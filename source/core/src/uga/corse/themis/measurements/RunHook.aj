package uga.corse.themis.measurements;



import java.io.Serializable;
import java.util.Map;

import uga.corse.themis.runtime.Node;

/**
 * Hooks for Start and End of a monitoring execution
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public abstract aspect RunHook {

    before(Node n, Map<String, Serializable> tags)
        :  Commons.monitoringStartCall(tags) && target(n) {
        prestart(n, tags);
    }

    before(Node n, Map<String, Serializable> tags)
        : Commons.monitoringStartExec(tags) && this(n) {
        start(n, tags);
    }


    after() returning()
        : Commons.monitoringStopCall() {
        end();
    }

    public void prestart(Node n, Map<String, Serializable> tags) {}
    public void start(Node n, Map<String, Serializable> tags) {}
    public void end() {}


}
