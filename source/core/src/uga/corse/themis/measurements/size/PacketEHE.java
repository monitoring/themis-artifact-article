package uga.corse.themis.measurements.size;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.comm.packets.RepresentationPacket;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.measurements.Config;
import uga.corse.themis.measurements.DataSize;
import uga.corse.themis.measurements.Size;
import uga.corse.themis.monitoring.ehe.Rule;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.utils.Expressions;

/**
 * Size of a memory transfer
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class PacketEHE implements MsgSize<RepresentationPacket> {
    @Override
    public Long getSize(RepresentationPacket msg, Size size) {
        Long total = 0l;
        for (Rule r : msg.repr.getRules().values()) {
            //A rule is a map of state -> expression
            //Size = size(state) + size(expression)
            for (BoolExpression expr : r.getPairs().values()) {
                Expressions.ExprSize s = Expressions.getLength(expr);
                for (Object leaf : s.leaves)
                    total += Config.atomsLength(leaf, size);
                // size(operator) * (no of ops) + size(state)
                total += size.get(DataSize.S_OPER) * s.expressions
                        + size.get(DataSize.S_STATE);
            }
        }
        return total;
    }
}
