package uga.corse.themis.measurements.size;

import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.measurements.Size;

/**
 * Size of an element
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface MsgSize<T extends Message> {
    Long getSize(T msg, Size size);
}
