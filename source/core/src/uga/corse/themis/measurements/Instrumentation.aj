package uga.corse.themis.measurements;

import java.io.Serializable;
import java.lang.Double;
import java.lang.Float;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.utils.Opts;

/**
 * Benchmark Abstract Aspect
 * Defines basic benchmark functionality
 * Is intercepted by other aspects to save measures
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public abstract aspect Instrumentation {

    private String description = "";
    protected Map<String, Measure> measures = new HashMap<String, Measure>();
    private static int verbose = Integer.parseInt(Opts.getGlobal("THEMIS_BENCH_VERBOSE", "1"));

    /**
     * Set the benchmark description (for printing purposes)
     * @param label
     */
    protected void setDescription(String label) {
        this.description = label;
    }

    /**
     * Returns the measure associated with a key
     * @param key
     * @return
     */
    public Measure getMeasure(String key) {
        return measures.get(key);
    }

    /**
     * Adds a measure, the measures getKey defines the measure key for retreival or update
     * @param value
     */
    public void addMeasure(Measure value) {
        measures.put(value.getKey(), value);
    }

    /**
     * Add Several measures at once
     * @param measures
     */
    public void addMeasures(Measure... measures) {
        for (Measure measure : measures)
            addMeasure(measure);
    }

    /**
     * Update a function with a custom update function
     * @param key
     * @param mfunc
     * @return
     */
    public Object update(String key, MeasureFunction mfunc) {
        if (!measures.containsKey(key))
            return null;
        Object res = mfunc.update(measures.get(key).getValue());
        measures.get(key).setValue(res);
        return res;
    }

    /**
     * Update a measure using its default update function
     * @param key
     * @param nvalue
     * @return
     */
    public Object update(String key, Object... nvalue) {
        if (!measures.containsKey(key))
            return null;
        Object res = measures.get(key).update(nvalue);
        measures.get(key).setValue(res);
        return res;
    }

    /**
     * Return all registered measures
     * @return
     */
    public Map<String, Measure> getAllMeasures() {
        return measures;
    }

    before(Map<String, Serializable> tags) : Commons.monitoringStartCall(tags) {
        Node node = (Node) thisJoinPoint.getTarget();
        setup(node, tags);
    }


    before(Map<String, Serializable> tags) : Commons.monitoringStartExec(tags) {
        runBegin();
    }


    after() returning(): Commons.monitoringStopCall() {
        runEnd();
        if (verbose > 0)
            printAll();
        cleanup();
    }

    before(Monitor mon, MemoryRO<Atom> obs) : Commons.monitorStep(mon, obs) {
        stepBegin(mon, obs);
    }
    after(Monitor mon, MemoryRO<Atom> obs) : Commons.monitorStep(mon, obs) {
        stepEnd(mon, obs);
    }

    /**
     * The purpose of this is to capture information and setup the run
     */
    protected void setup(Node n, Map<String, Serializable> tags) {
    }

    /**
     * Executes before starting reading each trace
     */
    protected void runBegin() {
    }

    /**
     * Executes after finishing reading each trace
     */
    protected void runEnd() {}

    /**
     * Executes at the start of each discrete step
     */
    protected void stepBegin(Monitor mon, MemoryRO<Atom> obs) {
    }

    /**
     * Executes at the end of each discrete step
     * This will not execute when the algorithm throws a verdict
     */
    protected void stepEnd(Monitor mon, MemoryRO<Atom> obs) {
    }

    /**
     * Executes when a verdict is reported by throwing the exception
     * (This indicates the full algorithm has reported a verdict)
     * This executes instead of stepEnd
     *
     * @param monid Monitor ID reporting the verdict
     * @param v Verdict reported
     */
    protected void stepVerdict(int monid, Verdict v) {
    }




    /**
     * Executes Before a run
     * The default removes non-persistent measures
     */
    protected void cleanup() {
        measures.clear();
    }

    /**
     * Prints all measures
     */
    protected void printAll() {
        Commons.printSection(description.isEmpty() ? this.getClass().getSimpleName() : description);
        for (Measure m : measures.values()) {
            Object data = m.getValue();
            String fmt = getFormat(data);
            Commons.printMeasureln(m.getDescription(), fmt, data);
        }
    }

    protected String getFormat(Object o) {
        if (o instanceof Long || o instanceof Integer) {
            return Commons.FMT_NUM;
        }
        if (o instanceof Double || o instanceof Float) {
            return Commons.FMT_DEC;
        }
        if (o instanceof Boolean) {
            return Commons.FMT_BOOL;
        }
        return Commons.FMT_STR;
    }
}
