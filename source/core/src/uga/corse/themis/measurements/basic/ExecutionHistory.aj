package uga.corse.themis.measurements.basic;


import uga.corse.themis.measurements.Instrumentation;
import uga.corse.themis.measurements.*;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.ehe.Representation;
import uga.corse.themis.runtime.Node;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

/**
 * Measures for EHE usage
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public privileged aspect ExecutionHistory extends Instrumentation {

    int ts_cur = 0;
    Set<Integer> updated = new HashSet<>();

    before(Integer nval, Monitor mon):
            set(int Representation.lastResolved)
                    && args(nval)
                    && withincode(* Representation.update(..))
                    && Commons.inMonitor(mon)
            {
                int id = mon.getID();
                //Already updated this round
                if(updated.contains(id)) return;

                int old     =  (((Representation) thisJoinPoint.getThis()).lastResolved);
                int size    = (int) (((Representation) thisJoinPoint.getThis()).size());
                int delay   = (ts_cur - 1) - old;

                if (delay > 0) {
                    update("resolutions", 1);
                    update("sum_delay", delay);
                    update("max_delay", delay);
                    update("min_delay", delay);
                    update("ehe_maxsize", size);
                }
            }

    @Override
    protected void setup(Node n, Map<String, Serializable> tags) {
        Integer maxDelay = (Integer) n.getPlatform().getData();

        setDescription("Execution History");
        addMeasure(new Measure("sum_delay", "Total Delay", 0, Measures.addInt));
        addMeasure(new Measure("resolutions", "Total Resolutions", 0, Measures.addInt));
        addMeasure(new Measure("max_delay", "Max Delay", 0, Measures.maxInt));
        addMeasure(new Measure("min_delay", "Min Delay", maxDelay, Measures.minInt));
        addMeasure(new Measure("ehe_maxsize", "Max EHE Size", 0, Measures.maxInt));
    }


    after(Integer t) : Commons.monitoringStep(t) {
        if(t == 0) return;
        ts_cur = t;
        updated.clear();
    }

}
