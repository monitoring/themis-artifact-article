package uga.corse.themis.measurements.basic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.measurements.*;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.utils.Opts;

/**
 * Standard Measures
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public privileged aspect GlobalRun extends Instrumentation {

    HashMap<String, Long> comms = new HashMap<String, Long>();

    Integer timestep = 0;


    @Override
    protected void setup(Node n, Map<String, Serializable> tags) {
        setDescription("Full Run Stats");
        addMeasures(
                new Measure("run_len", "Run Length", 0, Measures.replace)
                , new Measure("comps", "Components", n.getPlatform().getCountComponents(), Measures.replace)
                , new Measure("msg_num", "Number of Messages", 0L, Measures.addLong)
                , new Measure("msg_data", "Data Exchanged", 0L, Measures.addLong)
                , new Measure("verdict", "Verdict", Verdict.NA, Measures.replace)
        );
    }

    before(Integer t) : uga.corse.themis.measurements.Commons.monitoringStep(t) {
        synchronized (timestep) {
            if (t == 0) return;
            timestep = t;
            update("run_len", t);
        }
    }

    after(Integer monid, Verdict verdict) : uga.corse.themis.measurements.Commons.onVerdict(monid, verdict) {
        synchronized (timestep) {
            update("verdict", verdict);
        }
    }

    after(Integer to, Message m): uga.corse.themis.measurements.Commons.sendMessage(to, m)
    {
        synchronized (timestep) {
            update("msg_num", 1L);
            update("msg_data", Config.sizeMessage(m, new Config.SizeDefault()));

            String s = m.getClass().getSimpleName();

            Long n = 0l;
            if (comms.containsKey(s))
                n += comms.get(s);
            n++;
            comms.put(s, n);
        }
    }


    protected synchronized void printAll() {
        super.printAll();

        Commons.printSection("Messages Details");
        for (Entry<String, Long> entry : comms.entrySet())
            Commons.printMeasureln(entry.getKey(), Commons.FMT_NUM, entry.getValue());

        comms.clear();
    }

}
