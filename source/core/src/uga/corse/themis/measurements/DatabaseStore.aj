package uga.corse.themis.measurements;

import java.io.File;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import uga.corse.themis.utils.Opts;
import uga.corse.themis.runtime.Node;

/**
 * Persistence Layer 
 * Store in Database the measures per run
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public privileged aspect DatabaseStore {
    public static final String DB_NAME = "THEMIS_BENCH_DB";
    public static final String DEF_TAG = "THEMIS_BENCH_TAG";

    private Map<String, Serializable> tags;


    //Should we create the database
    private boolean create = false;

    declare precedence:DatabaseStore,Instrumentation;


    //Active benchmarks
    List<Instrumentation> benchmarks = new LinkedList<Instrumentation>();



    before (Map<String, Serializable> tags): Commons.monitoringStartCall(tags)
    {

    }


    //Capture the active benchmarks
    after(Instrumentation b):
    call(void Instrumentation.setup(Node, Map<String, Serializable>))
            && target(b)
    {
        benchmarks.add(b);
    }

    before (Map<String, Serializable> tags):  Commons.monitoringStartExec(tags){
        String database = Opts.getGlobal(DB_NAME, "");
        this.tags = tags;
        //Check Whether or not the database needs to be created
        if (!database.isEmpty())
            create = !(new File(database)).exists();

    }


    /**
     * Once done monitoring save benchmarks in database
     */
    before() : Commons.monitoringStopCall() {


        String database = Opts.getGlobal(DB_NAME, "");
        String tag = Opts.getGlobal(DEF_TAG, "");

        //If set to empty do not log to db
        if (database.isEmpty()) return;

        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + database);
            c.setAutoCommit(true);

            if (create) {
                makeTable(c);
                create = false;
            }

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return;
        }


        StringBuilder keys = new StringBuilder("run_id, tag");
        StringBuilder vals = new StringBuilder("?, ?");

        if(tags != null) {
            for(String key : tags.keySet()) {
                keys.append(", ").append(key);
                vals.append(", ?");
            }
        }
        for (Instrumentation b : benchmarks) {
            for (String key : b.getAllMeasures().keySet()) {
                keys.append(", ").append(key);
                vals.append(", ?");
            }
        }

        StringBuilder sql = new StringBuilder("INSERT INTO bench (");
        sql.append(keys.toString()).append(") VALUES (")
                .append(vals.toString()).append(")");


        try {
            PreparedStatement stmt = c.prepareStatement(sql.toString());

            int i = 3;
            stmt.setNull(1, Types.INTEGER);
            if (tag.isEmpty())
                stmt.setNull(2, Types.VARCHAR);
            else
                stmt.setString(2, tag);

            if(tags != null) {
                for(Map.Entry<String, Serializable> tagEntry : tags.entrySet()) {

                    String value = "";
                    if(tagEntry.getValue() != null)
                        value = tagEntry.getValue().toString();
                    if(value.isEmpty())
                        stmt.setNull(i, Types.VARCHAR);
                    else
                        stmt.setString(i, value);
                    i++;
                }
            }

            for (Instrumentation b : benchmarks) {
                for (Measure m : b.getAllMeasures().values()) {
                    stmt.setObject(i, m.getValue());
                    i++;
                }
            }
            stmt.executeUpdate();
            c.close();


        } catch (SQLException e) {
            System.err.println(e.getMessage());
        } finally {

        }
        benchmarks.clear();
    }

    private void makeTable(Connection c) {
        StringBuilder sql = new StringBuilder("CREATE TABLE bench ( run_id INTEGER PRIMARY KEY AUTOINCREMENT, tag TEXT");
        if(tags != null) {
            for(String tag : tags.keySet()) {
                sql.append(",")
                        .append(tag)
                        .append(" TEXT");
            }
        }
        for (Instrumentation b : benchmarks) {
            for (Measure measure : b.getAllMeasures().values()) {
                String key = measure.getKey();
                Object val = measure.getValue();
                String type = getSQLType(val);

                sql.append(", ")
                        .append(key)
                        .append(" ")
                        .append(type)
                        .append(" NOT NULL");
            }
        }
        sql.append(")");
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate(sql.toString());
        } catch (SQLException e) {
            return;
        }
    }

    private String getSQLType(Object o) {
        if (o instanceof Integer || o instanceof Long)
            return "INT";
        if (o instanceof Float || o instanceof Double)
            return "REAL";
        else
            return "TEXT";
    }
}
