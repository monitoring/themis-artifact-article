package uga.corse.themis.measurements;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.comm.packets.RepresentationPacket;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.expressions.bool.Atomic;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.expressions.bool.False;
import uga.corse.themis.expressions.bool.True;
import uga.corse.themis.measurements.size.MsgSize;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.ehe.Rule;
import uga.corse.themis.symbols.AtomEmpty;
import uga.corse.themis.symbols.AtomNegation;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.symbols.AtomString;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.Expressions.ExprSize;

import java.util.HashMap;
import java.util.Map;

/**
 * Benchmark Assumptions and Configurations
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Config {

    private static Map<String, MsgSize> bindings = new HashMap<>();

    public static void register(String msgClass, MsgSize computeSize) {
        bindings.put(msgClass, computeSize);
    }
    public static void unregister(String msgClass) {
        bindings.remove(msgClass);
    }
    public static void unbind() {
        bindings.clear();
    }
    /**
     * Default Sizes assumptions in Bytes
     *
     * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
     */
    public static class SizeDefault implements Size {
        @Override
        public Long get(DataSize s) {
            switch (s) {
                case S_BOOL:
                case S_CHAR:
                case S_CTRL:
                case S_OPER:
                case S_VER:
                    return 1L;
                case S_ID:
                case S_TIME:
                    return 4L;
                case S_STATE:
                    return 2L;
                default:
                    return 0L;
            }
        }
    }

    /**
     * Computes the message size given a set of size assumptions
     *
     * @param m
     * @param size
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Long sizeMessage(Message m, Size size) {
        String cls = m.getClass().getName();

        if(!bindings.containsKey(cls))
            throw new Error("Cannot determine size of message: " + cls);

        return bindings.get(cls).getSize(m, size);
    }

    /**
     * Computes the size of an atom
     *
     * @param leaf
     * @param size
     * @return
     */
    public static Long atomsLength(Object leaf, Size size) {
        if (leaf instanceof Atomic) //Unpack to atom
            return atomsLength(((Atomic) leaf).getAtom(), size);
        else if (leaf instanceof True || leaf instanceof False)
            return size.get(DataSize.S_BOOL);
        else if (leaf instanceof AtomString)    //String size
            return leaf.toString().length() * size.get(DataSize.S_CHAR);
        else if (leaf instanceof AtomObligation)//Atom + Timestamp
            return atomsLength(((AtomObligation) leaf).getAtom(), size)
                    + size.get(DataSize.S_TIME);
        else if (leaf instanceof AtomEmpty)        //Smaller than anything
            return size.get(DataSize.S_CTRL);
        else if (leaf instanceof AtomNegation)    //Operator + Atom
            return size.get(DataSize.S_OPER)
                    + atomsLength(((AtomNegation) leaf).getNegated(), size);
        else
            throw new Error("Object Size not specified: " + leaf.getClass().getName());
    }
}
