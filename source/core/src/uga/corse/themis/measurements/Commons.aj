package uga.corse.themis.measurements;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.MonitorRound;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.runtime.NodeRounds;

import java.io.Serializable;
import java.util.Map;

/**
 * Utility Functions for Benchmarks
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public privileged aspect Commons {

    //Formats
    public static final String FMT_KEY = "%-35s";
    public static final String FMT_SEP = " : ";
    public static final String FMT_NUM = "%15d";
    public static final String FMT_DEC = "%15.2f";
    public static final String FMT_BOOL = "%15b";
    public static final String FMT_STR = "%15s";

    //Pointcut to avoid the benchmarks package
    public pointcut benchPkg():
            within(uga.corse.themis.measurements.*);

    //Starting Monitoring
    public pointcut monitoringStartCall(Map<String, Serializable> tags):
            call(void Node.start(Map<String, Serializable>)) && args(tags);

    public pointcut monitoringStartExec(Map<String, Serializable> tags):
            execution(void Node.start(Map<String, Serializable>)) && args(tags);


    //Stop Monitoring
    public pointcut monitoringStopCall() :
            call(void Node.stop());

    public pointcut monitoringStopExec() :
            execution(void Node.stop());

    //Verdict
    public pointcut onVerdict(Integer monid, Verdict verdict) :
            call(void Node.notifyVerdict(int,Verdict)) && args(monid, verdict);


    //Pointcut to indicate the step phase for each timestamp only for MonitorRound monitors
    public pointcut monitoringStep(Integer t):
            set(int NodeRounds.round) && args(t);

    //Pointcut to indicate the step of monitoring
    public pointcut monitorStep(Monitor mon, MemoryRO<Atom> obs):
            execution(MonitorResult Monitor.monitor(MemoryRO<Atom>))
                    && args(obs) && target(mon);


    //Pointcut to capture messages sent
    public pointcut sendMessage(Integer to, Message m):
            call(void Network.put(int, Message))
                    && args(to, m);

    //Pointcut to capture evaluating Rules
    public pointcut evalRule():
            call(State Rule.resolve(Memory));

    //Pointcut to capture evaluating expressions
    public pointcut evalExpr():
            call(Verdict BoolExpression.eval(Memory));

    //Pointcut to capture Monitoring call
    public pointcut inMonitor(Monitor mon):
            cflow(execution(MonitorResult Monitor.monitor(MemoryRO<Atom>)) && target(mon));



    public static void printSection(String title) {
        System.out.println();
        System.out.println("[" + title + "]");
    }

    public static void printMeasure(String label, String fmt, Object data) {
        System.out.printf(
                FMT_KEY + FMT_SEP + fmt,
                label,
                data
        );
    }

    public static void printMeasureln(String label, String fmt, Object data) {
        System.out.printf(
                FMT_KEY + FMT_SEP + fmt + "\n",
                label,
                data
        );
    }

    public static void printMeasures(String sep, Object... data) {
        if ((data.length % 3) != 0)
            throw new Error("Measures supplied are not correct tuples");

        for (int i = 0; i < data.length; i += 3) {
            printMeasure((String) data[i], (String) data[i + 1], data[i + 2]);
            System.out.print(sep);
        }
    }
}
