package uga.corse.themis.measurements;

/**
 * A set of basic functions used for benchmarking
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Measures {
    /**
     * A function which adds longs
     */
    public static MeasureFunction addLong = new MeasureFunction() {
        @Override
        public Object update(Object existing, Object... args) {
            Long total = 0l;
            for (int i = 0; i < args.length; i++) {
                total += (Long) (args[i]);
            }
            return (Long) existing + total;
        }
    };
    /**
     * A fucntion which adds integers
     */
    public static MeasureFunction addInt = new MeasureFunction() {
        @Override
        public Object update(Object existing, Object... args) {

            Integer total = 0;
            for (Object o : args)
                total += (Integer) o;
            return (Integer) existing + total;
        }
    };
    /**
     * A fucntion which adds integers
     */
    public static MeasureFunction addDouble = new MeasureFunction() {
        @Override
        public Object update(Object existing, Object... args) {

            Double total = 0d;
            for (Object o : args)
                total += (Double) o;
            return (Double) existing + total;
        }
    };
    /**
     * A function which replaces a measure by the first value given
     */
    public static MeasureFunction replace = new MeasureFunction() {
        @Override
        public Object update(Object existing, Object... args) {
            return args[0];
        }
    };
    public static MeasureFunction max = new MeasureFunction() {

        @Override
        public Object update(Object existing, Object... args) {
            Long max = (Long) existing;
            for (Object o : args)
                max = Math.max(max, (Long) o);
            return max;
        }
    };
    public static MeasureFunction min = new MeasureFunction() {

        @Override
        public Object update(Object existing, Object... args) {
            Long max = (Long) existing;
            for (Object o : args)
                max = Math.min(max, (Long) o);
            return max;
        }
    };
    public static MeasureFunction maxInt = new MeasureFunction() {

        @Override
        public Object update(Object existing, Object... args) {
            int max = (int) existing;
            for (Object o : args)
                max = Math.max(max, (int) o);
            return max;
        }
    };
    public static MeasureFunction minInt = new MeasureFunction() {

        @Override
        public Object update(Object existing, Object... args) {
            int max = (int) existing;
            for (Object o : args)
                max = Math.min(max, (int) o);
            return max;
        }
    };

}
