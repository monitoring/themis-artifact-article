package uga.corse.themis.symbols;

import uga.corse.themis.automata.Atom;

/**
 * Designates an Empty observation
 * Special atom indicating nothing was observed
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class AtomEmpty implements Atom {

    private static final long serialVersionUID = 1L;
    public static String EMPTY = "empty";

    @Override
    public String observe() {
        return EMPTY;
    }

    @Override
    public Object group() {
        return observe();
    }

    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public String toString() {
        return observe();
    }

    @Override
    public int compareTo(Atom o) {
        if (o instanceof AtomEmpty)
            return 0;
        return 1;
    }

}
