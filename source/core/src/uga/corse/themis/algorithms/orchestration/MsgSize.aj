package uga.corse.themis.algorithms.orchestration;


import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.measurements.Config;
import uga.corse.themis.measurements.RunHook;
import uga.corse.themis.measurements.size.PacketMem;
import uga.corse.themis.runtime.Node;

import java.io.Serializable;
import java.util.Map;

public aspect MsgSize extends RunHook {

    @Override
    public void prestart(Node n, Map<String, Serializable> tags) {
        Config.register(MemoryPacket.class.getName(), new PacketMem());
    }

    @Override
    public void end() {
        Config.unregister(MemoryPacket.class.getName());
    }
}