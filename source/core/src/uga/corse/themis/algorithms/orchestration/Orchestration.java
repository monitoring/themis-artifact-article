package uga.corse.themis.algorithms.orchestration;


import java.util.*;

import uga.corse.themis.Topology;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.specifications.SpecAutomata;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.utils.Convert;

/**
 * Orchestration Algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Orchestration implements StandardMonitoring {

    protected Automata aut;

    public void setSpecification(Map<String, Specification> decentSpec) {
        Specification main = decentSpec.get("root");
        if(main == null)
            throw new IllegalArgumentException("No Root Spec found");
        setMainSpec(Convert.makeAutomataSpec(main));
    }

    public void setMainSpec(Specification spec) {
        if(!(spec instanceof  SpecAutomata))
            throw new IllegalArgumentException("Specification must be an automaton!");
        aut = ((SpecAutomata) spec).getAut();
        if(aut == null)
            throw new IllegalArgumentException("Automaton is not valid");
        aut = Convert.simplifyTransitions(aut);

    }

    @Override
    public Set<? extends Monitor> getMonitors(Topology topology) {

        int c = topology.getCountComponents();
        Set<Monitor> mons = new HashSet<>(c+1);
        if(c <= 0) return mons;
        if(aut == null)
            throw new IllegalStateException("No Automaton provided");


        Set<String> comps = topology.getGraph().keySet();
        Iterator<String> iter = comps.iterator();

        int i = 0;
        MonOrchMain main = new MonOrchMain(i);
        main.setSpec(aut);
        main.setComponentKey(iter.next());
        mons.add(main);

        for(i = 1; i < c; i++) {
            MonOrchSlave slave = new MonOrchSlave(i);
            slave.setComponentKey(iter.next());
            mons.add(slave);
        }


        return mons;
    }
}
