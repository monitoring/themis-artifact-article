package uga.corse.themis.algorithms.orchestration;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.monitoring.GeneralMonitor;
import uga.corse.themis.monitoring.memory.MemoryRO;

/**
 * Slave Monitor for Orchestration
 * Forwards observation to the main monitor
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MonOrchSlave extends GeneralMonitor  {



    private static final long serialVersionUID = 5821968047724979467L;

    public MonOrchSlave(Integer id) {
        super(id);
    }

    @Override
    public void setup() {
    }

    @Override
    public MonitorResult monitor(MemoryRO<Atom> observations) throws Exception {
        if(observations.isEmpty()) return MonitorResult.CONTINUE;
        send(0, new MemoryPacket(observations));
        return MonitorResult.CONTINUE;
    }

    @Override
    public void reset() {

    }

    @Override
    public Verdict getCurrentVerdict() {
        return Verdict.NA;
    }

    @Override
    public void communicate() {
    }

}
