package uga.corse.themis.algorithms.orchestration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.VerdictTimed;
import uga.corse.themis.comm.packets.MemoryPacket;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.monitoring.ehe.Representation;
import uga.corse.themis.monitoring.GeneralMonitor;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.utils.Simplifier;
import uga.corse.themis.utils.SimplifierFactory;


/**
 * Main Orchestration Monitor
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MonOrchMain extends GeneralMonitor {
    private static Logger log = LogManager.getLogger(MonOrchMain.class);

    private static final long serialVersionUID = 2370254843939836439L;
    private Automata spec;

    private transient Representation autRep;
    private transient Memory<Atom> memory;
    private transient Verdict verdict = Verdict.NA;
    private transient Simplifier simplifier;

    public MonOrchMain(Integer id) {
        super(id);
    }

    @Override
    public void reset() {
        memory.clear();
        autRep = new Representation(spec, simplifier);
    }

    @Override
    public Verdict getCurrentVerdict() {
        return verdict;
    }


    @Override
    public void setup() {
        autRep = new Representation(spec);
        memory = new MemoryAtoms();
        simplifier = SimplifierFactory.spawnDefault();
        verdict = Verdict.NA;
    }

    @Override
    public void cleanup() {
        simplifier.stop();
    }

    public void setSpec(Automata spec) {
        this.spec = spec;
    }
    public Automata getSpec() { return spec; }

    @Override
    public MonitorResult monitor(MemoryRO<Atom> observations) throws Exception {
        if(verdict.isFinal()) return MonitorResult.ABORT_RUN;
        memory.merge(observations);
        Message m;
        while ((m = recv()) != null) {
            MemoryPacket packet = (MemoryPacket) m;
            memory.merge((Memory<Atom>) packet.mem);
        }

        //Nothing observed: Either first timestamp or end of trace
        if (memory.isEmpty())
            return MonitorResult.CONTINUE;
        else
            autRep.tick();

        autRep.update(memory, -1);
        VerdictTimed v = autRep.scanVerdict();
        log.trace("At State: " + autRep.getLastResolvedState());
        memory.clear();

        if (v.isFinal()) {
            //throw new ReportVerdict(v.getVerdict(), t);
            this.verdict = v.getVerdict();
            return MonitorResult.VERDICT;
        }

        autRep.dropResolved();

        return MonitorResult.CONTINUE;
    }

    public Representation getRepresentation() {
        return autRep;
    }

    public Automata getAutomata() {
        return spec;
    }

    @Override
    public void communicate() {
    }

}
