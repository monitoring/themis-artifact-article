package uga.corse.themis.algorithms.choreography;


import uga.corse.themis.comm.packets.RepresentationPacket;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.measurements.Config;
import uga.corse.themis.measurements.RunHook;
import uga.corse.themis.measurements.Size;
import uga.corse.themis.measurements.size.PacketEHE;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.measurements.DataSize;

import java.io.Serializable;
import java.util.Map;

public aspect MsgSize extends RunHook {

    public static class SMsgKill implements uga.corse.themis.measurements.size.MsgSize<MessageKill> {
        @Override
        public Long getSize(MessageKill msg, Size size) {
            return size.get(DataSize.S_CTRL) + size.get(DataSize.S_ID);
        }
    }
    public static class SMsgVerdict implements uga.corse.themis.measurements.size.MsgSize<MessageVerdict> {
        @Override
        public Long getSize(MessageVerdict msg, Size size) {
            return size.get(DataSize.S_ID)
                    + size.get(DataSize.S_TIME)
                    + size.get(DataSize.S_VER);
        }
    }

    @Override
    public void prestart(Node n, Map<String, Serializable> tags) {
        Config.register(MessageKill.class.getName()     , new SMsgKill());
        Config.register(MessageVerdict.class.getName()  , new SMsgVerdict());
    }

    @Override
    public void end() {
        Config.unregister(MessageKill.class.getName());
        Config.unregister(MessageVerdict.class.getName());
    }
}