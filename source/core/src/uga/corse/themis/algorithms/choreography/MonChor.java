package uga.corse.themis.algorithms.choreography;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.VerdictTimed;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.monitoring.MonitorRound;
import uga.corse.themis.monitoring.ehe.*;
import uga.corse.themis.monitoring.memory.*;
import uga.corse.themis.monitoring.GeneralMonitor;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.symbols.AtomString;
import uga.corse.themis.utils.Simplifier;
import uga.corse.themis.utils.SimplifierFactory;

/**
 * Choreography Monitor
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MonChor extends GeneralMonitor implements MonitorRound {
    private static Logger log = LogManager.getLogger(MonChor.class);

    //Automaton of specification
    private Automata aut = null;
    //Should the monitor respawn
    private boolean respawn = false;
    //Monitors to inform of verdict
    private Set<Integer> ref = new HashSet<>();
    //Monitors to wait on verdict from
    private Set<Integer> coref = new HashSet<>();
    //Monitors no longer requesting verdict information
    //If kill = ref then this monitor is killed
    transient private Set<Integer> kill;
    transient private Integer t;

    //Inference rules for the automaton
    transient private Representation repr = null;
    //Memory
    transient private Memory<Atom> mem;
    //Set false when killed
    transient private boolean isMonitoring = true;
    //Which timestamp am I monitoring for?
    transient private int monTimestamp = 1;

    transient private Simplifier simplifier;

    transient protected Verdict currentVerdict;

    transient boolean abort = false;


    public MonChor(Integer id, Automata aut) {
        super(id);
        this.aut = aut;
    }



    @Override
    public void reset() {
        isMonitoring = true;
        monTimestamp = 1;

        repr = new Representation(aut,simplifier);
        mem.clear();
        kill.clear();
        repr.tick();
    }

    @Override
    public Verdict getCurrentVerdict() {
        return currentVerdict;
    }

    public Representation getRepresentation() {
        return repr;
    }

    public Automata getAutomata() {
        return aut;
    }


    public void setRespawn(boolean respawn) {
        this.respawn = respawn;
    }

    public boolean isRespawn() {
        return respawn;
    }

    public Set<Integer> getRef() {
        return ref;
    }

    public Set<Integer> getCoref() {
        return coref;
    }

    @Override
    public void setup() {
        mem   = new MemoryAtoms();
        kill  = new HashSet<>();
        abort = false;
        t     = 0;
        currentVerdict  = Verdict.NA;
        simplifier      = SimplifierFactory.spawnDefault();
        reset();
        log.trace("Loaded: " + this.toString());

        reset();
    }

    @Override
    public void cleanup() {
        simplifier.stop();
    }

    @Override
    public MonitorResult monitor(MemoryRO<Atom> observations) throws Exception {

        //Killed?
        if (!isMonitoring) return MonitorResult.CONTINUE;

        //Aborted
        if(abort) return MonitorResult.ABORT_RUN;

        //We keep only 1 processing state
        //(the first is true and denotes the initial/resolved state)
        //if (repr.size() < 2) {
        repr.tick();
        //}

        //Observe
        mem.merge(observations);

        Message m;
        //Process Incoming Messages
        while ((m = recv()) != null) {
            //Handle Kill - Killed do not Respawn
            if (m instanceof MessageKill) {
                int from = ((MessageKill) m).getFrom();
                kill.add(from);
                //Am I still needed?
                //More than one monitor may require my verdict
                if (kill.equals(ref))
                    die();
                return MonitorResult.CONTINUE;
            }
            //Handle Verdicts
            if (m instanceof MessageVerdict) {

                MessageVerdict v = (MessageVerdict) m;
                AtomString atom = LTLMonRef.generateAtom(v.getFrom());
                //Update memory with the verdict for a given timestamp
                mem.add(new AtomObligation(atom, v.getTimestamp()),
                        v.getVerdict() == Verdict.TRUE);
            }
        }

        //Attempt to resolve
        boolean updated = repr.update(mem, -1);

        //Resolved
        while (updated) {
            //Get Earliest Verdict
            VerdictTimed v = repr.scanVerdict();
            log.trace(id + " at <" + monTimestamp +", " + repr.getLastResolvedState() + ">");


            //Verdict Reached
            if (v.isFinal()) {
                //Root node : Report final verdict
                if (ref.isEmpty()) {
                    currentVerdict = v.getVerdict();
                    abort = true;
                    return MonitorResult.VERDICT;
                }//Not root
                else {
                    //Inform all my ref
                    for (Integer id : ref) {
                        //Unless they sent me a kill
                        if (kill.contains(id)) continue;
                        send(id, new MessageVerdict(v.getVerdict(), monTimestamp, getID()));
                    }

                    //Respawn or die
                    if (isRespawn()) respawn();
                    else die();
                }

            }
            //Cleanup resolved states
            repr.dropResolved();
            //Keep going so long as I havent reached the timestamp
            if (repr.getEnd() <= t){//&& repr.size() < 2) {
                repr.tick();
            }
            //Attempt to resolve again
            updated = repr.update(mem, -1);
        }
        return MonitorResult.CONTINUE;
    }


    /**
     * Respawn to monitor the property monTimestamp + 1
     */
    private void respawn() {

        isMonitoring = true;
        repr.reset(monTimestamp);
        monTimestamp++;
        log.trace("Monitor " + getID() + ": Respawn @ " + monTimestamp);
        repr.tick();
    }

    /**
     * Die and send appropriate Kill messages
     */
    private void die() {
        log.trace("Monitor " + getID() + ": Died");
        isMonitoring = false;
        mem.clear();
        for (Integer ref : coref)
            send(ref, new MessageKill(getID()));
    }

    @Override
    public void communicate() {
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[").append(respawn ? "(R) " : "").append(getID())
                .append(" , -> ").append(ref.toString())
                .append(" , <- ").append(coref.toString())
                .append(" ]");
        return sb.toString();
    }

    @Override
    public void notifyRound(int round) {
        t = round;
    }
}
