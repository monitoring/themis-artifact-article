package uga.corse.themis.algorithms.choreography;

import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.symbols.AtomString;

/**
 * LTL Symbol to denote a reference to a monitor
 * The prefix is used to encode the symbol so that it can be passed ot the simplifier
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class LTLMonRef implements LTLExpression {
    public static final String MON_PREFIX = "m_";
    private Integer id;

    /**
     * Generate a reference to the monitor with the given ID
     *
     * @param id Monitor ID to reference
     */
    public LTLMonRef(int id) {
        this.id = id;
    }

    /**
     * Return the ID of the monitor referenced
     *
     * @return Return monitor id
     */
    public Integer getID() {
        return id;
    }

    @Override
    public String toString() {
        return MON_PREFIX + id.toString();
    }

    /**
     * Generate the atomic string from the reference
     * Used to pass to simplified and read it back from simplifier
     * This will be the key that identifies it
     * (Make sure it is different from normal atoms)
     *
     * @param id Monitor ID
     * @return An atom that can be passed to simplifier and read back
     */
    public static AtomString generateAtom(int id) {
        return new AtomString(MON_PREFIX + id);
    }
}
