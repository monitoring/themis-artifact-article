package uga.corse.themis.algorithms.choreography;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import uga.corse.themis.Topology;
import uga.corse.themis.algorithms.choreography.MonitorNetwork.NetNode;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.expressions.ltl.LTLBinary;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.expressions.ltl.LTLSymbol;
import uga.corse.themis.expressions.ltl.LTLUnary;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.specifications.SpecAutomata;
import uga.corse.themis.monitoring.specifications.SpecLTL;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.utils.Convert;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;
import uga.corse.themis.utils.mapper.SymbolMapper;

/**
 * The Choreography Monitoring Algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Choreography implements StandardMonitoring {

    public SpecLTL spec;

    @Override
    public void setSpecification(Map<String, Specification> specs) {
        Specification spec = specs.get("root");
        if(spec == null)
            throw new IllegalArgumentException("No Root Spec found");
        setMainSpec(spec);
    }

    public void setMainSpec(Specification spec) {
        if(!(spec instanceof  SpecLTL))
            throw new IllegalArgumentException("Specification must be LTL ");
        this.spec = ((SpecLTL) spec);
    }

    @Override
    public Set<? extends Monitor> getMonitors(Topology topology) {

        LTLExpression spec = this.spec.getLTL();
        SymbolMapper mapper =  new DefaultSymbolMapper();
        Score data = Score.getScoreTree(spec, mapper);
        Map<Integer, MonChor> mons = new HashMap<>();

        resetId(0);
        //Split the LTL into different monitors
        MonitorNetwork net = splitFormula(newId(), data.comp, data, spec, false);
        compactNetwork(net);
        //Compute Respawn for each monitor
        Set<Integer> resp = respawn(net, net.root.spec, false);

        //Initialize and Attach Monitors
        for (NetNode node : net.nodes.values()) {

            //Generate Automaton
            SpecAutomata monitorSpec = Convert.makeAutomataSpec(new SpecLTL(node.spec));
            if(monitorSpec == null)
                throw new IllegalArgumentException("No Automata Generated for: " + node.spec.toString());
            Automata aut = monitorSpec.getAut();
            if(aut == null)
                throw new IllegalArgumentException("Null Automata Generated for: " + node.spec.toString());
            aut = Convert.simplifyTransitions(aut);

            //Create Monitor
            MonChor mon = new MonChor(node.id, aut);
            mon.setRespawn(resp.contains(node.id));
            mon.setComponentKey(node.comp);
            mons.put(node.id, mon);
        }

        //Update: Ref/CoRef
        for (Entry<Integer, Set<Integer>> entry : net.edges.entrySet()) {
            Integer from = entry.getKey();
            for (Integer to : entry.getValue()) {
                mons.get(from).getRef().add(to);
                mons.get(to).getCoref().add(from);
            }
        }
        /*
        int depth = depth(0, mons);
        for (MonChor mon : mons.values())
            mon.setWindow(depth);
            */

        return new HashSet<>(mons.values());
    }



    private static int monid = 0;

    public static void resetId(int start) {
        monid = start;
    }

    public static int newId() {
        return monid++;
    }

    public static MonitorNetwork compactNetwork(MonitorNetwork net) {
        HashMap<Integer, Integer> compacted = new HashMap<>();

        HashSet<NetNode> nodes = new HashSet<>(net.nodes.values());
        while (!nodes.isEmpty()) {
            Iterator<NetNode> iter = nodes.iterator();
            NetNode pick = iter.next();
            while (iter.hasNext()) {
                NetNode consider = iter.next();
                if (consider.spec.toString().equals(pick.spec.toString())) {
                    compacted.put(consider.id, pick.id);
                    iter.remove();
                }
            }
            nodes.remove(pick);
        }
        //Adjust edges
        //Redirect from replaced
        for (Entry<Integer, Integer> entry : compacted.entrySet()) {
            //No Edges outbound from redirect
            if (!net.edges.containsKey(entry.getKey())) continue;

            Set<Integer> toRedirect = net.edges.get(entry.getKey());
            net.edges.get(entry.getValue()).addAll(toRedirect);

            //Remove Edges for replaced node
            net.edges.remove(entry.getKey());

            net.nodes.remove(entry.getKey());
        }
        //Redirect to replaced
        for (Entry<Integer, Set<Integer>> entry : net.edges.entrySet()) {

            Iterator<Integer> iter = entry.getValue().iterator();
            LinkedList<Integer> toAdd = new LinkedList<>();
            while (iter.hasNext()) {
                Integer to = iter.next();
                if (compacted.containsKey(to)) {
                    iter.remove();
                    toAdd.add(compacted.get(to));
                }
            }
            for (Integer add : toAdd)
                entry.getValue().add(add);
        }
        for (NetNode node : net.nodes.values())
            node.spec = replaceReferences(node.spec, compacted);

        return net;
    }

    public static LTLExpression replaceReferences(LTLExpression expr, Map<Integer, Integer> map) {
        if (expr instanceof LTLUnary) {
            return new LTLUnary(((LTLUnary) expr).getOperator(),
                    replaceReferences(((LTLUnary) expr).getOperand(), map));
        } else if (expr instanceof LTLBinary) {
            LTLExpression left = replaceReferences(((LTLBinary) expr).getLeft(), map);
            LTLExpression right = replaceReferences(((LTLBinary) expr).getRight(), map);
            return new LTLBinary(((LTLBinary) expr).getOperator(), left, right);
        } else if (expr instanceof LTLMonRef) {
            Integer id = ((LTLMonRef) expr).getID();
            if (map.containsKey(id))
                return new LTLMonRef(map.get(id));
            else
                return expr;
        } else
            return expr;
    }

    public static MonitorNetwork splitFormula(
            int rootid,
            String comp,
            Score weights,
            LTLExpression formula,
            boolean split
    ) {
        if (formula instanceof LTLSymbol) {
            MonitorNetwork net = new MonitorNetwork();
            net.addNode(rootid, formula, comp, true);
            return net;
        } else if (formula instanceof LTLUnary) {
            //Forward the formula without the unary
            MonitorNetwork net = splitFormula(rootid, comp,
                    weights.subtrees[0], ((LTLUnary) formula).getOperand(), false);

            //No Splitting so append the unary operator
            //Keep the unary in the current component
            if (!split)
                net.root.spec = new LTLUnary(((LTLUnary) formula).getOperator(),
                        net.root.spec);

            return net;
        } else if (formula instanceof LTLBinary) {
            LTLExpression left = ((LTLBinary) formula).getLeft();
            LTLExpression right = ((LTLBinary) formula).getRight();
            Score sleft = weights.subtrees[0];
            Score sright = weights.subtrees[1];
            String cleft = sleft.comp;
            String cright = sright.comp;
            MonitorNetwork net = new MonitorNetwork();
            MonitorNetwork mleft = null;
            MonitorNetwork mright = null;
            //No Split
            if (cleft.equals(cright)) {
                mleft = splitFormula(rootid, comp, sleft, left, false);
                mright = splitFormula(rootid, comp, sright, right, false);


                net.addNode(rootid, new LTLBinary(
                        ((LTLBinary) formula).getOperator(),
                        mleft.root.spec,
                        mright.root.spec), comp, true);
            }
            //Right split
            else if (cleft.equals(comp)) {
                int nid = newId();
                mleft = splitFormula(rootid, comp, sleft, left, false);
                mright = splitFormula(nid, cright, sright, right, true);
                LTLExpression fright = new LTLMonRef(nid);
                if (right instanceof LTLUnary)
                    fright = new LTLUnary(((LTLUnary) right).getOperator(), fright);

                net.addNode(rootid, new LTLBinary(((LTLBinary) formula).getOperator(),
                        mleft.root.spec, fright), comp, true);
                net.addEdge(nid, rootid);
            }
            //Left split
            else {
                int nid = newId();
                mleft = splitFormula(nid, cleft, sleft, left, true);
                mright = splitFormula(rootid, comp, sright, right, false);

                LTLExpression fleft = new LTLMonRef(nid);
                if (left instanceof LTLUnary)
                    fleft = new LTLUnary(((LTLUnary) left).getOperator(), fleft);

                net.addNode(rootid, new LTLBinary(((LTLBinary) formula).getOperator(),
                        fleft, mright.root.spec), comp, true);
                net.addEdge(nid, rootid);
            }
            net.mergeGraphs(mleft, mright);
            return net;
        }
        throw new Error("LTL Expr not supported: "
                + formula.getClass().getSimpleName());
    }

    public static Set<Integer> respawn(MonitorNetwork net, LTLExpression expr, boolean flag) {
        if (expr instanceof LTLMonRef) {
            Integer id = ((LTLMonRef) expr).getID();
            Set<Integer> data = respawn(net, net.nodes.get(id).spec, flag);

            if (flag) data.add(id);
            return data;
        } else if (expr instanceof LTLSymbol) {
            return new HashSet<>();
        } else if (expr instanceof LTLUnary) {
            return respawn(net, ((LTLUnary) expr).getOperand(), true);
        } else if (expr instanceof LTLBinary) {
            HashSet<Integer> data = new HashSet<>();
            switch (((LTLBinary) expr).getOperator()) {
                case LTL_UNTIL:
                case LTL_RELEASE:
                case LTL_GLOBALLY:
                case LTL_FINALLY:
                    data.addAll(respawn(net, ((LTLBinary) expr).getLeft(), true));
                    data.addAll(respawn(net, ((LTLBinary) expr).getRight(), true));
                    break;
                default:
                    data.addAll(respawn(net, ((LTLBinary) expr).getLeft(), flag));
                    data.addAll(respawn(net, ((LTLBinary) expr).getRight(), flag));
            }
            return data;
        }
        throw new Error("Invalid LTL Expr: " + expr.getClass().getSimpleName());
    }

    public static int depth(int root, Map<Integer, MonChor> mons) {
        MonChor mon = mons.get(root);
        Set<Integer> coref = mon.getCoref();
        if (coref.isEmpty())
            return 0;
        else {
            int max = 0;
            for (Integer cref : coref)
                max = Math.max(max, depth(cref, mons));
            return 1 + max;
        }
    }



}
