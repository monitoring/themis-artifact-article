package uga.corse.themis.algorithms.choreography;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import uga.corse.themis.expressions.ltl.LTLBinary;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.expressions.ltl.LTLSymbol;
import uga.corse.themis.expressions.ltl.LTLUnary;
import uga.corse.themis.utils.mapper.SymbolMapper;
import uga.corse.themis.symbols.AtomString;

/**
 * Computes a score tree to be associated with an LTL formula tree
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Score implements Serializable {


    private static final long serialVersionUID = 8594754073235013490L;
    public Map<String, Double> score;
    public String comp = null;
    public Score[] subtrees = null;

    public Score() {
        score = new HashMap<>();
    }

    public void addScores(Score f2) {
        for (Entry<String, Double> entry : f2.score.entrySet()) {
            String comp = entry.getKey();
            Double w = entry.getValue();

            if (score.containsKey(comp))
                w += score.get(comp);
            score.put(comp, w);
        }
    }

    public void addSubtrees(Score... ltl) {
        subtrees = ltl;
    }

    public void recompute() {
        Double max = 0d;
        for (Entry<String, Double> entry : score.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                comp = entry.getKey();
            }
        }
    }

    /**
     * Computes a score tree for a formula.
     * A mapper is required to map an AP -> Component.
     * The score tree computes for each operator the number of references to each components in the subtree.
     *
     * @param formula LTL Expression
     * @param mapper A symbol mapper {@literal (AP -> Component)}
     * @return A tree of scores
     */
    public static Score getScoreTree(LTLExpression formula, SymbolMapper mapper) {
        if (formula instanceof LTLSymbol) {
            AtomString atom = new AtomString(((LTLSymbol) formula).getSym());
            String component = mapper.componentFor(atom);
            Score re = new Score();
            re.score.put(component, 1d);
            re.recompute();
            return re;
        } else if (formula instanceof LTLUnary) {
            Score sub = getScoreTree(((LTLUnary) formula).getOperand(), mapper);
            Score root = new Score();
            root.addScores(sub);
            root.addSubtrees(sub);
            root.recompute();
            return root;
        } else if (formula instanceof LTLBinary) {
            Score left = getScoreTree((LTLExpression) ((LTLBinary) formula).getLeft(), mapper);
            Score right = getScoreTree((LTLExpression) ((LTLBinary) formula).getRight(), mapper);
            Score root = new Score();
            root.addScores(left);
            root.addScores(right);
            root.addSubtrees(left, right);
            root.recompute();
            return root;
        } else
            throw new Error("[Not Supported] LTLExpression : "
                    + formula.getClass().getSimpleName());
    }
}
