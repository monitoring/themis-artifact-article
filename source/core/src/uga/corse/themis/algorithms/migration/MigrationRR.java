package uga.corse.themis.algorithms.migration;


import uga.corse.themis.automata.Automata;

/**
 * Migration Monitoring Algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MigrationRR extends MigrationAlg<MonMigrateRR> {
    @Override
    protected MonMigrateRR newMonitor(int id, Automata aut, Integer comps) {
        return new MonMigrateRR(id, aut, comps);
    }
}
