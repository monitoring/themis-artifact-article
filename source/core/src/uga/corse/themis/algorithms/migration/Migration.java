package uga.corse.themis.algorithms.migration;


import uga.corse.themis.automata.Automata;

/**
 * Migration Monitoring Algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Migration extends MigrationAlg<MonMigrate> {
    @Override
    protected MonMigrate newMonitor(int id, Automata aut, Integer comps) {
        return new MonMigrate(id, aut, comps);
    }
}
