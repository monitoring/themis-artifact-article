package uga.corse.themis.algorithms.migration;


import uga.corse.themis.comm.packets.RepresentationPacket;
import uga.corse.themis.measurements.Config;
import uga.corse.themis.measurements.RunHook;
import uga.corse.themis.measurements.size.PacketEHE;
import uga.corse.themis.runtime.Node;

import java.io.Serializable;
import java.util.Map;

public aspect MsgSize extends RunHook {

    @Override
    public void prestart(Node n, Map<String, Serializable> tags) {
        Config.register(RepresentationPacket.class.getName(), new PacketEHE());
    }

    @Override
    public void end() {
        Config.unregister(RepresentationPacket.class.getName());
    }
}