package uga.corse.themis.algorithms.migration;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uga.corse.themis.Topology;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.ehe.Representation;
import uga.corse.themis.monitoring.specifications.SpecAutomata;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.utils.Convert;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;

/**
 * Migration Monitoring Algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public abstract class MigrationAlg<T extends MonMigrate> implements StandardMonitoring {
    protected Automata aut;

    @Override
    public void setSpecification(Map<String, Specification> specs) {
        Specification main = specs.get("root");
        if(main == null)
            throw new IllegalArgumentException("No Root Spec found");
        setMainSpec(Convert.makeAutomataSpec(main));
    }

    public void setMainSpec(Specification spec) {
        if(!(spec instanceof SpecAutomata))
            throw new IllegalArgumentException("Specification must be an automaton!");
        aut = ((SpecAutomata) spec).getAut();
        if(aut == null)
            throw new IllegalArgumentException("Automaton is not valid");
        aut = Convert.simplifyTransitions(aut);
    }
    @Override
    public Set<? extends Monitor> getMonitors(Topology topology) {


        int startID = getStartID(aut);
        int c = topology.getCountComponents();

        Set<T> mons = new HashSet<>(c);

        //Generate one monitor per component
        Integer i = 0;
        for (String comp : topology.getGraph().keySet()) {

            T mon = newMonitor(i, aut, c);
            mon.setComponentKey(comp);
            mons.add(mon);
            if(startID == i) mon.setStart(true);
            i++;
        }

        return mons;
    }

    abstract protected T newMonitor(int id, Automata aut, Integer comps);

    public static int getStartID(Automata aut) {

            Representation ehe = new Representation(aut);

            //Move one to determine if we should start monitoring
            ehe.tick();

            //Find next obligation
            AtomObligation early = Expressions.findEarliestObligation(ehe, true);


            return (new DefaultSymbolMapper()).monitorFor(early);
    }
}
