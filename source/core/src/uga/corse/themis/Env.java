package uga.corse.themis;

import uga.corse.themis.utils.Opts;

/**
 * Environment Variables.
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public enum Env {


    LTL2MON_BIN("THEMIS_LTL2MON_BIN"), TIMEOUT("THEMIS_SUBPROCESS_TIMEOUT"), SPOT_LTLFILT("THEMIS_SPOT_LTLFILT");


    private String envname;

    /**
     * Instantiates a new env.
     *
     * @param name the name
     */
    private Env(String name) {
        this.envname = name;
    }

    /**
     * Return the name of the environment variable.
     *
     * @return the key
     */
    public String getKey() {
        return envname;
    }

    /**
     * Return the value of the environment variable.
     *
     * @param def the def
     * @return the value
     * @see Opts#getGlobal(String, String)
     */
    public String getValue(String def) {
        return Opts.getGlobal(envname, def);
    }
}
