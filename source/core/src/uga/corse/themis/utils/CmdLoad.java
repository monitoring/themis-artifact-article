/**
 * SpecLoader
 * <p>
 * name <email>
 */
package uga.corse.themis.utils;


import uga.corse.themis.comm.commands.CmdRuntime;
import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.runtime.Runtime;

import java.io.IOException;
import java.io.ObjectInputStream;

public class CmdLoad {
    // Call CmdX when MsgX is received
    // Classname prefix for commands
    final public static String CMD_PREFIX  = "uga.corse.themis.comm.commands.Cmd";
    // Prefix to trim from Message classname
    final public static int    CMD_TRIM    = 3;



    public static CmdRuntime load(MsgRuntime msg)
    throws ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        return loadCommand(msg, CMD_PREFIX, CMD_TRIM);
    }

    public static CmdRuntime loadCommand(MsgRuntime msg, String prefix, int trim)
    throws  ClassNotFoundException, InstantiationException, IllegalAccessException
    {
        CmdRuntime cmd;
        // Initialize the proper Command Object
        Class<CmdRuntime> cls = (Class<CmdRuntime>)
                Runtime.class.getClassLoader()
                        .loadClass(prefix +
                                msg.getClass().getSimpleName().substring(trim));
        cmd = cls.newInstance();
        return cmd;
    }


}
