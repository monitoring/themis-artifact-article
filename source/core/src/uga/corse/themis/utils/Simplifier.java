package uga.corse.themis.utils;

import uga.corse.themis.expressions.bool.BoolExpression;

import java.io.Serializable;

/**
 * A boolean expressions simplifier
 * Needed to simplify expressions for automata representation
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Simplifier extends Serializable {
    BoolExpression simplify(BoolExpression e);
    String simplify(String e);
    void stop();
}
