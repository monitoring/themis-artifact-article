package uga.corse.themis.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Env;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.expressions.bool.BoolSym;
import uga.corse.themis.parser.FrontExpressionParsers;

/**
 * Simplifier that uses ltlfilt to simplify
 * ltlfilt has some memory leaks so we reset the process every once and a while
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 * @see <a href="https://spot.lrde.epita.fr/ltlfilt.html">Spot's ltlfilt page</a>
 */
public class SimplifierLTLFILT implements Simplifier, AutoCloseable {
    private static Logger log = LogManager.getLogger(SimplifierLTLFILT.class);

    private static int RENEW = 100;
    private transient Process p = null;
    private transient PrintWriter write;
    private transient BufferedReader scan;
    private transient int simplifications = 0;

    public SimplifierLTLFILT() {
        boot();
    }

    private void boot() {
        simplifications = 0;
        try {
            if (p != null) {
                close();
            }
            p = Convert.runProc(Env.SPOT_LTLFILT.getValue("ltlfilt"), new Object[]{"--boolean-to-isop"});
            write = new PrintWriter(p.getOutputStream());
            scan = new BufferedReader(new InputStreamReader(p.getInputStream()));
        } catch (Exception e) {
            throw new Error("Could not start simplifier: " + e.getLocalizedMessage());
        }
    }

    public BoolExpression simplify(BoolExpression e) {
        if (e instanceof BoolSym) return e;
        renew();
        Map<String, BoolSym> syms = new HashMap<String, BoolSym>();
        BoolExpression expr = Expressions.reformatBoolean(e, syms);
        try {
            String simplified = simplify(expr.toString());
            return FrontExpressionParsers.parseStringBoolFILT(simplified, syms);
        }
        catch(StackOverflowError stoverflow) {
            log.warn("Could not parse output, stack overflow");
            return e;
        }
    }

    public String simplify(String e) {
        renew();
        write.println(e);
        write.flush();
        try {
            return scan.readLine();
        } catch (IOException ex) {
            throw new Error("Could not simplify: " + e);
        }
    }

    private void renew() {
        if (simplifications++ < RENEW) return;
        boot();
    }

    @Override
    public void close() throws Exception {
        write.close();
        p.destroy();
        p = null;
    }

    private void readObject(java.io.ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        boot();
    }

    @Override
    public void stop() {
        try {
            close();
        }
        catch(Exception e) {}
    }
}
