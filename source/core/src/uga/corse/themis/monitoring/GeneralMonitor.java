package uga.corse.themis.monitoring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.comm.protocol.Message;
import uga.corse.themis.comm.protocol.Network;

/**
 * A simple monitor with basic functionality
 *
 * Assumes network is reliable, it suppresses all network errors
 *
 * Basic monitor sets/gets ID and has send/recv
 * Needs to be extended to implement monitor() and communicate()
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public abstract class GeneralMonitor implements Monitor {
    private static Logger log = LogManager.getLogger(GeneralMonitor.class);

    /**
     * Monitor ID
     */
    protected Integer id;

    /**
     * Monitor Algorithm Reference
     */
    protected transient Network net;

    protected String component;

    /**
     * Create a monitor with a given ID
     *
     * @param id ID to give
     */
    public GeneralMonitor(Integer id) {
        this.id = id;
    }

    /**
     * Set the monitor ID
     *
     * @param id ID to give
     */
    public void setID(Integer id) {
        this.id = id;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public void send(int to, Message msg) {
        try {
            log.trace(getID() + " -> " + to + " : " + msg.getClass().getSimpleName());
            net.put(to, msg);
        } catch(Exception e) {
            log.warn(e.getLocalizedMessage(), e);
        }
    }


    public Message recv() {
        try {
            Message msg = net.get(getID(), true);
            if(msg != null)
                log.trace(getID() + " <- " + msg.getClass().getSimpleName());
            return msg;
        } catch(Exception e) {
            log.warn(e.getLocalizedMessage(), e);
            return null;
        }
    }

    @Override
    public Message recv(boolean consume) {
        try {
            return net.get(getID(), consume);
        } catch(Exception e) {
            log.warn(e.getLocalizedMessage(), e);
            return null;
        }
    }

    public Network getNetwork() {
        return net;
    }

    public void setNetwork(Network net) {
        this.net = net;
    }


    @Override
    public void setComponentKey(String attachedComponent) {
        this.component = attachedComponent;
    }

    @Override
    public String getComponentKey() {
        return this.component;
    }

    public void cleanup() {}
}
