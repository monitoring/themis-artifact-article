package uga.corse.themis.monitoring.components;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.periphery.Periphery;
import uga.corse.themis.runtime.Runtime;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class AsyncObservations extends Component {


    List<Periphery> input = new LinkedList<>();

    public AsyncObservations(String id) {
        super(id);
    }
    synchronized public void addInput(Periphery per) {
            input.add(per);
    }

    public Memory<Atom> observe() throws Exception  {
        Memory<Atom> symbols = new MemoryAtoms();
        if(input.isEmpty()) { return symbols; }

        List<Future<Memory<Atom>>> tasks = new LinkedList<>();

        for(final Periphery peri : input)
            tasks.add(Runtime.getScheduler().submit(new Callable<Memory<Atom>>() {
                @Override
                public Memory<Atom> call() throws Exception {
                    return peri.next();
                }
            }));

        for(Future<Memory<Atom>> task : tasks)
            symbols.merge(task.get());


        return symbols;
    }
    public void shutdown() {
        for(Periphery peri : input) {
            peri.stop();
        }
    }
}
