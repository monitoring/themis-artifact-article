package uga.corse.themis.monitoring;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.periphery.Periphery;

/**
 * Wrapper that represents a component
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public abstract class Component {
    private String name;

    /**
     * Name of the component
     *
     * @param name Name of the component
     */
    public Component(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Component)
            return ((Component) obj).name.equals(name);
        else
            return false;
    }

    /**
     * Return the component name
     *
     * @return The name of the component
     */
    public String getName() {
        return name;
    }

    /**
     * Set the component name
     *
     * @param name New name to give to the component
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public abstract void addInput(Periphery per);
    public abstract  Memory<Atom> observe() throws Exception;
    public void shutdown() {}

}
