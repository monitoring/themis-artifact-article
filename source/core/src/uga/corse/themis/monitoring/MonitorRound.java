package uga.corse.themis.monitoring;

/**
 * Round based monitor
 */
public interface MonitorRound extends Monitor {
   void notifyRound(int round);
}
