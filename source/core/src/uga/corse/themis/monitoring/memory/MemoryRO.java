package uga.corse.themis.monitoring.memory;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;

import java.util.Collection;
import java.util.Collections;

/**
 * Read-only memory
 */
public class MemoryRO<K> implements Memory<K> {
    private Memory<K> data;

    public MemoryRO(Memory<K> memory) {
        data = memory;
    }

    public Verdict get(Atom atom)      { return data.get(atom);  }

    public Verdict get(Event event)    { return data.get(event); }

    public boolean isEmpty()           { return data.isEmpty();  }

    public Collection<K> getData()     {
        return Collections.unmodifiableCollection(data.getData());
    }

    @Override
    public void add(Atom atom, boolean b) {
        throw new Error("Cannot Modify Memory (Read-Only)");
    }

    @Override
    public void merge(Memory<K> memory) {
        throw new Error("Cannot Modify Memory (Read-Only)");
    }

    @Override
    public void clear() {
        throw new Error("Cannot Modify Memory (Read-Only)");
    }

    @Override
    public void remove(Collection<K> collection) {
        throw new Error("Cannot Modify Memory (Read-Only)");
    }

    public Object clone() throws CloneNotSupportedException {
        return new MemoryRO<K>((Memory<K>)data.clone());
    }

    @Override
    public String toString() {
        return "RO{"+ data.toString() +'}';
    }
}
