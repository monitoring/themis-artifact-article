package uga.corse.themis.monitoring.memory;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.symbols.AtomNegation;

import java.util.Collection;
import java.util.HashSet;

/**
 * A memory that stores atoms
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class MemoryAtoms implements Memory<Atom> {

    private static final long serialVersionUID = -1915238386051617579L;
    HashSet<Atom> mem;

    public MemoryAtoms() {
        mem = new HashSet<>();
    }

    @Override
    public void add(Atom a, boolean observed) {
        if (!observed)
            a = new AtomNegation(a);
        mem.add(a);
    }

    /**
     * Has {a} been observed?
     * If !a is in memory it returns FALSE
     * If a is not in the memory it returns NA
     * Otherwise it returns true
     *
     * @param a Atom to check
     * @return Verdict associated with Atom
     */
    @Override
    public Verdict get(Atom a) {
        if (mem.contains(new AtomNegation(a)))
            return Verdict.FALSE;
        else if (!mem.contains(a))
            return Verdict.NA;
        else
            return Verdict.TRUE;
    }

    /**
     * Is the Event found in the memory?
     * TRUE		: if all sub atoms are in the memory
     * NA   	: if one or more are missing
     * FALSE	: if a negation is encoutered
     *
     * @param e Event
     * @return Verdict associated with event
     */
    @Override
    public Verdict get(Event e) {
        // An Empty event is always associated with false
        // To designate special no-observations an event can contain Atom.empty()
        // And it will be handled via Event.hasEmpty()
        if (e.empty()) return Verdict.FALSE;

        for (Atom a : e.getAtoms()) {
            Verdict v = get(a);
            switch (v) {
                case FALSE:
                    return Verdict.FALSE;
                case NA:
                    return Verdict.NA;
                default:
                    break;
            }
        }
        return Verdict.TRUE;
    }

    /**
     * Hard Clone
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object clone() throws CloneNotSupportedException {
        MemoryAtoms m = new MemoryAtoms();
        m.mem = (HashSet<Atom>) mem.clone();
        return m;
    }

    @Override
    public Collection<Atom> getData() {
        return mem;
    }

    @Override
    public boolean isEmpty() {
        return mem.isEmpty();
    }

    @Override
    public String toString() {
        return mem.toString();
    }

    @Override
    public void merge(Memory<Atom> m) {
        mem.addAll(m.getData());
    }

    @Override
    public void clear() {
        mem.clear();
    }

    @Override
    public void remove(Collection<Atom> keys) {
        mem.removeAll(keys);
    }


}
