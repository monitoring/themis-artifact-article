package uga.corse.themis.monitoring.specifications;

import java.io.File;
import java.util.Scanner;

import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.utils.Expressions;

/**
 * Specification using LTL
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class SpecLTL implements Specification {
    private LTLExpression ltl;

    /**
     * Create an empty spec
     */
    public SpecLTL() {

    }

    /**
     * Create a new LTL specification by parsing the string
     *
     * @param expr String to parse as LTL
     */
    public SpecLTL(String expr) {
        setLTL(expr);
    }

    /**
     * Create a new LTL specification by using an LTL expression
     *
     * @param expr Use existing LTL expression as the spec
     */
    public SpecLTL(LTLExpression expr) {
        setLTL(expr);
    }

    /**
     * Create a new LTL specification by parsing the first line in the file
     *
     * @param fname Path to file
     */
    public void fromFile(String fname) {
        File f = new File(fname);
        Scanner scan;
        try {
            scan = new Scanner(f);
            setLTL(scan.nextLine());
            scan.close();
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Set the LTL spec by parsing string
     *
     * @param ltl String to parse as an LTL expression
     */
    public void setLTL(String ltl) {
        this.ltl = Expressions.parseLTL(ltl);
    }

    /**
     * Set the LTL spec to the expression
     *
     * @param ltl LTL expression to use as spec
     */
    public void setLTL(LTLExpression ltl) {
        this.ltl = ltl;
    }

    /**
     * @return the LTL expression that this specification uses
     */
    public LTLExpression getLTL() {
        return ltl;
    }

    @Override
    public String toString() {
        return ltl.toString();
    }
}
