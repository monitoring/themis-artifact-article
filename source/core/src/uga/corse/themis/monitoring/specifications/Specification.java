package uga.corse.themis.monitoring.specifications;

import java.io.Serializable;

/**
 * A specification for the algorithm
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface Specification extends Serializable {
}
