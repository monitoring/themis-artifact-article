package uga.corse.themis.monitoring.specifications;

import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.formats.FormatExpression;
import uga.corse.themis.parser.FrontAutomataParser;

/**
 * Specification using Automata
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class SpecAutomata implements Specification {
    Automata aut;

    /**
     * Create an empty specification
     */
    public SpecAutomata() {

    }

    /**
     * Create a new specification given an automaton
     *
     * @param aut
     */
    public SpecAutomata(Automata aut) {
        this.aut = aut;
    }

    /**
     * Return the automaton
     *
     * @return
     */
    public Automata getAut() {
        return aut;
    }


    /**
     * Load an automaton from a file
     * Format is DOT format obtained from LTL3Tools
     *
     * @param filename
     * @see <a href="http://ltl3tools.sourceforge.net/">LTL3Tools</a>
     */
    public void loadFile(String filename) {
        try {
            aut = FrontAutomataParser.load(filename,
                    new FormatExpression());
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
