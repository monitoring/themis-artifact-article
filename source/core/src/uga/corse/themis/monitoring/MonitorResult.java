package uga.corse.themis.monitoring;

/**
 * Result of a monitor
 */
public enum MonitorResult {
    /**
     * Continue Execution
     */
    CONTINUE,
    /**
     * Verdict Changed!
     */
    VERDICT,
    /**
     * Abort execution on Local node
     */
    ABORT_NODE,
    /**
     * Abort entire execution
     */
    ABORT_RUN;

    public boolean isAbort() {
        return this == ABORT_NODE || this == ABORT_RUN;
    }
}
