package uga.corse.themis.monitoring.ehe;

import java.io.Serializable;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.InvalidStateException;
import uga.corse.themis.automata.State;
import uga.corse.themis.automata.Transition;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.VerdictTimed;
import uga.corse.themis.expressions.bool.BoolAnd;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.expressions.bool.BoolOr;
import uga.corse.themis.expressions.bool.False;
import uga.corse.themis.expressions.bool.True;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.monitoring.memory.MemoryAtoms;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.Simplifier;

import java.util.Map.Entry;

/**
 * The Execution Encoding History of an automaton.
 * We use Rule as a terminology to indicate the entry at a timestamp.
 * An EHE is maintained between an interval of encodings defined by start-end.
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 * @see Rule
 */
public class Representation implements Cloneable, Serializable {
    private static Logger log = LogManager.getLogger(Representation.class);

    private static final long serialVersionUID = 5378333636127741478L;
    private Automata aut;
    private Map<Integer, Rule> rules;
    private Map<Integer, Verdict> verdicts;
    private int start = 0;
    private int end = 0;
    private int lastResolved = 0;
    private int lastVerdict = 0;
    private transient Simplifier simplifier = null;
    private transient State lastResolvedState = null;


    private Representation() {
        rules = new HashMap<>();
        verdicts = new HashMap<>();
        start = end = lastResolved = lastVerdict = 0;
    }

    /**
     * Create a newExecution History Encoding from an automaton
     *
     * @param a Automaton to encode
     */
    public Representation(Automata a) {
        this();
        if(a == null || a.getStates().size() <= 0 || a.getDelta().size() <= 0)
            throw new IllegalArgumentException("Invalid Automaton: " + a);
        aut = a;
        addInitRule();
        lastResolvedState = aut.getInitialState();
    }

    /**
     * Create a new Execution History Encoding from an automaton that uses a simplifier
     *
     * @param a Automaton to encode
     * @param simplify Simplifier to call for simplifying expressions
     */
    public Representation(Automata a, Simplifier simplify) {
        this(a);
        simplifier = simplify;
    }

    /**
     * Create the initial state rule for the start timestamp
     *
     * @see Representation#addInitRule(int)
     */
    public void addInitRule() {
        addInitRule(start);
    }

    /**
     * Create the initial state rule at a timestamp
     * [{@literal timestamp -> q_0 -> true}]
     *
     * @param timestamp Set the timestamp for the initial rule
     */
    public void addInitRule(int timestamp) {
        Rule r = new Rule(simplifier);
        r.add(aut.getInitialState(), new True());
        rules.put(timestamp, r);
        verdicts.put(timestamp, aut.getVerdict());
    }

    /**
     * Reset the execution to start from timestamp + 1
     * This is done by setting the initial state at timestamp t with expression "true"
     *
     * @param timestamp Set the timestamp to reset the state at
     */
    public void reset(int timestamp) {
        rules.clear();
        verdicts.clear();
        start = end = lastResolved = lastVerdict = timestamp;
        addInitRule(timestamp);
    }

    @SuppressWarnings("serial")
    private static class ERule extends TreeMap<State, BoolExpression> {
    }

    /**
     * Update ruleset with timestamp t+1.
     * Takes previous rules and appends the transitions going from those states.
     * This corresponds to a mov operation of 1
     */
    public void tick() {
        int timestamp = end + 1;
        Rule update = new Rule(simplifier);
        Map<State, List<Transition>> delta = aut.getDelta();
        Map<State, ERule> build = new TreeMap<>();

        //Where am I?
        for (Entry<State, BoolExpression> e : rules.get(end).getPairs().entrySet()) {
            //What can I reach?
            for (Transition t : delta.get(e.getKey())) {
                State to = t.getTo();
                State from = t.getFrom();
                BoolExpression expr = Expressions.fromTransition(t, timestamp);


                if (!build.containsKey(to))
                    build.put(to, new ERule());
                ERule ruleEntry = build.get(to);

                //From where can I reach it?
                if (!ruleEntry.containsKey(from))
                    ruleEntry.put(from, expr);
                else
                    ruleEntry.put(from, new BoolOr(ruleEntry.get(from), expr));
            }

        }

        for (Entry<State, ERule> e : build.entrySet()) {
            BoolExpression expr = new False();
            for (Entry<State, BoolExpression> r : e.getValue().entrySet()) {
                expr = new BoolOr(expr, new BoolAnd(
                        rules.get(end).get(r.getKey()),
                        r.getValue()));
            }
            update.add(e.getKey(), expr);
        }

        rules.put(timestamp, update);
        verdicts.put(timestamp, Verdict.NA);
        end++;
    }

    /**
     * @return Lowest timestamp in the EHE (interval)
     */
    public int getStart() {
        return start;
    }

    /**
     * Set the lowest timestamp of the EHE interval
     *
     * @param start timestamp to use as a lower bound
     */
    public void setStart(int start) {
        this.start = start;
    }

    /**
     * @return Highest timestamp in the EHE (interval)
     */
    public int getEnd() {
        return end;
    }

    /**
     * Set the maximal timestmap in this encoding
     *
     * @param end timestamp to use as an upper bound
     */
    public void setEnd(int end) {
        this.end = end;
    }

    /**
     * Rewrites the ehe system starting from timestamp
     * (1) By simplifying expressions
     * (2) By rewriting statements based on contents on mem
     * Returns true if a state can be reached and verdict was updated
     *
     * @param mem Memory to use for evaluation of rules
     * @param timestampFrom Only update from this timestamp onwards
     * @return true if updated
     */
    @SuppressWarnings("rawtypes")
    public boolean update(Memory mem, Integer timestampFrom) {
        return update(mem, timestampFrom, -1);
    }

    /**
     * Updates up to a certain depth
     * Depth = number of rules before giving up.
     *
     * See: {@link #update(Memory, Integer)}
     * @param mem Memory to use for update
     * @param depth Depth to use
     * @return true if updated
     */
    @SuppressWarnings("rawtypes")
    public boolean updateDepth(Memory mem, int depth) {
        return update(mem, lastResolved + 1, depth);
    }


    @SuppressWarnings("rawtypes")
    public boolean update(Memory mem, Integer timestampFrom, int depth) {
        boolean updated = false;
        int from = Math.max(timestampFrom, lastResolved + 1);

        for (int k = from; k <= end; k++) {
            Rule r = rules.get(k);
            Verdict v = verdicts.get(k);

            //No need to update a resolved verdict
            if (v.isFinal())
                continue;

            State s = null;
            try {
                if(r.update(mem))
                    s = r.resolve(mem);
            }catch(StackOverflowError over) {
                log.warn("Cannot resolve: Stack overflow for rules @ timestamp " + k + " (bool expressions are too deep)");
            }

            if (s == null) {
                depth--;
                if (depth == 0)
                    return updated;
                else
                    continue;
            }
            lastResolvedState = s;
            lastResolved = Math.max(lastResolved, k);
            try {
                Verdict verdict = aut.getVerdict(s);
                verdicts.put(k, verdict);

                if (verdict.isFinal())
                    lastVerdict = k;
                updated = true;
            } catch (InvalidStateException e) {
                System.err.print("Could not update verdict: no verdict found for " + s);
            }
        }
        return updated;
    }

    public State getLastResolvedState() {
        return lastResolvedState;
    }

    /**
     * Drop all rules before the resolved (known) state
     * A state is known if it contains one expression that evaluates to true
     *
     * @return Number of timestamps discarded
     */
    public int dropResolved() {
        return drop(lastResolved);
    }

    /**
     * Discard all rules prior to the given timestamp
     * All rules between lower interval and timestamp are discarded
     *
     * @param from Timestamp to stop at
     * @return Number of timestamps discarded
     */
    public int drop(int from) {
        int toRemove = from - start;
        for (int x = start; x < from; x++) {
            rules.remove(x);
            verdicts.remove(x);
        }

        start = from;
        return toRemove;
    }

    /**
     * Returns a new encoding starting at the timestamp
     *
     * @param from Starting from this timestamp
     * @return A new EHE
     * @see Representation#slice(int, int)
     */
    public Representation slice(int from) {
        return slice(from, end);
    }

    /**
     * Returns a new encoding starting from the last known state
     *
     * @return A new EHE
     * @see Representation#slice(int, int)
     * @see Representation#dropResolved()
     */
    public Representation sliceLive() {
        return slice(lastResolved, end);
    }

    /**
     * Returns a new encoding that encodes the execution between two timestamps
     * This method clones the EHE and returns a new one
     *
     * @param from Timestamp that specifies the start of the interval
     * @param to Timestamp that specifies the end of the interval
     * @return A new EHE
     */
    public Representation slice(int from, int to) {

        Representation newRep = new Representation(aut, simplifier);

        from = Math.max(from, start);


        newRep.start = from;
        newRep.end = from;
        newRep.lastResolved = lastResolved;

        newRep.rules.clear();
        newRep.verdicts.clear();

        //No slices to give
        if (to < from)
            throw new IllegalArgumentException("Requesting EHE with end < from");

        to = Math.min(to, end);
        while (from <= to) {
            newRep.rules.put(from, rules.get(from));
            newRep.verdicts.put(from, verdicts.get(from));
            from++;
        }
        newRep.end = to;

        return newRep;
    }

    /**
     * Merge an EHE with current EHE
     * Ignores all data before start interval of this EHE
     *
     * @param r2 EHE to merge
     */
    public void mergeForward(Representation r2) {

        int from = Math.max(r2.start, start);
        int to = r2.end;

        for (int t = from; t <= to; t++) {
            Rule r;
            if (rules.containsKey(t))
                r = rules.get(t);
            else
                r = new Rule(simplifier);

            r.merge(r2.rules.get(t));
            rules.put(t, r);

            Verdict v2 = r2.verdicts.get(t);
            if (verdicts.containsKey(t))
                verdicts.put(t, Verdict.merge(verdicts.get(t), v2));
            else
                verdicts.put(t, v2);
        }
        end = Math.max(end, r2.end);
    }

    /**
     * Returns the currently determined verdict for a timestamp t
     *
     * @param t Timestamp
     * @return Verdict associated with t
     */
    public Verdict getVerdict(int t) {
        return verdicts.get(t);
    }

    /**
     * @return Returns the verdict associated with the last timestamp
     * @see Representation#getVerdict(int)
     */
    public Verdict getVerdictLast() {
        return getVerdict(end);
    }

    /**
     * @return Return the timestamp of the last known verdict
     */
    public int getLastVerdict() {
        return lastVerdict;
    }

    /**
     * Find the earliest final verdict starting from the lower interval
     *
     * @return Final Verdict
     * @see Representation#scanVerdict(int, int)
     */
    public VerdictTimed scanVerdict() {
        return scanVerdict(start, end);
    }

    /**
     * Find the earliest final verdict starting at a given timestamp
     *
     * @param start Timestamp from which to start
     * @return Final Verdict
     * @see Representation#scanVerdict(int, int)
     */
    public VerdictTimed scanVerdict(int start) {
        return scanVerdict(start, end);
    }

    /**
     * Find the earliest final verdict between start and end
     *
     * @param start Interval start
     * @param end Interval end
     * @return Earliest Final Verdict
     */
    public VerdictTimed scanVerdict(int start, int end) {
        int begin = Math.max(start, this.start);
        int to = Math.min(end, this.end);
        for (int i = begin; i <= to; i++) {
            if (Verdict.isFinal(verdicts.get(i)))
                return new VerdictTimed(verdicts.get(i), i);
        }
        return new VerdictTimed(Verdict.NA, end);
    }

    /**
     * Return all final verdicts known to the EHE
     *
     * @return A list of all final verdict, associated with their timestamp
     */
    public List<VerdictTimed> scanVerdictsAll() {
        List<VerdictTimed> list = new LinkedList<>();

        int begin = Math.max(start, this.start);
        int to = Math.min(end, this.end);
        for (int i = begin; i <= to; i++) {
            if (Verdict.isFinal(verdicts.get(i)))
                list.add(new VerdictTimed(verdicts.get(i), i));
        }
        return list;
    }

    /**
     * Return the list of rules for a timestamp
     * Rules are the [{@literal state -> expr}] bindings
     *
     * @return Rules
     */
    public Map<Integer, Rule> getRules() {
        return rules;
    }

    /**
     * Returns the list of verdicts (for each timestamp)
     *
     * @return Verdicts
     */
    public Map<Integer, Verdict> getVerdicts() {
        return verdicts;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<Integer, Rule> entry : rules.entrySet()) {
            int t = entry.getKey();
            Rule r = entry.getValue();
            String s;
            if (r.isResolved()) s = r.resolve(new MemoryAtoms()).toString();
            else s = "?";
            sb.append("[" + t + "] [" + s + "]-> " + verdicts.get(t) + " \n" + entry.getValue().toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return this.slice(start);
    }

    /**
     * Returns the size of the encoding
     * Size = Number of timestamps encoded
     * (This is the size of the interval covered by the EHE)
     *
     * @return interval size
     */
    public int size() {
        return rules.size();
    }

}
