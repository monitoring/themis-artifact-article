package uga.corse.themis.monitoring.ehe;

import uga.corse.themis.automata.State;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.expressions.bool.BoolOr;
import uga.corse.themis.expressions.bool.True;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.utils.Expressions;
import uga.corse.themis.utils.Simplifier;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * A Rule is a map that maps states with boolean expressions
 * <p>
 * It represents the partial function for each timestamp that
 * encodes an expression that checks if a state is reachable
 * </p><p>
 * get(q) = expr, if expr.eval(mem) then we are in q
 * </p>
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Rule implements Serializable {


    private static final long serialVersionUID = -1514425827043130250L;
    private Map<State, BoolExpression> map;
    private transient Simplifier simplifier = null;

    public Rule() {
        map = new HashMap<>();
    }

    public Rule(Simplifier simp) {
        this();
        this.simplifier = simp;
    }

    /**
     * And an entry to the map
     * Associate a state with an expression
     *
     * @param state The state to associate with
     * @param expr The expression
     */
    public void add(State state, BoolExpression expr) {
        map.put(state, expr);
    }

    /**
     * Return the expression associated with a state
     *
     * @param state The state whose expression is sought
     * @return The expression to reach the state
     */
    public BoolExpression get(State state) {
        if (map.containsKey(state))
            return map.get(state);
        else
            return null;
    }

    /**
     * Merge Rules
     * Two expressions that have the same state are ORed
     *
     * @param r Rule to merge with current one
     */
    public synchronized void merge(Rule r) {
        for (Entry<State, BoolExpression> e : r.getPairs().entrySet()) {
            if (map.containsKey(e.getKey()))
                map.put(e.getKey(), new BoolOr(map.get(e.getKey()), e.getValue()));
            else
                map.put(e.getKey(), e.getValue());
        }
    }

    /**
     * Did we find a reachable state?
     * i.e There exists a state q, such that get(q) == expr, and expr == True
     *
     * @return true if we found a state, false otherwise
     */
    public synchronized boolean isResolved() {
        for (Entry<State, BoolExpression> entry : map.entrySet()) {
            if (entry.getValue() instanceof True)
                return true;
        }
        return false;
    }

    /**
     * Rewrite all expressions with a memory and simplify
     * This coincides with the rw() operation followed by a simplifier call for each expression
     *
     * @param mem Memory to use to update rules
     */
    @SuppressWarnings("rawtypes")
    public synchronized boolean update(Memory mem) {
        boolean updated = false;
        for (Entry<State, BoolExpression> entry : map.entrySet()) {
            Expressions.ProcessResult res = new Expressions.ProcessResult();
            BoolExpression replace = Expressions.process(entry.getValue(), mem, res);
            updated |= (res.changed | res.containsLeaf);
            if(res.changed | res.containsLeaf) {
                if (simplifier != null) replace = simplifier.simplify(replace);
                map.put(entry.getKey(), replace);
            }
        }
        return updated;
    }

    /**
     * Eval operation
     * Check that expr.eval(mem) == true for all expressions
     * [Note: Needs to be updated first ({@link #update(Memory)})]
     * If an expr evaluates to True then cleanup and return the found state
     *
     * @param mem Memory to use for resolving
     * @return State found
     */
    @SuppressWarnings("rawtypes")
    public synchronized State resolve(Memory mem) {
        List<State> toRemove = new LinkedList<>();
        State found = null;

        for (Entry<State, BoolExpression> entry : map.entrySet()) {
            Verdict v =  entry.getValue().eval(mem);
            if (v == Verdict.NA) continue;
            if (v == Verdict.TRUE) {
                found = entry.getKey();
                break;
            } else
                toRemove.add(entry.getKey());
        }

        if (found != null) {
            Set<State> states = map.keySet();
            for (State s : states)
                if (s != found)
                    toRemove.add(s);
        }
        for (State s : toRemove)
            map.remove(s);

        return found;
    }

    /**
     * Return all state expression pairs
     *
     * @return Map of (state, expression)
     */
    public Map<State, BoolExpression> getPairs() {
        return map;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<State, BoolExpression> entry : map.entrySet()) {
            sb.append(entry.getValue().toString() + " -> " + entry.getKey());
            sb.append("\n");
        }
        return sb.toString();
    }
}
