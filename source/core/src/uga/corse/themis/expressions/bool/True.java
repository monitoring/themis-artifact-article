package uga.corse.themis.expressions.bool;

import java.util.List;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.memory.Memory;

@SuppressWarnings("serial")
public class True implements BoolSym {
    @Override
    public Boolean eval(Event e) {
        return true;
    }

    @Override
    public String toString() {
        return "true";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        return Verdict.TRUE;
    }

    @Override
    public List<Object> getOperands() {
        return null;
    }


}
