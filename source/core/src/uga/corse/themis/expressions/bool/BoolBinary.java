package uga.corse.themis.expressions.bool;

import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.expressions.ExpressionBinary;

public abstract class BoolBinary implements BoolExpression, ExpressionBinary<BoolExpression> {

    private static final long serialVersionUID = 1L;
    protected BoolExpression a, b;
    protected String operator;

    public BoolBinary(BoolExpression a, BoolExpression b) {
        this.a = a;
        this.b = b;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("(");
        sb.append(a.toString());
        sb.append(") ").append(operator).append(" (");
        sb.append(b.toString());
        sb.append(")");
        return sb.toString();
    }

    @Override
    public List<Object> getOperands() {
        List<Object> l = new LinkedList<Object>();
        l.add(a);
        l.add(b);
        return l;
    }

    @Override
    public BoolExpression getLeft() {
        return a;
    }

    @Override
    public BoolExpression getRight() {
        return b;
    }
}
