package uga.corse.themis.expressions.bool;

import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.memory.Memory;

/**
 * A Boolean symbol representing an Event
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
@SuppressWarnings("serial")
public class EventExpr implements BoolSym {
    private Event event;

    public EventExpr(Event event) {
        this.event = event;
    }

    @Override
    public Boolean eval(Event e) {
        return event.equals(e);
    }

    @Override
    public String toString() {
        return event.toString();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        return m.get(event);
    }

    public Event getEvent() {
        return event;
    }

    @Override
    public List<Object> getOperands() {
        List<Object> l = new LinkedList<Object>();
        l.add(event);
        return l;
    }
}
