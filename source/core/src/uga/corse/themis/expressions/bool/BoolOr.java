package uga.corse.themis.expressions.bool;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.memory.Memory;

public class BoolOr extends BoolBinary {
    private static final long serialVersionUID = 1L;


    public BoolOr(BoolExpression a, BoolExpression b) {
        super(a, b);
        operator = "||";
    }

    public Boolean eval(Event e) {
        return a.eval(e) || b.eval(e);
    }


    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        Verdict v1 = a.eval(m);
        Verdict v2 = b.eval(m);

        if (v1 == Verdict.TRUE)
            return Verdict.TRUE;
        if (v2 == Verdict.TRUE)
            return Verdict.TRUE;

        if ((v1 == Verdict.FALSE) && (v2 == Verdict.FALSE))
            return Verdict.FALSE;
        else
            return Verdict.NA;
    }
}
