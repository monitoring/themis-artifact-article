package uga.corse.themis.expressions.bool;


import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.expressions.ExpressionUnary;
import uga.corse.themis.monitoring.memory.Memory;

@SuppressWarnings("serial")
public class BoolNot implements BoolExpression, ExpressionUnary<BoolExpression> {
    private BoolExpression expr;

    public BoolNot(BoolExpression expr) {
        this.expr = expr;
    }

    @Override
    public Boolean eval(Event e) {
        return !expr.eval(e);
    }

    @Override
    public String toString() {
        return "!(" + expr.toString() + ")";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {

        Verdict v = expr.eval(m);
        switch (v) {
            case FALSE:
                return Verdict.TRUE;
            case NA:
                return Verdict.NA;
            case TRUE:
                return Verdict.FALSE;
            default:
                return Verdict.NA;

        }
    }

    @Override
    public List<Object> getOperands() {
        List<Object> l = new LinkedList<Object>();
        l.add(expr);
        return l;
    }

    @Override
    public BoolExpression getOperand() {
        return expr;
    }
}