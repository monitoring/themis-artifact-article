package uga.corse.themis.expressions.bool;

import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.monitoring.memory.Memory;

/**
 * A Boolean symbol that contains an Atom
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
@SuppressWarnings("serial")
public class Atomic implements BoolSym {
    private Atom atom;

    public Atomic(Atom atom) {
        this.atom = atom;
    }

    @Override
    public Boolean eval(Event e) {
        return e.observed(atom);
    }

    @Override
    public String toString() {
        return atom.toString();
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        return m.get(atom);
    }

    public Atom getAtom() {
        return atom;
    }

    @Override
    public List<Object> getOperands() {
        List<Object> l = new LinkedList<Object>();
        l.add(atom);
        return l;
    }
}
