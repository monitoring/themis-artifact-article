package uga.corse.themis.expressions.bool;

import uga.corse.themis.expressions.ExpressionSymbol;

public interface BoolSym extends BoolExpression, ExpressionSymbol {

}
