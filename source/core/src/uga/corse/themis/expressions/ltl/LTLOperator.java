package uga.corse.themis.expressions.ltl;

public enum LTLOperator {
    LTL_NEG("!", (short) 1),
    LTL_AND("&&", (short) 2),
    LTL_OR("||", (short) 2),
    LTL_IMPLIES("->", (short) 2),
    LTL_EQUIV("<->", (short) 2),
    LTL_GLOBALLY("[]", (short) 1),
    LTL_FINALLY("<>", (short) 1),
    LTL_NEXT("X", (short) 1),
    LTL_UNTIL("U", (short) 2),
    LTL_RELEASE("V", (short) 2);

    private String sym;
    private short operands;

    private LTLOperator(String sym, short operands) {
        this.sym = sym;
        this.operands = operands;
    }

    @Override
    public String toString() {
        return sym;
    }

    public short getOperands() {
        return operands;
    }

    public static LTLOperator fromString(String op) {
        return LTLOperator.valueOf(op);
    }
}
