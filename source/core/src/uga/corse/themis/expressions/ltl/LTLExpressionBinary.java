package uga.corse.themis.expressions.ltl;

import uga.corse.themis.expressions.ExpressionBinary;

public interface LTLExpressionBinary extends LTLExpression, ExpressionBinary<LTLExpression> {

}
