package uga.corse.themis.expressions.ltl;

public abstract class LTLExpressionBase implements LTLExpression {
    protected LTLExpression[] exprs;
    protected LTLOperator op;

    protected LTLExpressionBase(LTLOperator op, LTLExpression... exprs) {
        this.exprs = exprs;
        this.op = op;
    }

    protected void setExprs(LTLExpression[] exprs) {
        this.exprs = exprs;
    }

    protected LTLExpression[] getExprs() {
        return exprs;
    }

    public LTLOperator getOperator() {
        return op;
    }

}
