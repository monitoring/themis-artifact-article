package uga.corse.themis.expressions;

public interface ExpressionUnary<T extends Expression> {
    /**
     * Return subexpression
     *
     * @return subexpression
     */
    T getOperand();
}
