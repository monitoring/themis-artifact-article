package uga.corse.themis.expressions;

public interface ExpressionBinary<T extends Expression> {
    /**
     * Return left sub-expression
     *
     * @return left subexpression
     */
    T getLeft();

    /**
     * Return right sub-expression
     *
     * @return right subexpression
     */
    T getRight();
}
