package uga.corse.themis;


import java.io.Serializable;
import java.util.*;

public class Topology implements Serializable {
    Map<String, Set<String>> componentMatrix = new HashMap<>();

    public int getCountComponents() {
        return componentMatrix.size();
    }

    public boolean add(String component) {
        if(!componentMatrix.containsKey(component)) {
            componentMatrix.put(component, new HashSet<>());
            return true;
        }
        return false;
    }
    public boolean connect(String from, String to, boolean directed) {
        if(!componentMatrix.containsKey(from) || !componentMatrix.containsKey(to))
            return false;
        componentMatrix.get(from).add(to);
        if(!directed)
            componentMatrix.get(to).add(from);
        return true;
    }

    public Map<String, Set<String>> getGraph() {
        return Collections.unmodifiableMap(componentMatrix);
    }
}
