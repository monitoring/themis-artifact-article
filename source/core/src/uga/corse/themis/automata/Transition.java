package uga.corse.themis.automata;

import java.io.Serializable;

/**
 * Automaton Transition
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Transition implements Serializable {
    private State from;
    private State to;
    private Label label;

    /**
     * Create a transition from a state to another state given on a label
     *
     * @param from State from
     * @param to State to
     * @param label Transition label
     */
    public Transition(State from, State to, Label label) {
        this.from = from;
        this.to = to;
        this.label = label;
    }

    /**
     * @return State from which the transition is outbound
     */
    public State getFrom() {
        return from;
    }

    /**
     * @return State the transition is inbound (points to)
     */
    public State getTo() {
        return to;
    }

    /**
     * @return The transition label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * Is the transition possible for a given event?
     *
     * @param e Event to check
     * @return True if the transition is possible
     */
    public boolean possible(Event e) {
        return label.matches(e);
    }

    @Override
    public String toString() {
        return from + " -> " + to;
    }
}
