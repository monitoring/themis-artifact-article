package uga.corse.themis.automata;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Automata data struct
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Automata implements Serializable {


    private static final long serialVersionUID = 6369983612970738809L;
    //Current state
    private State cur = null;
    //Initial State
    private State init = null;
    //Labelling function
    private Map<State, Verdict> states = null;
    //Delta
    private Map<State, List<Transition>> delta = null;

    /**
     * Create a new Moore automaton
     *
     * @param init   Initial state
     * @param states Mapping from states to verdict
     * @param delta  Specify the transition function
     */
    public Automata(State init, Map<State, Verdict> states, Map<State, List<Transition>> delta) {
        this.init = init;
        this.states = states;
        this.delta = delta;
        cur = this.init;
    }

    /**
     * Go back to initial state
     */
    public void reset() {
        cur = init;
    }

    /**
     * @return Return the current state the automaton is in
     */
    public State getCurrentState() {
        return cur;
    }

    /**
     * @return Return the initial State
     */
    public State getInitialState() {
        return init;
    }

    /**
     * @return Return all states and their verdicts
     */
    public Map<State, Verdict> getStates() {
        return states;
    }

    /**
     * @return Return the transition function
     */
    public Map<State, List<Transition>> getDelta() {
        return delta;
    }

    /**
     * Move on an event
     * An empty event is associated with an event containing an empty atom
     *
     * @param e The input event
     * @return Returns the state reached or null if sink
     */
    public State move(Event e) {
        //Normalize:
        // an AtomString.empty() labels a transition on empty events
        Event e_normal = e;
        if (e.empty()) {
            e_normal = new Event();
            e_normal.addObservation(Atom.empty());
        }
        for (Transition t : delta.get(cur)) {
            if (t.possible(e_normal)) {
                cur = t.getTo();
                return cur;
            }
        }
        return null;
    }

    /**
     * Returns verdict associated with a state
     *
     * @param s The state
     * @return The label of the state (typically a Verdict)
     * @throws InvalidStateException If the state does not belong in the automaton
     */
    public Verdict getVerdict(State s) throws InvalidStateException {
        if (states.containsKey(s))
            return states.get(s);
        else {
            throw new InvalidStateException();
        }
    }

    /**
     * Returns current verdict

     * @return Returns the verdict on the current state of the automaton
     */
    public Verdict getVerdict() {
        return states.get(cur);
    }

}
