package uga.corse.themis.automata;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * The Event consists of a set of atoms
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Event implements Serializable {
    private Set<Atom> atoms;

    /**
     * Initialize an event
     */
    public Event() {
        atoms = new HashSet<>();
    }

    /**
     * Add an observation to the event
     *
     * @param ap Atom to add
     */
    public void addObservation(Atom ap) {
        atoms.add(ap);
    }

    /**
     * Is an atom part of the event?
     *
     * @param a Atom to check
     * @return true if a in event (set contain)
     */
    public boolean observed(Atom a) {
        return atoms.contains(a);
    }

    /**
     * Return all atoms observed
     * @return  A set of observed atoms
     */
    public Set<Atom> getAtoms() {
        return atoms;
    }

    /**
     * Check whether it contains an empty atom
     * See: {@link Atom#isEmpty()}
     * @return true if the event contains an empty atom
     */
    public boolean hasEmpty() {
        for (Atom atom : atoms)
            if (atom.isEmpty())
                return true;
        return false;
    }

    /**
     * Event has no observations?
     * Note that a set containing empty atoms is not empty
     * @return True if event is the empty set
     */
    public boolean empty() {
        return atoms.isEmpty();
    }

    @Override
    public String toString() {
        if (empty()) return "[]";
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Atom> iter = atoms.iterator();
        sb.append(iter.next());
        while (iter.hasNext()) {
            sb.append(" ");
            sb.append(iter.next().toString());
        }
        sb.append("]");

        return sb.toString();
    }

    /**
     * Add all observations from another event
     *
     * @param ev Event from which to add observations
     */
    public void merge(Event ev) {
        atoms.addAll(ev.atoms);
    }

    /**
     * Is it equal to another event
     * Set equality of observations
     *
     * @param obj Event to compare again
     * @return True if equal (Set equality) ({@link Set#equals(Object)})
     */
    public boolean equals(Event obj) {
        return atoms.equals(obj.atoms);
    }
}
