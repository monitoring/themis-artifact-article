package uga.corse.themis.automata;

/**
 * Wrong state in an automaton
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
@SuppressWarnings("serial")
public class InvalidStateException extends Exception {
}
