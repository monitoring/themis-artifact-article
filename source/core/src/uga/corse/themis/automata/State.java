package uga.corse.themis.automata;

import java.io.Serializable;

/**
 * Automaton State
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
@SuppressWarnings("serial")
public class State implements Serializable, Comparable<State> {


    private static final long serialVersionUID = -9194385407646019401L;
    private String name;

    /**
     * Create an automaton state
     *
     * @param name Name of the state
     */
    public State(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    @Override
    public int compareTo(State state) {
        return this.name.compareTo(state.name);
    }
}
