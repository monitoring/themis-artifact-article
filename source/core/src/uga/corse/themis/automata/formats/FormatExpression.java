package uga.corse.themis.automata.formats;

import uga.corse.themis.automata.Label;
import uga.corse.themis.utils.Expressions;

/**
 * Formats the text as a boolean Expression
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class FormatExpression implements TransitionFormatter {

    @Override
    public Label getLabel(String text) {
        return new LabelExpression(Expressions.parseBoolean(text));
    }

}
