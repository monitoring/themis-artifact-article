package uga.corse.themis.automata.formats;

import uga.corse.themis.automata.Label;
import uga.corse.themis.utils.Expressions;

/**
 * Formats the transition text as Events
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class FormatEvent implements TransitionFormatter {

    @Override
    public Label getLabel(String text) {
        return new LabelEvent(Expressions.parseEvent(text));
    }

}
