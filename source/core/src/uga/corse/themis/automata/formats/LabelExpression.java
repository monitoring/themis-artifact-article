package uga.corse.themis.automata.formats;

import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.automata.Event;
import uga.corse.themis.automata.Label;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.expressions.bool.BoolExpression;
import uga.corse.themis.monitoring.memory.Memory;
import uga.corse.themis.utils.Expressions;

/**
 * Transition Label: Boolean Expression
 * To check if a transition is possible, it evaluates a boolean expression
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
@SuppressWarnings("serial")
public class LabelExpression implements Label, BoolExpression {

    private BoolExpression label;

    /**
     * Initialize a transition label with a boolean expression
     *
     * @param label Boolean Expression
     */
    public LabelExpression(BoolExpression label) {
        this.label = label;
    }

    @Override
    public boolean matches(Event e) {
        return label.eval(e);
    }

    @Override
    public String toString() {

        return Expressions.formatShort(label).toString();
    }

    @Override
    public Boolean eval(Event e) {
        return label.eval(e);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Verdict eval(Memory m) {
        return label.eval(m);
    }

    @Override
    public List<Object> getOperands() {
        List<Object> l = new LinkedList<>();
        l.add(label);
        return l;
    }


}
