package uga.corse.themis.automata.formats;

import uga.corse.themis.automata.Label;

/**
 * Parses a text to generate a transition Label
 * Used when parsing automata
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public interface TransitionFormatter {
    Label getLabel(String text);
}
