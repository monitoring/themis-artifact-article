package uga.corse.themis.automata;

import java.io.Serializable;

/**
 * A pair of Verdict and Timestamp
 * To denote a verdict for (or at) a given timestamp
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class VerdictTimed implements Serializable {
    private Verdict verdict;
    private int timestamp;


    /**
     * Initialize a timed verdict with a verdict and a timestamp
     *
     * @param verdict Verdict found
     * @param timestamp Timestamp
     */
    public VerdictTimed(Verdict verdict, int timestamp) {
        this.verdict = verdict;
        this.timestamp = timestamp;
    }

    /**
     * Returns the verdict
     *
     * @return Verdict
     */
    public Verdict getVerdict() {
        return verdict;
    }

    /**
     * Change the verdict
     *
     * @param verdict New verdict
     */
    public void setVerdict(Verdict verdict) {
        this.verdict = verdict;
    }

    /**
     * Return the timestamp
     *
     * @return timestamp
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * Change Timestamp
     *
     * @param timestamp Set the current timestamp of the verdict to this
     */
    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Check if the verdict is final
     *
     * See: {@link Verdict#isFinal()}
     *
     * @return true if verdict is final
     */
    public boolean isFinal() {
        return Verdict.isFinal(verdict);
    }

    @Override
    public String toString() {
        return verdict + "@" + timestamp;
    }
}
