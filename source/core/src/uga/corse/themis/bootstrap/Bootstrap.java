package uga.corse.themis.bootstrap;


import uga.corse.themis.Topology;
import uga.corse.themis.monitoring.Monitor;

import java.util.Set;

public interface Bootstrap  {
    Set<? extends Monitor> getMonitors(Topology topology);
}
