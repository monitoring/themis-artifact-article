package uga.corse.themis.runtime.observers;


import uga.corse.themis.Platform;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.messages.MsgAbort;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.runtime.Node;
import uga.corse.themis.runtime.NodeObserver;

import java.io.IOException;


public class DispatcherEnd implements NodeObserver
{

    Dispatcher disp;
    String toNotify;

    public DispatcherEnd(Dispatcher disp, String address) {
        this.disp = disp;
        this.toNotify = address;
    }

    @Override
    public void onVerdict(Node node, int monid, Verdict verdict) {

    }

    @Override
    public void onAbort(Node node, int monid) {

    }

    @Override
    public void onAddComponent(Node node, Component comp) {

    }

    @Override
    public void onAttachMonitor(Node node, String comp, Monitor mon) {

    }

    @Override
    public void onSetDispatcher(Node node, Dispatcher disp) {

    }

    @Override
    public void onSetNetwork(Node node, Network net) {

    }

    @Override
    public void onChangePlatform(Node node, Platform plat) {

    }

    @Override
    public void onStart(Node node) {

    }

    @Override
    public void onStop(Node node) {
        try {
            disp.cmdSync(toNotify, new MsgAbort());
        } catch (IOException e) {

        }
        node.observerRemove(this.hashCode());
    }

}
