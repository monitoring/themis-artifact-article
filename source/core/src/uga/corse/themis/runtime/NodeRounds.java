package uga.corse.themis.runtime;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Platform;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.comm.protocol.NetworkDistributed;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.MonitorResult;
import uga.corse.themis.tasks.ObserveRound;
import uga.corse.themis.comm.messages.MsgAbort;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.*;

public class NodeRounds implements Node {
    private static Logger log = LogManager.getLogger(NodeRounds.class);

    ExecutorService scheduler                   = Runtime.getScheduler();
    CompletionService<MonitorResult> monitoring = new ExecutorCompletionService<>(scheduler);
    Map<Integer, NodeObserver> observers = new HashMap<>();


    private Platform platform  = null;
    private Network net        = null;
    private String myAddr      = "";

    private Map<String, Component> managedComps    = new HashMap<>();
    private Map<Integer, Monitor> managedMons      = new HashMap<>();
    private Map<Integer, String>  monMap           = new HashMap<>();
    private Map<String, Set<Monitor>> compMap      = new HashMap<>();
    private volatile Boolean isStarted             = false;
    private int round                              = 0;
    Dispatcher disp                                = null;

    Integer maxRounds                              = -1;

    private volatile boolean isAborting = false;


    public NodeRounds(String addr, Dispatcher disp) {
        setAddress(addr);
        setDispatcher(disp);
        reload();
    }

    @Override
    public void setAddress(String addr) {
        this.myAddr = addr;
    }

    @Override
    public String getAddress() {
        return this.myAddr;
    }

    public synchronized void addComponent(Component comp) {
        String cid = comp.getName();
        managedComps.put(cid, comp);
        compMap.put(cid, new HashSet<>());
        for(NodeObserver obs : observers.values()) obs.onAddComponent(this, comp);
    }
    public synchronized boolean attachMonitor(String comp, Monitor mon) {
        if(!managedComps.containsKey(comp)) return false;

        int mid = mon.getID();

        managedMons.put(mid, mon);
        monMap.put(mid, comp);
        compMap.get(comp).add(mon);
        mon.setComponentKey(comp);
        mon.setup();
        for(NodeObserver obs : observers.values()) obs.onAttachMonitor(this, comp, mon);
        return true;
    }

    public synchronized  void stop() {
        if(isStarted) {
            isStarted = false;
        }
    }
    public synchronized void reset() {
        if(isStarted)
            doStop();
        else
            reload();
    }
    private synchronized void reload() {
        Runtime.reload();
        scheduler  = Runtime.getScheduler();
        monitoring = new ExecutorCompletionService<>(scheduler);

        managedComps.clear();
        managedMons.clear();
        monMap.clear();
        compMap.clear();

        net        = null;

        log.info("Node Reloaded");
    }
    private synchronized void doStop() {
        log.info("Stopped");
        isStarted = false;
        round     = 0;

        try {
            if (net != null)
                net.reset();
            for (Monitor mon : managedMons.values()) {
                mon.cleanup();
            }
            for (Component comp : managedComps.values()) {
                comp.shutdown();
            }
        }
        catch(Exception e) {
            Runtime.report(e);
        }
        scheduler.shutdownNow();


        for(NodeObserver obs : observers.values()) obs.onStop(this);

        reload();

    }


    public Platform getPlatform() {
        return platform;
    }

    public synchronized boolean changePlatform(Platform platform) {
        this.platform = platform;
        this.net      = new NetworkDistributed(myAddr, disp, platform);
        for(Monitor mon : managedMons.values())
            mon.setNetwork(net);

        maxRounds = (Integer) platform.getData();
        for(NodeObserver obs : observers.values()) obs.onChangePlatform(this, platform);
        return true;
    }

    public void notifyVerdict(int monid, Verdict verdict) {
        log.info("Verdict found by " + monid + " " + verdict);
        for(NodeObserver obs : observers.values()) obs.onVerdict(this, monid, verdict);

    }
    public void notifyAbort(int monid) {
        log.info("Abort Issued by " + monid);
        for(NodeObserver obs : observers.values()) obs.onAbort(this, monid);
    }

    @Override
    public void setDispatcher(Dispatcher disp) {
        this.disp = disp;
        for(NodeObserver obs : observers.values()) obs.onSetDispatcher(this, disp);
    }

    @Override
    public Dispatcher getDispatcher() {
        return this.disp;
    }

    @Override
    public int observerRegister(NodeObserver obs) {
        observers.put(obs.hashCode(), obs);
        log.trace("Added Observer: " + obs);
        return obs.hashCode();
    }

    @Override
    public void observerRemove(int id) {
        log.trace("Removed Observer: " + id);
        observers.remove(id);
    }

    public synchronized boolean isStarted() { return isStarted; }

    @Override
    public Integer getMonitorCount() {
        return managedMons.size();
    }

    @Override
    public Integer getComponentCount() {
        return managedComps.size();
    }

    @Override
    public Component getComponentForMonitor(Integer monid) {
        Monitor mon = managedMons.get(monid);
        if(mon == null) return null;
        return managedComps.get(mon.getComponentKey());
    }

    @Override
    public Set<Monitor> getMonitorsForComponent(String componentKey) {
        return compMap.get(componentKey);
    }


    @Override
    public  Network getNetwork() {
        return net;

    }

    @Override
    public  void setNetwork(Network net) {
        this.net = net;
        for(NodeObserver obs : observers.values()) obs.onSetNetwork(this, net);
    }

    @Override
    public Map<String, Component> getManagedComps() {
        return managedComps;
    }

    @Override
    public Map<Integer, Monitor> getManagedMonitors() {
        return managedMons;
    }

    @Override
    public void start(Map<String, Serializable> tags) {

        log.info("Monitoring Started");
        synchronized (this){
            isStarted = true;
            round = 0;
        }
        for(NodeObserver obs : observers.values()) obs.onStart(this);
        boolean interrupted = false;
        while(true) {
            synchronized(isStarted) { if(!isStarted) break; }
            boolean stop = false;

            synchronized(this) {
                if (round >= maxRounds)
                    stop = true;
            }
            if(stop) {
                log.warn("Timed Out");
                break;
            }

            final List<Future<MonitorResult>> tasks = new LinkedList<>();
            synchronized(this) {
                round++;
                log.trace("Monitoring round: " + round);
                for(Component comp : managedComps.values()) {
                    Callable<MonitorResult> task = new ObserveRound(this, round, comp,
                        Collections.unmodifiableSet(compMap.get(comp.getName())));
                    tasks.add(monitoring.submit(task));
                }
            }
            for(int i = 0; i < tasks.size(); i++) {
                try {
                    MonitorResult res = monitoring.take().get();
                    if(res.isAbort()) {
                        synchronized(this) {
                            interrupted = true;
                            log.info("Aborting Monitoring");
                            for (Future<MonitorResult> t : tasks)
                                t.cancel(true);
                            if(res == MonitorResult.ABORT_RUN) {
                                try {
                                    boolean abort = disp.broadcast(platform.getComponents().values(),
                                            this.myAddr, new MsgAbort());
                                    if (abort) {
                                        log.info("Abort Signal sent to all nodes");
                                    } else
                                        log.warn("Abort Signal did not reach all nodes");
                                }catch(IOException exception) {
                                    log.warn("Abort Signal did not reach all nodes", exception);
                                }
                            }
                        }
                        stop();
                        break;
                    }
                } catch (CancellationException cancelled) {
                    //Ignore
                } catch (ExecutionException exec) {
                    Runtime.report(exec);
                } catch (InterruptedException interrupt) {
                    for (Future<MonitorResult> t : tasks) {
                        t.cancel(true);
                    }
                    Thread.currentThread().interrupt();
                }

            }
/*
            try {
                Thread.sleep(600);
            } catch (InterruptedException e) {
                break;
            } */
        }
        if(!interrupted)
            stop();

        doStop();
    }


}
