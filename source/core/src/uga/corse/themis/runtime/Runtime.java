package uga.corse.themis.runtime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.comm.commands.CmdRuntime;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.dispatcher.Sockets;
import uga.corse.themis.comm.messages.MsgResult;
import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.utils.Opts;
import uga.corse.themis.utils.Server;
import uga.corse.themis.utils.CmdLoad;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runtime {
    private static Logger log = LogManager.getLogger(Runtime.class.getName());

    static int THCNT = Integer.parseInt(Opts.getGlobal("THEMIS_THREADS", "-1"));
    static ExecutorService executor = (THCNT >= 0)
            ? Executors.newFixedThreadPool(THCNT)
            : Executors.newCachedThreadPool();
    //static ExecutorService executor =  Executors.newCachedThreadPool();

    public static void reload() {
        executor.shutdown();
        //executor = Executors.newCachedThreadPool();
        executor = (THCNT >= 0)
                ? Executors.newFixedThreadPool(THCNT)
                : Executors.newCachedThreadPool();
    }


    public static ExecutorService getScheduler() {
        return executor;
    }

    public static void info(Object e) {
        if(e instanceof Throwable) {
            if(e instanceof ExecutionException)
                report(((ExecutionException) e).getCause());
            else
                log.info(((Throwable) e).getLocalizedMessage(), e);
        }
        else {
            log.info(e);
        }
    }
    public static void report(Object e) {
        if(e == null) {
            log.error("Received Null Error > Tracing", new Exception());
        }
        else if(e instanceof Throwable) {
            if(e instanceof ExecutionException)
                report(((ExecutionException) e).getCause());
            else
                log.error(((Throwable) e).getLocalizedMessage(), e);
        }
        else {
            log.error(e);
        }

    }

    public static void main(String[] args) throws Exception {
        final Dispatcher disp = new Sockets();

        final Server s      = new Server(args[0], Integer.parseInt(args[1]));
        final NodeRounds node   = new NodeRounds(args[0] + ":" + args[1], disp);

        s.start(new Server.ClientHandler() {
            @Override
            public void handleClient(AsynchronousSocketChannel client, ObjectInputStream in, ObjectOutputStream out) {
                try {

                    MsgRuntime msg = (MsgRuntime) in.readObject();

                    if(msg == null)
                        throw new IOException("Message was not readable");

                    // Read Client Message Object
                    log.info("Received " + msg.getClass().getSimpleName()
                            +": " + msg.toString());

                    CmdRuntime cmd = CmdLoad.load(msg);

                    if(cmd == null)
                        throw new IOException("Command was not valid");

                    // Execute Command
                    boolean result = cmd.handle(msg, node, client, in, out);

                    // Send Result to Client
                    out.writeObject(new MsgResult(result));
                    out.flush();
                    out.close();

                }
                catch(Exception | Error  e) {
                    // Report an error
                    report(e);
                    //Attempt to notify Sender
                    try {
                        MsgResult res = new MsgResult(false);
                        res.setData(e);
                        out.writeObject(res);
                        out.flush();
                        out.close();
                    }
                    catch(Exception e2) {
                        // No point worrying anymore
                    }
                }
            }
        });
        while(s.isRunning()) {
            Thread.sleep(250);
        }

    }
}
