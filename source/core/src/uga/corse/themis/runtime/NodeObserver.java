package uga.corse.themis.runtime;


import uga.corse.themis.Platform;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.protocol.Network;
import uga.corse.themis.monitoring.Component;
import uga.corse.themis.monitoring.Monitor;


public interface NodeObserver {
    void onVerdict(Node node, int monid, Verdict verdict);
    void onAbort(Node node, int monid);
    void onAddComponent(Node node, Component comp);
    void onAttachMonitor(Node node, String comp, Monitor mon);
    void onSetDispatcher(Node node, Dispatcher disp);
    void onSetNetwork(Node node, Network net);
    void onChangePlatform(Node node, Platform plat);
    void onStart(Node node);
    void onStop(Node node);
}
