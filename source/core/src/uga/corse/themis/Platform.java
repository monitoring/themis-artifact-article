package uga.corse.themis;


import uga.corse.themis.encoders.Encoder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Platform implements Serializable {


    private static final long serialVersionUID = 7662009598107521114L;
    private Map<String, String> components = new HashMap<>();
    private Map<Integer, String> monitors  = new HashMap<>();
    private Serializable data;
    private Encoder encoder;

    public Platform() {
    }

    public void setAddressMonitor(Integer id,  String addr){
        monitors.put(id, addr);
    }
    public void setAddressComponent(String id,  String addr){
        components.put(id, addr);
    }

    public String getAddressMonitor(Integer id) {
        return monitors.get(id);
    }
    public String getAddressComponent(String id) {
        return components.get(id);
    }

    public int getCountComponents() { return components.size(); }
    public int getCountMonitors()   { return monitors.size();   }

    public Map<String, String> getComponents() {
        return components;
    }

    public void setComponents(Map<String, String> components) {
        this.components = components;
    }

    public Map<Integer, String> getMonitors() {
        return monitors;
    }

    public void setMonitors(Map<Integer, String> monitors) {
        this.monitors = monitors;
    }

    public Serializable getData() {
        return data;
    }

    public void setData(Serializable data) {
        this.data = data;
    }

    public Encoder getEncoder() {
        return encoder;
    }

    public void setEncoder(Encoder encoder) {
        this.encoder = encoder;
    }

}
