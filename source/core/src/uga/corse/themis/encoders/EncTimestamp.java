package uga.corse.themis.encoders;


import uga.corse.themis.automata.Atom;
import uga.corse.themis.symbols.AtomNegation;
import uga.corse.themis.symbols.AtomObligation;

public class EncTimestamp implements Encoder {
    private int t;
    public EncTimestamp(int timestamp) {
        t = timestamp;
    }

    @Override
    public Atom encode(Atom atom) {
        if(atom instanceof AtomNegation) {
            atom = ((AtomNegation) atom).getNegated();
            return new AtomNegation(new AtomObligation(atom, t));
        }
        else
            return new AtomObligation(atom, t);
    }
}
