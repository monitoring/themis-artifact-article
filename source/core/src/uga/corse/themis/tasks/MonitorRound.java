package uga.corse.themis.tasks;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.monitoring.memory.MemoryRO;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.MonitorResult;

import java.util.concurrent.Callable;

public class MonitorRound implements Callable<MonitorResult> {
    private Monitor m;
    private MemoryRO<Atom> mem;
    private int round;

    public MonitorRound(Monitor m, MemoryRO<Atom> mem, int round) {
        this.m = m;
        this.mem = mem;
        this.round = round;
    }

    @Override
    public MonitorResult call() throws Exception {
        if(m instanceof uga.corse.themis.monitoring.MonitorRound)
            ((uga.corse.themis.monitoring.MonitorRound) m).notifyRound(round);
        return m.monitor(mem);
    }
}
