package uga.corse.themis.parser;

import uga.corse.themis.automata.Atom;
import uga.corse.themis.automata.Event;
import uga.corse.themis.parser.backend.LabelsParser.AtomContext;
import uga.corse.themis.parser.backend.LabelsParser.EV_PARContext;
import uga.corse.themis.parser.backend.LabelsParser.E_ATOMContext;
import uga.corse.themis.parser.backend.LabelsParser.E_COMPOSITEContext;
import uga.corse.themis.parser.backend.LabelsParser.E_PARENContext;
import uga.corse.themis.parser.backend.LabelsParser.E_SINGLEContext;
import uga.corse.themis.parser.backend.LabelsParser.L_EMPTYContext;
import uga.corse.themis.parser.backend.LabelsParser.L_eventContext;
import uga.corse.themis.parser.backend.LabelsParser.ObligationContext;
import uga.corse.themis.parser.backend.LabelsBaseVisitor;
import uga.corse.themis.symbols.AtomObligation;
import uga.corse.themis.symbols.AtomString;


/**
 * Generates an event from parsing a Label from LTL2MON dot
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class VisitorLabelEvent extends LabelsBaseVisitor<Object> {
    private Event ev;

    @Override
    public Object visitL_EMPTY(L_EMPTYContext ctx) {
        Event event = new Event();
        event.addObservation(Atom.empty());
        return event;
    }

    @Override
    public Object visitL_event(L_eventContext ctx) {
        ev = new Event();
        visit(ctx.event());
        return ev;
    }

    @Override
    public Object visitEV_PAR(EV_PARContext ctx) {
        return visit(ctx.event());
    }

    @Override
    public Object visitE_SINGLE(E_SINGLEContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Object visitE_COMPOSITE(E_COMPOSITEContext ctx) {
        visit(ctx.event());
        visit(ctx.expr());
        return null;
    }

    @Override
    public Object visitE_PAREN(E_PARENContext ctx) {
        return visit(ctx.expr());
    }

    @Override
    public Object visitE_ATOM(E_ATOMContext ctx) {
        return visit(ctx.atom());
    }

    @Override
    public Object visitAtom(AtomContext ctx) {
        if (ctx.obligation() != null)
            visit(ctx.obligation());
        else
            ev.addObservation(new AtomString(ctx.STRING().getText()));
        return null;
    }

    @Override
    public Object visitObligation(ObligationContext ctx) {
        ev.addObservation(new AtomObligation(
                new AtomString(ctx.STRING().getText()),
                Integer.parseInt(ctx.INT().getText()))
        );
        return null;
    }
}
