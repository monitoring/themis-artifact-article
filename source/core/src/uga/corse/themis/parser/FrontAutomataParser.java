package uga.corse.themis.parser;


import java.io.BufferedReader;
import java.io.IOException;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.formats.TransitionFormatter;
import uga.corse.themis.parser.backend.DOTLexer;
import uga.corse.themis.parser.backend.DOTParser;
import uga.corse.themis.parser.backend.DOTVisitor;

/**
 * The front controller for the Automata parsers
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class FrontAutomataParser {
    public static DOTParser getParserDot(String filename) throws IOException {
        ANTLRInputStream input = new ANTLRFileStream(filename);
        DOTLexer lexer = new DOTLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DOTParser parser = new DOTParser(tokens);
        FrontExpressionParsers.addParserError(parser);
        return parser;
    }

    public static DOTParser getParserDotStream(CharStream input) {
        DOTLexer lexer = new DOTLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        DOTParser parser = new DOTParser(tokens);
        FrontExpressionParsers.addParserError(parser);
        return parser;
    }

    /**
     * Load an automata in a filename
     * Uses dot-files generated from LTL2MON
     *
     * @param filename Path to file containing .dot graph of the automata
     * @param format Choose the formatter for the transition labels
     * @return The generated Automata
     * @throws IOException If parsing fails
     */
    public static Automata load(String filename, TransitionFormatter format) throws IOException {
        DOTParser parser = FrontAutomataParser.getParserDot(filename);
        DOTVisitor<Object> visitor = new VisitorDOTAutomata(format);

        return (Automata) visitor.visit(parser.graph());
    }

    /**
     * Load an automata from a string
     * Uses dot-files generated from LTL2MON
     *
     * @param expr String containing .dot graph of the automata
     * @param format Choose the formatter for the transition labels
     * @return An Automata
     * @throws IOException parsing fails
     */
    public static Automata loadString(String expr, TransitionFormatter format) throws IOException {
        ANTLRInputStream input = new ANTLRInputStream(expr);
        DOTParser parser = FrontAutomataParser.getParserDotStream(input);
        DOTVisitor<Object> visitor = new VisitorDOTAutomata(format);

        return (Automata) visitor.visit(parser.graph());
    }

    public static Automata fromStream(BufferedReader stream, TransitionFormatter format) throws IOException {
        CharStream cs = new ANTLRInputStream(stream);
        DOTParser parser = getParserDotStream(cs);
        DOTVisitor<Object> visitor = new VisitorDOTAutomata(format);

        return (Automata) visitor.visit(parser.graph());
    }

}
