package uga.corse.themis.parser;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import uga.corse.themis.automata.Automata;
import uga.corse.themis.automata.Label;
import uga.corse.themis.automata.State;
import uga.corse.themis.automata.Transition;
import uga.corse.themis.automata.Verdict;
import uga.corse.themis.automata.formats.TransitionFormatter;
import uga.corse.themis.parser.backend.DOTParser.Edge_stmtContext;
import uga.corse.themis.parser.backend.DOTParser.GraphContext;
import uga.corse.themis.parser.backend.DOTParser.Node_idContext;
import uga.corse.themis.parser.backend.DOTParser.Node_stmtContext;
import uga.corse.themis.parser.backend.DOTParser.StmtContext;
import uga.corse.themis.parser.backend.DOTBaseVisitor;


/**
 * Visitor to generate an Automata from a DOT
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class VisitorDOTAutomata extends DOTBaseVisitor<Object> {

    Map<String, State> states = null;
    Map<State, Verdict> verdicts = null;
    Map<State, List<Transition>> delta = null;
    TransitionFormatter format;

    public VisitorDOTAutomata(TransitionFormatter format) {
        this.format = format;
    }

    @Override
    public Automata visitGraph(GraphContext ctx) {
        states = new HashMap<String, State>();
        verdicts = new HashMap<State, Verdict>();
        delta = new HashMap<State, List<Transition>>();


        for (StmtContext stmt : ctx.stmt_list().stmt()) {

            if (stmt.edge_stmt() != null) {
                Transition t = (Transition) visit(stmt.edge_stmt());
                delta.get(t.getFrom()).add(t);
            } else if (stmt.node_stmt() != null) {
                visit(stmt.node_stmt());
            }
        }

        return new Automata(states.get("(0, 0)"), verdicts, delta);
    }

    @Override
    public State visitNode_id(Node_idContext ctx) {
        String name = trim(ctx.id().getText());
        if (!states.containsKey(name)) {
            State s = new State(name);
            states.put(name, s);
            delta.put(s, new LinkedList<Transition>());
        }
        return states.get(name);
    }

    @Override
    public Object visitEdge_stmt(Edge_stmtContext ctx) {
        State from = (State) visit(ctx.node_id());
        State to = (State) visit(ctx.edgeRHS().node_id(0));
        String txt = trim(ctx.attr_list().a_list(0).id(1).getText());
        Label lbl = format.getLabel(txt);

        return new Transition(from, to, lbl);
    }

    @Override
    public Object visitNode_stmt(Node_stmtContext ctx) {
        State s = (State) visit(ctx.node_id());
        String v = ctx.attr_list().a_list(0).id(5).getText();

        Verdict ver = null;
        if (v.equals("yellow")) ver = Verdict.NA;
        else if (v.equals("red")) ver = Verdict.FALSE;
        else if (v.equals("green")) ver = Verdict.TRUE;

        verdicts.put(s, ver);
        return null;
    }

    private String trim(String t) {
        return t.substring(1, t.length() - 1);
    }
}
