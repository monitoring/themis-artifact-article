package uga.corse.themis.tools.experiment;

import uga.corse.themis.algorithms.choreography.Choreography;
import uga.corse.themis.algorithms.choreography.MonitorNetwork;
import uga.corse.themis.algorithms.choreography.Score;
import uga.corse.themis.algorithms.choreography.MonitorNetwork.NetNode;
import uga.corse.themis.expressions.ltl.LTLExpression;
import uga.corse.themis.monitoring.specifications.SpecLTL;
import uga.corse.themis.utils.Convert;
import uga.corse.themis.utils.mapper.DefaultSymbolMapper;

/**
 * Simple command line tool to check if an LTL formula can be used
 * Mostly for our benchmarks, it tries to see if LTL2mon runs properly
 * And that the formula can be decomposed
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Verify {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Please specify an ltl formula");
            System.exit(1);
        }
        System.exit(verify(args[0]) ? 0 : 1);
    }

    public static boolean verify(String formula) {
        SpecLTL ltlspec = new SpecLTL(formula);
        return verify(ltlspec);
    }

    public static boolean verify(SpecLTL ltlspec) {
        try {

            LTLExpression spec = ltlspec.getLTL();

            Convert.makeAutomataSpec(ltlspec);

            Score data = Score.getScoreTree(spec, new DefaultSymbolMapper());

            Choreography.resetId(0);
            //Split the LTL into different monitors
            MonitorNetwork net = Choreography.splitFormula(Choreography.newId(), data.comp, data, spec, false);
            Choreography.compactNetwork(net);
            //Compute Respawn for each monitor
            Choreography.respawn(net, net.root.spec, false);

            for (NetNode node : net.nodes.values()) {
                Convert.makeAutomataSpec(new SpecLTL(node.spec));
            }
            return true;

        } catch (Exception | Error e) {
            System.err.println("Failed: " + e.getLocalizedMessage());
            return false;
        }
    }
}
