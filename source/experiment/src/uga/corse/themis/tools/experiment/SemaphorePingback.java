package uga.corse.themis.tools.experiment;

import org.apache.logging.log4j.Logger;
import uga.corse.themis.comm.messages.MsgAbort;
import uga.corse.themis.comm.messages.MsgRuntime;
import uga.corse.themis.utils.Server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Semaphore;


public class SemaphorePingback implements Server.ClientHandler {


    private Semaphore lock;
    private Logger log;

    public SemaphorePingback(Semaphore lock, Logger logger) {
        this.lock = lock;
        this.log = logger;
    }


    @Override
    public void handleClient(AsynchronousSocketChannel client, ObjectInputStream in, ObjectOutputStream out) {
        MsgRuntime msg = null;
        try {
            msg = (MsgRuntime) in.readObject();

            out.flush();
            out.close();

        } catch (AsynchronousCloseException close) {
            if(log != null) {
                log.error("Crash possible!");
                log.error(close.getMessage(), close);
            }

        } catch (Exception | Error e) {
            if(log != null) {
                log.error(e.getMessage(), e);
            }

            try {
                out.flush();
                out.close();
            } catch (Exception e2) {

            }
        }
        if (msg == null)
            throw new IllegalArgumentException("Wrong Message");

        if (msg instanceof MsgAbort) {
            lock.release();
        }

        try {
            client.close();
        }
        catch(IOException e) {}
    }
}
