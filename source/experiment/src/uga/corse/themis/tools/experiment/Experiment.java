package uga.corse.themis.tools.experiment;

import java.io.*;
import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uga.corse.themis.Platform;
import uga.corse.themis.Topology;
import uga.corse.themis.bootstrap.StandardMonitoring;
import uga.corse.themis.comm.dispatcher.Dispatcher;
import uga.corse.themis.comm.dispatcher.Sockets;
import uga.corse.themis.comm.messages.MsgCreateComponent;
import uga.corse.themis.monitoring.Monitor;
import uga.corse.themis.monitoring.components.AsyncObservations;
import uga.corse.themis.monitoring.specifications.Specification;
import uga.corse.themis.runtime.Runtime;
import uga.corse.themis.utils.Server;
import uga.corse.themis.utils.SpecLoader;

/**
 * Command-Line tool to run an experiment
 *
 * @author Antoine El-Hokayem {@literal antoine.el-hokayem@imag.fr}
 */
public class Experiment {
    public static String PATH_CONFIG = "setup.properties";
    private static String NODE_TARGET = "localhost:8056";

    private static Logger log = LogManager.getLogger(Experiment.class);

    @SuppressWarnings("serial")
    public static class Config extends Properties {
        public String getProperty(ConfigExperiment key) {
            return getProperty(key.getValue());
        }

        public void merge() {
            System.getProperties().putAll(this);
        }
    }


    public static void main(String[] args) throws Exception {
        // Basic Diagnostics


        if (args.length <= 0) abort("Experiment path not provided");

        File root = new File(args[0]);
        if (!root.exists()) abort("Experiment path does not exist: " + args[0]);
        if(args.length >= 2)
            PATH_CONFIG = args[1];


        notify("Running experiment: " + root.getAbsolutePath());

        File cfile = new File(root, PATH_CONFIG);
        FileReader cread = new FileReader(cfile);

        Config config = new Config();
        config.load(cread);
        cread.close();


        notify("Using configuration: " + cfile.getAbsolutePath());
        if (!verifyConfig(config)) abort("Experiment configuration incomplete or invalid");

        config.merge();


        // Load Algorithms

        File algfile = new File(root, config.getProperty(ConfigExperiment.RUN_ALGS));
        if (!algfile.exists()) abort("Could not find Algorithms file: " + algfile.getAbsolutePath());
        notify("Loading algorithms in: " + algfile.getAbsolutePath());

        List<Class<StandardMonitoring>> algs = new LinkedList<>();
        BufferedReader in = new BufferedReader(new FileReader(algfile));
        String line = "";
        while ((line = in.readLine()) != null) {
            try {
                @SuppressWarnings("unchecked")
                Class<StandardMonitoring> cls =
                        (Class<StandardMonitoring>) Experiment.class.getClassLoader().loadClass(line);
                algs.add(cls);
                notify("Loaded: " + line);
            } catch (Exception e) {
                abort("Could not load: " + line + " (" + e.getMessage() + ")");
            }
        }
        in.close();


        // Verify Traces
        int ntcomps = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_COMP));
        int nrcomps = Integer.parseInt(config.getProperty(ConfigExperiment.RUN_COMP));
        int ntraces = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_NUM));
        int nobs = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_OBS));
        int tlen = Integer.parseInt(config.getProperty(ConfigExperiment.TRACES_LEN));
        int runlen = Integer.parseInt(config.getProperty(ConfigExperiment.RUN_LEN));


        if (nrcomps > ntcomps)
            abort("Run requires more components than those provided in trace");

        String tdir = config.getProperty(ConfigExperiment.TRACES_DIR);
        if(!tdir.endsWith("/"))  tdir += "/";


        // Verify Formulas

        File forms = new File(root, config.getProperty(ConfigExperiment.RUN_SPECS));
        if (!forms.exists()) abort("Could not find formula file: " + forms.getAbsolutePath());

        if (!config.getProperty(ConfigExperiment.SKIP_TESTS).equals("true")) {
            String oraclecls = config.getProperty(ConfigExperiment.SPEC_VERIFIER);
            if (oraclecls.isEmpty()) abort("No Verifier for the specification provided");

            try {
                @SuppressWarnings("unchecked")
                Class<? extends TestOracle> cls = (Class<? extends TestOracle>)
                        Experiment.class.getClassLoader().loadClass(oraclecls);

                TestOracle oracle = cls.newInstance();
                notify("Using Verifier: " + oracle.getClass().getCanonicalName());
                verifySpec(oracle, forms, config.getProperty(ConfigExperiment.SPEC_DISCARD));

            } catch (Exception e) {
                e.printStackTrace();
                abort("Could not verify specification: " + e.getMessage());
            }


        }

        // Setup Platform

        String[] host       = config.getProperty(ConfigExperiment.NODE_HOST).split(":");
        int R_PORT          = Integer.parseInt(host[1]);


        Dispatcher disp     = new Sockets();
        Platform platform   = new Platform();
        Topology top        = new Topology();
        Set<String> nodes   = new HashSet<>();


        Map<String, Serializable> tags = new HashMap<>();







        platform.setData(new Integer(runlen));
        List<MsgCreateComponent> comps = new LinkedList<>();

        for(int k = 0; k < nrcomps; k++) {
            String name =  ((char) ('a' + k)) + "";
            top.add(name);
            MsgCreateComponent info = new MsgCreateComponent(
                    name, null, AsyncObservations.class
            );
            comps.add(info);
        }

        String[] addrs = config.getProperty(ConfigExperiment.NODE_TARGETS).split(",");

        for(String addr : addrs)
            nodes.add(addr);

        //Create workload
        Integer[] workload = new Integer[addrs.length];
        int tracesPerNode = ntraces / addrs.length;

        for(int j = 0; j < addrs.length; j++)
            workload[j] = tracesPerNode;

        workload[addrs.length - 1] = ntraces - (tracesPerNode * (addrs.length - 1));


        notify("Detected nodes: " + nodes.toString());
        notify("Workload: " + Arrays.toString(workload));


        final Server[] notifications = new Server[addrs.length];
        final Semaphore[] locks      = new Semaphore[addrs.length];

        for(int j = 0; j < addrs.length; j++) {
            locks[j] = new Semaphore(0);
            log.info("Starting Listen server on: " + host[0] + ":" + (R_PORT + j));
            notifications[j] = new Server(host[0], R_PORT + j);

            try {
                notifications[j].start(new SemaphorePingback(locks[j], log));
            }
            catch(Exception e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }





        notify("Beginning Runs");

        int totalForms = 0;
        int currentProg = 1;
        in = new BufferedReader(new FileReader(forms));
        while ((line = in.readLine()) != null) {
            totalForms++;
        }
        in.close();
        in = new BufferedReader(new FileReader(forms));
        int totalAlgs = algs.size();


        while ((line = in.readLine()) != null) {
            int currentAlg = 0;
            for (Class<StandardMonitoring> algcls : algs) {
                currentAlg++;

                StandardMonitoring alg = algcls.newInstance();
                Map<String, Specification> spec = SpecLoader.loadSpec(new File(root, line));
                alg.setSpecification(spec);
                Set<? extends Monitor> mons = alg.getMonitors(top);

                tags.put("spec",  line);
                tags.put("alg" ,  alg.getClass().getSimpleName());
                tags.put("tracedir", tdir.replace("/", ""));
                platform.getMonitors().clear();

                int ss = 0;
                int ee = 0;
                Future[] toWait = new Future[addrs.length];

                for(int k = 0; k < addrs.length; k++) {
                    ee = ss + workload[k] - 1;
                    RunTraces task = new RunTraces(
                        ss, ee, comps, mons, platform, nodes, addrs[k], tdir,
                            host[0] + ":" + (R_PORT + k), locks[k], tags, disp
                    );
                    log.info(currentProg + "/" + totalForms + " " + alg.getClass().getSimpleName() + " " +  addrs[k] + " monitoring: " + ss + " - " + ee);
                    ss = ee + 1;
                    toWait[k] = Runtime.getScheduler().submit(task);
                }

                for(Future f : toWait) {
                    f.get();
                }
            }

            currentProg++;
        }
        in.close();
        System.out.println();
        System.out.println("Done");
        for(Server s : notifications)
            s.stop();
        Runtime.getScheduler().shutdown();
    }



    public static void verifySpec(TestOracle oracle, File forms, String discard) {

        notify("Verifying specifications in: " + forms.getAbsolutePath());
        Set<Integer> detected = new HashSet<Integer>();
        BufferedReader in;
        int lineno = 1;
        int maxlines = 0;
        String spec;
        try {
            in = new BufferedReader(new FileReader(forms));
            while (in.readLine() != null)
                maxlines++;
            in.close();

            in = new BufferedReader(new FileReader(forms));
            while ((spec = in.readLine()) != null) {
                System.out.printf("\r%8d/%8d", lineno, maxlines);

                Map<String, Specification> specinfo = SpecLoader.loadSpec(new File(spec));
                if (!oracle.verify(specinfo)) {
                    if (discard.isEmpty())
                        abort("Could not verify spec" + spec);
                    else
                        detected.add(lineno);
                }

                lineno++;
            }
            System.out.println();
            in.close();

            if (detected.size() > 0) {
                in = new BufferedReader(new FileReader(forms));
                lineno = 1;

                PrintWriter fkeep = new PrintWriter(
                        new FileOutputStream(forms.getCanonicalPath() + ".keep"));
                PrintWriter fdiscard = new PrintWriter(
                        new FileOutputStream(forms.getCanonicalPath() + ".discard"));

                while ((spec = in.readLine()) != null) {
                    if (detected.contains(lineno))
                        fdiscard.println(spec);
                    else
                        fkeep.println(spec);
                    lineno++;
                }
                fkeep.close();
                fdiscard.close();
                in.close();
                abort("Invalid specs: " + detected.size());
            }
        } catch (IOException e) {
            abort(e.getLocalizedMessage());
        }


    }

    public static boolean verifyConfig(Config conf) {
        for (ConfigExperiment v : ConfigExperiment.values()) {
            if (!conf.containsKey(v.getValue())) {
                System.err.println("Missing: " + v.getValue());
                return false;
            }
        }
        return true;
    }

    public static void statusLine(int f_cur, int f_total, int t_cur, int t_total,
                                  int c_alg, int t_alg, String alg) {
        System.out.printf("\rSpec: %8d/%8d | Trace: %8d/%8d | Algorithm: (%3d/%3d) %-50s",
                f_cur, f_total, t_cur, t_total, c_alg, t_alg, alg);
    }

    public static void abort(String message) {
        log.fatal(message);
        System.exit(1);
    }

    public static void notify(String message) {
        log.info(message);
    }

}
