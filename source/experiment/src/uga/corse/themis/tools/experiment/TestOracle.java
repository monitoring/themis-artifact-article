package uga.corse.themis.tools.experiment;

import java.util.Map;
import uga.corse.themis.monitoring.specifications.Specification;


public interface TestOracle {
    boolean verify(Map<String, Specification> spec);
}
