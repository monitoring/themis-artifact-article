---
title: Experiment Tool
---

The THEMIS experiment tool allows THEMIS to execute multiple parametrized runs.

It works in two passes: verification and execution.

> If THEMIS is installed, you can directly call the experiment tool using `themis-experiment`

Usage:

```bash
themis-experiment /path/to/experiment/folder /path/to/properties
```

The properties path defaults to `setup.properties`
Typically it is called inside an experiment folder as:

```bash
themis-experiment . 
```


#### Verification Pass ####
The verification pass is executed if `tests.skip=false` is set in the [setup.properties](example/comp3/setup.properties) file.
This pass loads each specification and checks it using a [TestOracle](src/uga/corse/themis/tools/experiment/TestOracle.java) specified in `tests.oracle` in the [setup.properties](example/comp3/setup.properties) file.
The oracle has one method `verify()` which performs a test on a spec.
If `tests.discard` is enabled, then the pass will generate two files:

* `.keep` lists all the specifications that passed the test
* `.discard` lists all the specifications that failed the test

The default provided provided checks LTL expressions. [TestLTL](src/uga/corse/themis/tools/experiment/TestLTL.java) generates an automaton from the specification using `ltl2mon`.

**Note:** Since the `ltl2mon` script sometimes keeps rogue processes running if it timesout, use the `cleanup` script while this pass is being done, typically you should run `themis_ltl2monkill 3m`.

#### Execution Pass ####
Once the verification pass terminates and all specifications pass the test, then the tool proceeds to the second phase: execution.
In the execution phase the tool will for each specification consider all traces.
Then, for each trace run all the algorithms with the specification.
The outcome of the execution is stored in a database specified by the `THEMIS_BENCH_DB` env variable or configuration in  [setup.properties](example/comp3/setup.properties)).

#### Creating an Experiment ####
To setup experiments quickly, you can use the scripts provided as follows.
Make sure that THEMIS is already installed and the scripts are copied properly (see [dist](../dist/))

First create a folder for the experiment:
```
make myexperiment
cd myexperiment
```

Then generate the skeleton of an experiment by running the command:
```
themis_expskeleton
```

Generate a 50  random traces of length 60 with 3 components and 2 observations per component using a normal probability distribution.
```
themis_gentrace 50 60 3 2  $THEMIS_HOME/resources/profiles/normal.profile traces 1
```

Generate 15 specifications based on LTL that reference 3 components with 2 observations each
```
themis_genspec 15 3 2  $THEMIS_HOME/resources/templates/ltl.xml spec 1
```

If you wish to visualize a specification (and have the themis-ui module installed)
```
themis-draw -spec -expr specs/spec1.xml       # For centralized
themis-draw -spec -expr -net specs/spec1.xml  # For the choreography decentralization
```

Start a node:
```
make node HOST=localhost PORT=8056
```

While the node is running execute the experiment
```
make run CFILE=setup.properties
```

If some specifications fail the oracle test, two files will be generated:

* `specs.txt.keep` : with the formulae that passed
* `specs.txt.discard` : with the formulae that failed

To delete the associated XML files with the specs to discard execute, and replace your specs with the ones that pass the oracle test, you can optionally clear the cache:

```
themis_specdiscard < specs.txt.discard
mv specs.txt.keep specs.txt
rm -rf cache/*
```

You can now execute the experiment again, all formulae should pass the test.
